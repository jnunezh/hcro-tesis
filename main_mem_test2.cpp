/*
 * File:   main.cpp
 * Author: Alexander
 *
 * Created on 21 de mayo de 2015, 01:56 PM
 */

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <list>
#include <sstream>

#include "BHEFTAlgorithm.h"
#include "CPOPAlgorithm.h"
#include "HCROAlgorithm.h"
#include "Priorities.h"
#include "file_persist.h"
using namespace std;



int main(int argc, char** argv) {
  std::srand(unsigned(std::time(0)));

#ifdef CPU_MULTITHREADED
  std::cout << "==================[Multi-thread Mode]=======================" << std::endl;
#else
  std::cout << "==================[Single-thread Mode]=======================" << std::endl;
#endif

  size_t num_procs = 5;
  size_t num_tasks = 30;
  b_rank_t bp;
  t_rank_t tp;  // a,t_rank_t tp;
  l_rank_t lp;
  std::vector<priority_cmp_t<float> > priorities(3);
  priorities[0].set_criterion(&bp);
  priorities[1].set_criterion(&tp);
  priorities[2].set_criterion(&lp);

  std::string uid = "uid_nu_123456";
  std::vector<hcro_molecule_t> model_molecs;
  hms_param_t custom_param;
  custom_param = hms_params_from_json_file("data/nu_params.json", uid);

  if(custom_param.is_empty()){
    custom_param = hms_param_t::random_generate(num_procs, num_tasks, 0.4f, 1.5f, 9.5f, 1.0f, 10.0f);
    auto res = persist_hms_params_to_json_file("data/nu_params.json", custom_param, uid);
    std::cout << "persist_hms_params_to_json_file => res.first:" << res.first << " res.second:" << res.second << std::endl; 
    hms_param_t param_b = hms_params_from_json_file("data/nu_params.json", uid);
    std::cout << "param_b empty?:"<< param_b.is_empty() << std::endl;
    std::cout << "param == param_b?:" << (custom_param == param_b) << std::endl;
  }
  

  std::cout << "custom_param.edges==>" << custom_param.get_num_edges() << std::endl;

  //std::cout << custom_param << std::endl;

  std::vector<std::string> molecs_strs = {
    "0;2;1;3;5;4;6;7;8;9;10;11;12;15;13;14;18;19;16;17;21;20;23;22;24;26;25;28;27;29;", //-
    "0;2;9;1;3;14;10;4;5;6;7;12;8;13;11;18;17;15;16;19;20;21;23;22;24;26;25;27;28;29;", //-
    "0;1;9;28;2;3;4;10;14;5;6;8;7;13;12;11;18;17;15;16;19;23;21;20;22;24;26;25;27;29;",
    "0;2;1;3;5;4;6;7;8;9;10;11;12;15;13;14;18;19;16;17;21;20;23;22;24;26;25;28;27;29;",
    "0;28;2;9;1;3;14;10;4;5;6;7;12;8;13;11;18;17;15;16;19;20;21;23;22;24;26;25;27;29;",
    "0;1;2;9;28;4;10;14;3;5;6;8;7;13;12;11;18;17;15;16;19;23;21;20;22;24;26;25;27;29;",
    "0;2;1;3;5;4;6;7;8;9;10;11;12;15;13;14;18;19;16;17;21;20;23;22;24;26;25;28;27;29;",
    "0;28;2;9;1;3;14;4;5;6;7;10;12;8;13;11;18;17;15;16;19;20;21;23;22;24;26;25;27;29;",
    "0;1;2;9;28;3;4;10;5;6;8;7;13;12;11;18;14;17;15;16;19;23;21;20;22;24;26;25;27;29;",
    "0;2;1;3;5;4;6;7;8;10;11;12;9;15;13;14;18;19;16;17;21;20;23;22;24;26;25;28;27;29;",
    "0;28;2;9;1;3;14;10;4;5;6;7;12;8;13;11;18;17;15;16;19;20;21;23;22;24;26;25;27;29;",
    "0;1;2;9;28;3;4;10;14;5;6;7;13;12;8;11;18;17;15;16;19;23;21;20;22;24;26;25;27;29;",
    "0;2;1;3;5;4;6;7;8;9;10;11;15;13;14;12;18;19;16;17;21;20;23;22;24;26;25;28;27;29;",
    "0;28;2;9;1;3;14;10;4;5;6;7;12;13;8;11;18;17;15;16;19;20;21;23;22;24;26;25;27;29;",
    "0;1;2;9;28;3;4;10;14;5;6;8;7;13;11;12;18;17;15;16;19;23;21;20;22;24;26;25;27;29;",
    "0;2;1;3;5;4;6;7;8;9;10;11;12;15;13;18;14;19;16;17;21;20;23;22;24;26;25;28;27;29;",
    "0;28;2;9;1;3;14;10;4;5;6;7;12;8;13;11;17;15;16;19;20;18;21;23;22;24;26;25;27;29;",
    "0;1;2;9;28;3;4;10;14;5;6;8;7;13;12;11;18;15;16;19;17;23;21;20;22;24;26;25;27;29;",
    "0;2;1;3;5;4;6;7;8;9;10;11;12;15;13;14;18;19;17;21;16;20;23;22;24;26;25;28;27;29;",
    "0;28;2;9;1;3;14;10;4;5;6;7;12;8;13;11;18;17;15;19;16;20;21;23;22;24;26;25;27;29;"
  };
  
  float beta = 100.0f;

  try {
    hcro_algorithm_t alg1(200,   // num_iterations
                       3,     // initial_num_groups : aka groups of sections
                       8,     // max_num_groups
                       30,    // section_amount aka elements per section
                       0.5f,  // percent elements per section initially created
                       7,     // num_candidates
                       priorities,  // priorities
                       2000.0f,     // buffer_init_val
                       0.0f,        // mu
                       1.0f,        // sigma
                       0.5f,        // collision_rate
                       200,         // alfa:char
                       beta,      // beta
                       10000.0f,    // ke_init
                       0.5f         // ke_loss_rate
    );

    alg1.set_param(&custom_param);

    /*
    float pe;
    for(const std::string& str : molecs_strs){
      
      hcro_molecule_t temp_molec = hcro_molecule_t::from_string(
        str,
        500,
        10000,
        200.0f,
        500.0f,
        false);
      pe = temp_molec.calculate_pe();
      //std::cout << temp_molec << std::endl;
      std::cout << "====================[pe]====> " << pe << "<======================>" << std::endl;
      
    }
    */
    std::string s0, s1;
    s0 = "0;2;1;3;5;4;6;7;8;9;28;10;14;13;12;11;15;18;17;16;19;20;21;23;22;24;26;25;27;29;";
    s1 = "0;2;1;3;5;4;6;7;8;9;10;11;12;15;13;28;14;18;17;16;19;21;20;23;22;24;26;25;27;29;";
    int b, e;
    hcro_molecule_t molec0 = hcro_molecule_t::from_string(
      s0,
      500,
      10000,
      200.0f,
      500.0f,
      false);
      
    hcro_molecule_t molec1 = hcro_molecule_t::from_string(
      s1,
      500,
      10000,
      200.0f,
      500.0f,
      false);  

    molec0.xor_range_bounds(molec1, b, e);
    std::cout << "b:" << b << ",e:" << e << std::endl;
    //std::cout << "other molec" << std::endl;
    //std::cout << other_molec << std::endl;

    /*
    molec.left_rotate_by_one(20, 22);
    std::cout << "molec modified" << std::endl;
    std::cout << molec << std::endl;
    */
    
    /*

    schedule_t sc;
    molec.calculate_pe(&sc, true);
    std::cout << "pe ====> " << molec.get_pe() << std::endl;
    std::cout << sc << std::endl;
    */
    /*
    reaction_min_t result;
    do_synthesis(
      molec,
      other_molec,
      result,
      beta);
    */  

  } catch (const std::exception& e) {
    std::cout << "failed@maim_mem_test:" << e.what() << std::endl;
    return 0;
  }  
  
  std::cout << "fin" << std::endl;
  return 0;
}
