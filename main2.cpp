/*
 * File:   main.cpp
 * Author: Alexander
 *
 * Created on 21 de mayo de 2015, 01:56 PM
 */

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <list>
#include <sstream>


#include "BHEFTAlgorithm.h"
#include "CPOPAlgorithm.h"
#include "HCROAlgorithm.h"
#include "Priorities.h"
#include "file_persist.h"
using namespace std;

int main(int argc, char** argv) {
  std::srand(unsigned(std::time(0)));

#ifdef CPU_MULTITHREADED
  std::cout << "==================[Multi-thread Mode]=======================" << std::endl;
#else
  std::cout << "==================[Single-thread Mode]=======================" << std::endl;
#endif

  /*TODO LIST: set priority vector;verify constraints of setnodes_info;(15 & 16
   * alfa y beta); minlocal correctness:null?,memory leaks? not initialized? pe
   * outdated:how? or better update pe everytime inner_molecule is
   * changed[better!!!]
   * ;*/
  b_rank_t bp;
  t_rank_t tp;  // a,t_rank_t tp;
  l_rank_t lp;
  std::vector<priority_cmp_t<float> > priorities(3);
  priorities[0].set_criterion(&bp);
  priorities[1].set_criterion(&tp);
  priorities[2].set_criterion(&lp);
 
  size_t curr_procs = 20, curr_tasks = 50;  // 4, 35

  hms_param_t param = hms_param_t::random_generate(curr_procs, curr_tasks, 0.4f,
                                                           1.5f, 9.5f, 1.0f, 10.0f);

  float biv, cr, beta, ki, klr, e1;
  schedule_t s1;
  short alfa;

  biv = 50.0f;
  cr = 0.4f;
  alfa = 30;
  beta = 480.0f;
  ki = 50.0f;
  klr = 0.1f; // 0.1f

  try {
    hcro_algorithm_t alg1(200,   // num_iterations
                       3,     // initial_num_groups : aka groups of sections
                       8,     // max_num_groups
                       30,    // section_amount aka elements per section
                       0.5f,  // percent elements per section initially created
                       7,     // num_candidates
                       priorities,  // priorities
                       biv,     // buffer_init_val
                       0.0f,        // mu
                       1.0f,        // sigma
                       cr,        // collision_rate
                       alfa,         // alfa:char
                       beta,      // beta
                       ki,    // ke_init
                       klr         // ke_loss_rate
    );
    // std::cout << "ended hcro A" << std::endl;
    alg1.set_param(&param);

    assign_elapsed_ms(e1, s1 = alg1.run());
    

    /*
    myfile << "duration_ms:" << std::fixed << std::setw(7) << std::setprecision(1) << e1;
    myfile << " buffer_init_val:" << std::fixed << std::setw(7) << std::setprecision(1) << biv;
    myfile << " collision_rate:" << std::fixed << std::setw(3) << std::setprecision(1) << cr;
    myfile << " alfa:" << std::fixed << std::setw(3) << alfa;
    myfile << " beta:" << std::fixed << std::setw(5) << std::setprecision(1) << beta;
    myfile << " ke_init:" << std::fixed << std::setw(7) << std::setprecision(1) << ki;
    myfile << " ke_loss_rate:" << std::fixed << std::setw(3) << std::setprecision(1) << klr;
    myfile << " makespan:" << std::fixed << std::setw(7) << std::setprecision(1) << s1.makespan;
    myfile << std::endl;
    */

    //std::cout << " duration_ms:" << e1;
    //std::cout << " makespan:" << s1.makespan;
    //std::cout << std::endl;

  } catch (const std::exception& e) {
    std::cout << "failed@hcro:" << e.what() << std::endl;
    return 0;
  }

  std::cout << "makespan:" << s1.makespan << ", duration_ms:" << e1 << std::endl;
  std::cout << "fin" << std::endl;
  return 0;
}
