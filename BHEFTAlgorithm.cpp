/* 
 * File:   BHEFTAlgorithm.cpp
 * Author: Alexander
 * 
 * Created on 12 de junio de 2015, 07:20 PM
 */
#include "BHEFTAlgorithm.h"
#include "Priorities.h"

bheft_algorithm_t::bheft_algorithm_t() {
}

bheft_algorithm_t::bheft_algorithm_t(const bheft_algorithm_t& orig) {
}

bheft_algorithm_t::~bheft_algorithm_t() {
}

schedule_t bheft_algorithm_t::old_run(){

  //@@@@@@@@@@@@@@@@@qqPaper35.pdf PAG. 5
  const hms_param_t& ref_param = get_param();
  proc_t pid, num_procs, min_proc;
  num_procs = ref_param.get_num_procs();
  task_t tid, num_tasks = ref_param.get_num_tasks();
  time_unit_t ast, eft, min_eft, min_ast;
  std::vector<proc_t> proc_assignmts(num_tasks);
  std::vector<time_unit_t> afts(num_tasks);
  std::vector<float> blevels(num_tasks);
  std::priority_queue<std::pair<task_t,time_unit_t>,std::vector<std::pair<task_t,time_unit_t> >,desc_second_cmp_t<task_t,time_unit_t> > pq; //DESC order
  b_rank_t op;
  time_unit_t makespan = default_zero_value<time_unit_t>();
  schedule_t schedule;

  op.generate(ref_param, blevels);
  
  
  for(tid = 0; tid < num_tasks; ++tid){
      pq.push(std::pair<task_t,time_unit_t>(tid,blevels[tid]));
  }
  
  schedule.sched.resize(num_procs);
  std::pair<task_t,time_unit_t> ptop;
  std::vector<std::pair<task_t,time_unit_t> >::const_iterator trial_it,min_trial_it;

  while(!pq.empty()){
    tid = pq.top().first;
    pq.pop();
    min_eft = plus_infinity<time_unit_t>();
    
    for(pid = 0; pid < num_procs; ++pid){
      eft = plus_infinity<time_unit_t>();
      if(!ref_param.try_insertion_policy(
        tid,
        pid,
        afts,
        proc_assignmts,
        trial_it,
        schedule.sched[pid],
        min_eft,
        ast,
        eft)){
        continue;
      }

      if (eft < min_eft) {
        min_eft = eft;
        min_ast = ast;
        min_proc = pid;
        min_trial_it = trial_it;
      }
    }
    
    ref_param.assign_task_to_processor(
      tid,
      min_proc,
      min_trial_it,
      min_ast,
      schedule);

    proc_assignmts[tid] = min_proc;
    afts[tid] = min_eft;
    if(min_eft > makespan)
     makespan = min_eft;
  }
  schedule.makespan = makespan;        
  return schedule;
}

schedule_t bheft_algorithm_t::run(){
  //@@@@@@@@@@@@@@@@@qqPaper35.pdf PAG. 5
  const hms_param_t& ref_param = get_param();
  proc_t pid, num_procs, min_proc;
  num_procs = ref_param.get_num_procs();
  task_t tid, num_tasks = ref_param.get_num_tasks();
  time_unit_t ast, eft, min_eft, min_ast;
  std::vector<max_entry_t> rest_runways_maxs;
  std::vector<float> blevels(num_tasks);
  std::priority_queue<std::pair<task_t,time_unit_t>,std::vector<std::pair<task_t,time_unit_t> >,desc_second_cmp_t<task_t,time_unit_t> > pq; //DESC order
  b_rank_t op;
  time_unit_t makespan = default_zero_value<time_unit_t>();
  schedule_t schedule;

  op.generate(ref_param, blevels);
  rest_runways_maxs.resize(num_tasks);
  
  for(tid = 0; tid < num_tasks; ++tid){
      pq.push(std::pair<task_t,time_unit_t>(tid,blevels[tid]));
  }
  
  schedule.sched.resize(num_procs);
  std::pair<task_t,time_unit_t> ptop;
  std::vector<std::pair<task_t,time_unit_t> >::const_iterator trial_it,min_trial_it;

  while(!pq.empty()){
    tid = pq.top().first;
    pq.pop();
    min_eft = plus_infinity<time_unit_t>();
    
    for(pid = 0; pid < num_procs; ++pid){
      eft = plus_infinity<time_unit_t>();
      if(!ref_param.try_insertion_policy(
        tid,
        pid,
        rest_runways_maxs[tid],
        trial_it,
        schedule.sched[pid],
        min_eft,
        ast,
        eft)){
        continue;
      }

      if (eft < min_eft) {
        min_eft = eft;
        min_ast = ast;
        min_proc = pid;
        min_trial_it = trial_it;
      }
    }

    ref_param.update_rest_runways_maxs(
      rest_runways_maxs,
      tid,
      min_proc,
      min_eft);
        
    ref_param.assign_task_to_processor(
      tid,
      min_proc,
      min_trial_it,
      min_ast,
      schedule);

    if(min_eft > makespan)
     makespan = min_eft;
  }
  schedule.makespan = makespan;        
  return schedule;
}

