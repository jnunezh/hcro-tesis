#include "Stats.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <iomanip>

std::string procs_runway_population_pctgs(const schedule_t& s, size_t num_tasks){

	std::stringstream str;
	proc_t pid = 0;
	float pctg;
	for(const auto& proc_runway : s.sched){
		pctg = static_cast<float>(proc_runway.size()) / static_cast<float>(num_tasks);
		pctg *= 100.f;
		str << std::endl;
		str << std::fixed << std::setw(3);
		str << pid++ << ":" << std::fixed << std::setw(6) << std::setprecision(2);
		str << pctg << "%";
	}
	return str.str();
}

float get_max_proc_runway_occupancy(const schedule_t& s){
	size_t num_tasks = 0;
	size_t max_proc_runway_size = 0;
	for(const auto& proc_runway : s.sched){
		
		if(proc_runway.size() > max_proc_runway_size){
			max_proc_runway_size = proc_runway.size();
		}
		num_tasks += proc_runway.size();
		
	}
	return static_cast<float>(max_proc_runway_size)/static_cast<float>(num_tasks);
}