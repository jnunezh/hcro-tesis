# encoding=utf8
# this script, given an unified_params filepath, outputs to terminal the value of a algorithm constant or property
# invoked, for example, like this:
# >python hms_info.py -p "/home/alexander/Documents/hcro-tesis/data/params.txt" -f E

# such fields could be {M, P, E, R} or their long values such as {num_tasks, num_procs, num_edges}, where M => tasks, P => processors, E => edges, R=> relatives of all nodes

import time
import os
import sys
import getopt
import itertools


# <collision_rate:float>
# ##########
# <ke_loss_rate:float>
# ##########
# <ke_init:float>
# ##########
# <beta:float>
# ##########
# <buffer_value:float>
# ##########
# <mu:float>
# ##########
# <sigma:float>
# ##########
# <alpha:short>
# ##########
# <init_pop:int>
# ##########
# <seed:unsigned long>
# ##########
# <limit_offsets as <succesor_offset, pred_offsets>> ==> 2,3;4,5;8,8;
# ##########
# <comm_successors> ==> 2, 3.11;5, 9.455;7, 11.77;10, 9.781;
# ##########
# <comm_predecessors> ==> 2, 3.11;5, 9.455;7, 11.77;10, 9.781;
# ##########
# <weights_matrix> ==> 2.5; 1.2; 3.1; 4.8; 1.66; 9.99; 14.66; 7.1; 1.66; 9.99; 14.66; 7.1;

LIMIT_OFFSETS_INDEX = 10
MATRIX_INDEX = 13
COMM_SUCCS_INDEX = 11
COMM_PREDS_INDEX = 12

def filtered_chunks(token, delim):
    return [s for s in token.split(delim) if len(s.strip()) > 0]

def css_split_count(token, delim):
    return len(filtered_chunks(token, delim))

def get_field_count(token, field, recursive=False):
    global LIMIT_OFFSETS_INDEX
    global MATRIX_INDEX
    global COMM_SUCCS_INDEX

    seqs = token.split('##########')
    m = dict()
    m['M'] = {'fi': LIMIT_OFFSETS_INDEX, 'div': 1}
    m['num_tasks'] = {'fi': LIMIT_OFFSETS_INDEX, 'div': 1}
    m['P'] = {'fi': MATRIX_INDEX, 'div': 1 if recursive else get_field_count(token, 'M', True)}
    m['num_procs'] = {'fi': MATRIX_INDEX, 'div': 1 if recursive else get_field_count(token, 'M', True)}
    m['E'] = {'fi': COMM_SUCCS_INDEX, 'div': 1}
    m['num_edges'] = {'fi': COMM_SUCCS_INDEX, 'div': 1}

    return css_split_count(seqs[m[field]['fi']], ';') / m[field]['div']

def print_relatives(token):
    global LIMIT_OFFSETS_INDEX
    global MATRIX_INDEX
    global COMM_SUCCS_INDEX
    global COMM_PREDS_INDEX

    seqs = token.split('##########')
    limit_offsets = [s.split(',') for s in filtered_chunks(seqs[LIMIT_OFFSETS_INDEX], ';')] # [(succ_offset, pred_offset)]
    comm_successors = [s.split(',') for s in filtered_chunks(seqs[COMM_SUCCS_INDEX], ';')]
    comm_predecessors = [s.split(',') for s in filtered_chunks(seqs[COMM_PREDS_INDEX], ';')]
    matrix = [s for s in filtered_chunks(seqs[MATRIX_INDEX], ';')]
    num_tasks = get_field_count(token, 'M')
    num_edges = get_field_count(token, 'E')
    num_procs = get_field_count(token, 'P')
    for i in xrange(0, num_tasks):
        print('==============[tid:%3d]=============' % i)
        print('ancestors:')
        task_offset = int(limit_offsets[i][1])
        if i + 1 == num_tasks:
            next_task_offset = num_edges
        else:
            next_task_offset = int(limit_offsets[i + 1][1])

        curr_off = task_offset
        while curr_off != next_task_offset:
            print('\ttid:%3d, ct:%f' % (int(comm_predecessors[curr_off][0]), float(comm_predecessors[curr_off][1])))
            curr_off += 1
        print('children:')
        task_offset = int(limit_offsets[i][0])
        if i + 1 == num_tasks:
            next_task_offset = num_edges
        else:
            next_task_offset = int(limit_offsets[i + 1][0])

        curr_off = task_offset
        while curr_off != next_task_offset:
            print('\ttid:%3d, ct:%f' % (int(comm_successors[curr_off][0]), float(comm_successors[curr_off][1])))
            curr_off += 1
        print('matrix:')
        sub_matrix = matrix[num_procs * i : num_procs * (i + 1)]
        for j in xrange(0, len(sub_matrix)):
            print('\tpid:%3d, ct:%f' % (j, float(sub_matrix[j])))



def main(argv):
    
    filepath = None
    field = None
    reload(sys)
    sys.setdefaultencoding('utf8')

    try:
        opts, args = getopt.getopt(argv,"p:f:",["path=", "field="])
    except getopt.GetoptError:
        print '<script>.py -p <filepath> -f <field>'
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-p", "--path"):
            filepath = arg
        elif opt in ("-f", "--field"):
            field = arg

    if filepath is None or field is None:
        print 'error # <script>.py -p <filepath> -f <field>'
        sys.exit(2)
    
    start = time.time()

    text = None

    with open(filepath, 'r') as content_file:
        text = content_file.read()

    output = None
    if field != 'R':
        output = get_field_count(text, field)
        print(field, output)
    else:
        print_relatives(text)
    
    

    end = time.time()
    print('program finished on %f mins' % ((end - start) / 60))

if __name__ == "__main__":
    main(sys.argv[1:])