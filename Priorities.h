/*
 * File:   Priorities.h
 * Author: Alexander
 *
 * Created on 8 de noviembre de 2015, 10:31 PM
 */

#ifndef PRIORITIES_H
#define PRIORITIES_H

#include "Utils.h"

class b_rank_t : public priority_criterion_t {
 public:
  virtual void generate(const hms_param_t& param, std::vector<float>& buffer);
};
class t_rank_t : public priority_criterion_t {
 public:
  virtual void generate(const hms_param_t& param, std::vector<float>& buffer);
};
class l_rank_t : public priority_criterion_t {
 public:
  virtual void generate(const hms_param_t& param, std::vector<float>& buffer);
};

//----------------------------------------------------------------------------
class a_rank_t : public priority_criterion_t {
 public:
  virtual void generate(const hms_param_t& param, std::vector<float>& buffer);
};
class cldd_rank_t : public priority_criterion_t {
 public:
  virtual void generate(const hms_param_t& param, std::vector<float>& buffer);
};

class sl_rank_t : public priority_criterion_t {
 public:
  virtual void generate(const hms_param_t& param, std::vector<float>& buffer);
};

class mixed_rank_t : public priority_criterion_t {
 public:
  virtual void generate(const hms_param_t& param, std::vector<float>& buffer);
};
#endif /* PRIORITIES_H */
