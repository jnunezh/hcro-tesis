/*
 * File:   HCROAlgorithm.h
 * Author: Alexander
 *
 * Created on 31 de mayo de 2015, 08:40 AM
 */

#ifndef UTILS2_H
#define UTILS2_H

#include "Utils.h"

#include <set>
#include <vector>

#include <algorithm>
#include <cmath>
#include <cstring>
#include <functional>
#include <iostream>
#include <limits>
#include <map>
#include <numeric>
#include <sstream>
#include <string>
#include <iomanip>

#include "Utils.h"
#include "file_persist.h"
#include "HCROAlgorithm.h"

inline float my_rand_inclusive(float lb, float ub){
  static std::vector<float> values = split_values_from_file<float>("./../hcro_tesis_tests/data/fouts.txt");
  static int index = 0;
  assert(lb <= ub);
  assert(index < values.size());
  return values[index++];
}


inline int my_rand_inclusive(int lb, int ub){
  static std::vector<int> values = split_values_from_file<int>("./../hcro_tesis_tests/data/iouts.txt");
  static int index = 0;
  assert(lb <= ub);
  assert(index < values.size());
  return values[index++];
}


void generate_owi_candidate(
  const hcro_molecule_t& molecule,
  hcro_molecule_t& first);

void generate_decomp_candidates(
  const hcro_molecule_t& molecule,
  hcro_molecule_t& first,
  hcro_molecule_t& second);

void generate_imi_candidates(
  const hcro_molecule_t& molecule,
  const hcro_molecule_t& omolecule,
  hcro_molecule_t& first,
  hcro_molecule_t& second);

bool generate_synth_candidate(
  const hcro_molecule_t& molecule,
  const hcro_molecule_t& other,
  hcro_molecule_t& candidate); /*returns if generated */

void do_decomposition(
  hcro_molecule_t& molecule,
  int index,
  hcro_molecule_t& other_molecule,
  int om_index,
  reaction_min_t& result);

void do_on_wall_ineffect_collision(
  hcro_molecule_t& molecule,
  int index,
  reaction_min_t& result);


void do_inter_molec_ineffect_collision(
  hcro_molecule_t& molecule,
  int index,
  hcro_molecule_t& omolecule,
  int om_index,
  reaction_min_t& result);


bool do_synthesis(
    hcro_molecule_t& molecule,
    int m_index,
    hcro_molecule_t& omolecule,
    int om_index,
    reaction_min_t& result);

float gen_new_random_x(hcro_algorithm_t* ptr);


void do_single_reaction(
  hcro_molecule_t& selected,
  int index,
  hcro_molecule_t& other_selected,
  int other_index,
  reaction_min_t& selected_ms);

#endif /* UTILS2_H */
