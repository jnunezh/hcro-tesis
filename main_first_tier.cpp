/*
 * File:   main.cpp
 * Author: Alexander
 *
 * Created on 21 de mayo de 2015, 01:56 PM
 */

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <list>
#include <sstream>

#include "BHEFTAlgorithm.h"
#include "CPOPAlgorithm.h"
#include "HCROAlgorithm.h"
#include "Priorities.h"
#include "file_persist.h"
using namespace std;

int main(int argc, char** argv) {
  std::srand(unsigned(std::time(0)));

#ifdef CPU_MULTITHREADED
  std::cout << "==================[Multi-thread Mode]=======================" << std::endl;
#else
  std::cout << "==================[Single-thread Mode]=======================" << std::endl;
#endif

  /*TODO LIST: set priority vector;verify constraints of setnodes_info;(15 & 16
   * alfa y beta); minlocal correctness:null?,memory leaks? not initialized? pe
   * outdated:how? or better update pe everytime inner_molecule is
   * changed[better!!!]
   * ;*/
  size_t num_procs, num_tasks;
  b_rank_t bp;
  a_rank_t ap; //t_rank_t tp;
  l_rank_t lp;
  std::vector<priority_cmp_t<float> > priorities(3);
  std::string uid = "uid_created_new0002";
  std::string hms_param_path = "./data/created_params.json";
  std::string model_molecs_path = "./data/model_molecs.txt";
  std::string cr_params_path = "./data/cr_params.txt";
  std::vector<hcro_molecule_t> model_molecs;
  hcro_molecule_t molec, other_molec;
  hms_param_t custom_param;
  float buffer_init_val = 2000.0f;
  float mu = 0.0f;
  float sigma = 1.0f;
  float collision_rate = 0.5f;
  short alpha = 200;
  float beta = 100.0f;
  float ke_init = 10000.0f;
  float ke_loss_rate = 0.5f;

  num_tasks = 75;
  num_procs = 22;

  priorities[0].set_criterion(&bp);
  priorities[1].set_criterion(&ap);
  priorities[2].set_criterion(&lp);

  

  custom_param = hms_params_from_json_file(hms_param_path, uid);

  if(custom_param.is_empty()){
    custom_param = hms_param_t::random_generate(num_procs, num_tasks, 0.4f, 1.5f, 9.5f, 1.0f, 10.0f);
    auto res = persist_hms_params_to_json_file(hms_param_path, custom_param, uid);
    std::cout << "persist_hms_params_to_json_file => res.first:" << res.first << " res.second:" << res.second << std::endl; 
    hms_param_t param_b = hms_params_from_json_file(hms_param_path, uid);
    std::cout << "param_b empty?:"<< param_b.is_empty() << std::endl;
    std::cout << "param == param_b?:" << (custom_param == param_b) << std::endl;
  }
  

  std::cout << "custom_param.edges==>" << custom_param.get_num_edges() << std::endl;

  //std::cout << custom_param << std::endl;

  try {
    hcro_algorithm_t alg1(200,   // num_iterations
                       3,     // initial_num_groups : aka groups of sections
                       8,     // max_num_groups
                       30,    // section_amount aka elements per section
                       0.5f,  // percent elements per section initially created
                       7,     // num_candidates
                       priorities,  // priorities
                       buffer_init_val,     // buffer_init_val
                       mu,        // mu
                       sigma,        // sigma
                       collision_rate,        // collision_rate
                       alpha,         // alfa:char
                       beta,      // beta
                       ke_init,    // ke_init
                       ke_loss_rate         // ke_loss_rate
    );

    alg1.set_param(&custom_param);

    std::cout << "-----------------[c-readable-model-molecs]---------------------" << std::endl;

    int i;
    bool molecs_persisted = false;
    {
      model_molecs = hcro_molecule_t::generate_model_molecules(priorities);
      for(hcro_molecule_t& m : model_molecs){
        m.calculate_pe(); //to avoid trash values as pe
        m.set_inner_min_default();
      }
      i = rand_inclusive(0, model_molecs.size() - 1);
      molec = model_molecs[i];

      if(persist_molecules_to_c_readable_file(model_molecs_path, model_molecs)){
        std::cout << "model molecs were persisted!!!" << std::endl;     
        molecs_persisted = true;
      }
      else{
        std::cout << "model molecs could not be persisted!!!" << std::endl;
      }      
    }

    if(molecs_persisted){
      std::vector<hcro_molecule_t> molecs = molecules_from_c_readable_file(model_molecs_path);
      other_molec = molecs[i];
      
      if(other_molec.is_empty()){
        std::cout << "other_molec is empty" << std::endl;
      }
      else{
        std::cout << "other_molec is not empty" << std::endl;
        if(molec == other_molec){
          std::cout << "molec & other_molec are equal" << std::endl;
        }
        else{
          std::cout << "molec & other_molec are NOT equal" << std::endl;
        }
      }
    }

    std::cout << "-----------------[c-readable-unified-params]-----------" << std::endl;

    unified_params_t<default_traits_t> u_params = custom_param.to_unified_params_t<default_traits_t>(
      alg1.get_buffer_value(),
      alg1.get_mu(),
      alg1.get_sigma(),
      alg1.get_collision_rate(),
      alg1.get_alpha(),
      alg1.get_beta(),
      alg1.get_ke_init_value(),
      alg1.get_ke_loss_rate(),
      true,
      get_curr_epoch_ns());

    if(persist_params_to_c_readable_file(cr_params_path, u_params)){
      std::cout << "params were persisted!!!" << std::endl;     
      
      unified_params_t<default_traits_t> other_params = params_from_c_readable_file(cr_params_path);

      if(other_params.is_empty()){
        std::cout << "other_params is empty" << std::endl;
      }
      else{
        std::cout << "other_params is not empty" << std::endl;
        if(u_params == other_params){
          std::cout << "u_params & other_params are equal" << std::endl;
        }
        else{
          std::cout << "u_params & other_params are NOT equal" << std::endl;
        }
      }

    }
    else{
      std::cout << "u_params could not be persisted!!!" << std::endl;
    }         

    std::cout << "ended hcro molecule experiment++" << std::endl;
  } catch (const std::exception& e) {
    std::cout << "failed@hcro molecule experiment++:" << e.what() << std::endl;
    return 0;
  }  
  
  std::cout << "fin" << std::endl;
  return 0;
}
