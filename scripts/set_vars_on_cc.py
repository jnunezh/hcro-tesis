# encoding=utf8
# this script, given an json_filepath, a cpp filepath & an uid; said cpp file's vars get replaced by values from such json file
# invoked, for example, like this:
# >python set_vars_on_cpp.py -c "/home/alexander/Documents/hcro_tesis_tests/Utils.h" -j "/home/alexander/Documents/hcro-tesis/data/created_params.json" -u "uid_created_123456"

# such fields could be {M, P, E} or their long values such as {num_tasks, num_procs, num_edges}, where M => tasks, P => processors, E => edges, R=> relatives of all nodes

import time
import sys
import getopt
import json
import re

def get_replacements(entry_uid, json_filepath):
    regexps = {
        "num_tasks": {"regexp": r"#define M \d+", "format": "#define M %s", "value": ""},
        "num_procs": {"regexp": r"#define P \d+", "format": "#define P %s", "value": ""},
        "num_edges": {"regexp": r"#define E \d+", "format": "#define E %s", "value": ""},
    }
    ret_copy = regexps.copy()
    with open(json_filepath, 'r') as fp:
        obj = json.load(fp)
        entry = [e for e in obj["entries"] if e["uid"] == entry_uid ][0]
        for key, value in regexps.iteritems():
            ret_copy[key]["value"] = entry[key]
    return ret_copy

def main(argv):
    
    cpp_filepath = None
    json_filepath = None
    entry_uid = None
    reload(sys)
    sys.setdefaultencoding('utf8')

    try:
        opts, args = getopt.getopt(argv,"c:j:u:",["cppath=", "jsonpath=", "uid="])
    except getopt.GetoptError:
        print '<script>.py -c <cppath> -j <jsonpath> -u <uid>'
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-c", "--cpp"):
            cpp_filepath = arg
        elif opt in ("-j", "--json"):
            json_filepath = arg
        elif opt in ("-u", "--uid"):
            entry_uid = arg

    if cpp_filepath is None or json_filepath is None or entry_uid is None:
        print '<script>.py -c <cppath> -j <jsonpath> -u <uid>'
        sys.exit(2)
    
    start = time.time()

    text = None

    with open(cpp_filepath, 'r') as content_file:
        text = content_file.read()

    if text is not None:
        replacements = get_replacements(entry_uid, json_filepath)
        for key, value in replacements.iteritems():
            r = re.compile(value["regexp"])
            output_text, replace_count = re.subn(r, value["format"] % value["value"], text)
            text = output_text

        with open(cpp_filepath, 'w') as output_file:
            output_file.seek(0)
            output_file.write(text)
    
    end = time.time()
    print('program finished on %f mins' % ((end - start) / 60))

if __name__ == "__main__":
    main(sys.argv[1:])