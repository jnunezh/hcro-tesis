/*
 * File:   CPOPAlgorithm.cpp
 * Author: Alexander
 *
 * Created on 10 de junio de 2015, 06:44 PM
 */

#include "CPOPAlgorithm.h"

cpop_algorithm_t::cpop_algorithm_t() {}

cpop_algorithm_t::cpop_algorithm_t(const cpop_algorithm_t& orig) {}

cpop_algorithm_t::~cpop_algorithm_t() {}

schedule_t cpop_algorithm_t::old_run() {
  schedule_t s;
  time_unit_t makespan = default_zero_value<time_unit_t>();
  const hms_param_t& ref_param = get_param();
  proc_t pid, min_proc, num_procs = ref_param.get_num_procs();
  time_unit_t ast, eft, min_eft, min_ast;
  std::vector<task_t>::const_iterator curr, end;
  std::vector<std::set<task_t> > spredecessors =
      ref_param.predecessors_as_vector_of_sets();
  s.sched.resize(num_procs);

  std::vector<proc_t> proc_assignmts(ref_param.get_num_tasks());
  std::vector<time_unit_t> afts(ref_param.get_num_tasks());
  crit_path_out_arg_t arg;
  ref_param.to_hetero_critical_path(arg);

  std::list<task_t> topolist = ref_param.to_sorted_topo_list();
  std::priority_queue<std::pair<task_t, time_unit_t>,
                      std::vector<std::pair<task_t, time_unit_t> >,
                      desc_second_cmp_t<task_t, time_unit_t> >
      ms; /*ordenarlos por priority,al top verificar si tiene sus antepasados
             llamados segun launched,si si es el elegido*/
  ms.push(std::pair<task_t, time_unit_t>(*(topolist.begin()),
                                        arg.priorities[*(topolist.begin())]));

  std::set<task_t> cp_set(arg.critical_path.begin(), arg.critical_path.end());
  task_t tid;
  std::vector<std::pair<task_t, time_unit_t> >::const_iterator trial_it, min_trial_it;
  bool r;
  while (!ms.empty()) {
    tid = ms.top().first;
    if (cp_set.find(tid) != cp_set.end()) {
      eft = plus_infinity<time_unit_t>();
      min_eft = plus_infinity<time_unit_t>();
      pid = arg.cpp; 
      ref_param.try_insertion_policy(
        tid,
        pid,
        afts,
        proc_assignmts,
        trial_it,
        s.sched[pid],
        min_eft,
        ast,
        eft);
      proc_assignmts[tid] = pid;
      afts[tid] = eft;
      ref_param.assign_task_to_processor(
        tid,
        pid,
        trial_it,
        ast,
        s);
      if (eft > makespan){
        makespan = eft;
      }
    }
    else {
      min_eft = plus_infinity<time_unit_t>();
      for (pid = 0; pid < num_procs; ++pid) {
        eft = plus_infinity<time_unit_t>();
        if(!ref_param.try_insertion_policy(
          tid,
          pid,
          afts,
          proc_assignmts,
          trial_it,
          s.sched[pid],
          min_eft,
          ast,
          eft)){
          continue;
        }

        if (eft < min_eft) {
          min_eft = eft;
          min_ast = ast;
          min_proc = pid;
          min_trial_it = trial_it;
        }
      }
      
      ref_param.assign_task_to_processor(
        tid,
        min_proc,
        min_trial_it,
        min_ast,
        s);
      proc_assignmts[tid] = min_proc;
      afts[tid] = min_eft;
      if (min_eft > makespan){
        makespan = min_eft;
      }
    }
    ms.pop();
    ref_param.sorted_successors(tid, curr, end);
    for (; curr != end; ++curr) {
      std::set<task_t>& ref = spredecessors[*curr];
      ref.erase(tid);
      if (!(ref.size())) {
        ms.push(std::pair<task_t, time_unit_t>(*curr, arg.priorities[*curr]));
      }
    }
  }
  s.makespan = makespan;
  return s;
}

schedule_t cpop_algorithm_t::run() {
  schedule_t s;
  time_unit_t makespan = default_zero_value<time_unit_t>();
  const hms_param_t& ref_param = get_param();
  proc_t pid, min_proc, num_procs = ref_param.get_num_procs();
  time_unit_t ast, eft, min_eft, min_ast;
  std::vector<max_entry_t> rest_runways_maxs;
  std::vector<task_t>::const_iterator curr, end;
  std::vector<std::set<task_t> > spredecessors =
      ref_param.predecessors_as_vector_of_sets();
  s.sched.resize(num_procs);
  rest_runways_maxs.resize(ref_param.get_num_tasks());
  crit_path_out_arg_t arg;
  ref_param.to_hetero_critical_path(arg);

  std::list<task_t> topolist = ref_param.to_sorted_topo_list();
  std::priority_queue<std::pair<task_t, time_unit_t>,
                      std::vector<std::pair<task_t, time_unit_t> >,
                      desc_second_cmp_t<task_t, time_unit_t> >
      ms; /*ordenarlos por priority,al top verificar si tiene sus antepasados
             llamados segun launched,si si es el elegido*/
  ms.push(std::pair<task_t, time_unit_t>(*(topolist.begin()),
                                        arg.priorities[*(topolist.begin())]));

  std::set<task_t> cp_set(arg.critical_path.begin(), arg.critical_path.end());
  task_t tid;
  std::vector<std::pair<task_t, time_unit_t> >::const_iterator trial_it, min_trial_it;
  bool r;
  while (!ms.empty()) {
    tid = ms.top().first;
    if (cp_set.find(tid) != cp_set.end()) {
      eft = plus_infinity<time_unit_t>();
      min_eft = plus_infinity<time_unit_t>();
      pid = arg.cpp; 
      ref_param.try_insertion_policy(
        tid,
        pid,
        rest_runways_maxs[tid],
        trial_it,
        s.sched[pid],
        min_eft,
        ast,
        eft);

      ref_param.update_rest_runways_maxs(
        rest_runways_maxs,
        tid,
        pid,
        eft);

      ref_param.assign_task_to_processor(
        tid,
        pid,
        trial_it,
        ast,
        s);
      if (eft > makespan){
        makespan = eft;
      }
    }
    else {
      min_eft = plus_infinity<time_unit_t>();
      for (pid = 0; pid < num_procs; ++pid) {
        eft = plus_infinity<time_unit_t>();
        if(!ref_param.try_insertion_policy(
          tid,
          pid,
          rest_runways_maxs[tid],
          trial_it,
          s.sched[pid],
          min_eft,
          ast,
          eft)){
          continue;
        }

        if (eft < min_eft) {
          min_eft = eft;
          min_ast = ast;
          min_proc = pid;
          min_trial_it = trial_it;
        }
      }
      ref_param.update_rest_runways_maxs(
        rest_runways_maxs,
        tid,
        min_proc,
        min_eft);

      ref_param.assign_task_to_processor(
        tid,
        min_proc,
        min_trial_it,
        min_ast,
        s);


      if (min_eft > makespan){
        makespan = min_eft;
      }
    }
    ms.pop();
    ref_param.sorted_successors(tid, curr, end);
    for (; curr != end; ++curr) {
      std::set<task_t>& ref = spredecessors[*curr];
      ref.erase(tid);
      if (!(ref.size())) {
        ms.push(std::pair<task_t, time_unit_t>(*curr, arg.priorities[*curr]));
      }
    }
  }
  s.makespan = makespan;
  return s;
}

