/*
 * File:   main.cpp
 * Author: Alexander
 *
 * Created on 21 de mayo de 2015, 01:56 PM
 */

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <list>
#include <sstream>

#include "BHEFTAlgorithm.h"
#include "CPOPAlgorithm.h"
#include "HCROAlgorithm.h"
#include "Priorities.h"
#include "file_persist.h"
using namespace std;

int main(int argc, char** argv) {
  std::srand(unsigned(std::time(0)));

#ifdef CPU_MULTITHREADED
  std::cout << "==================[Multi-thread Mode]=======================" << std::endl;
#else
  std::cout << "==================[Single-thread Mode]=======================" << std::endl;
#endif

  /*TODO LIST: set priority vector;verify constraints of setnodes_info;(15 & 16
   * alfa y beta); minlocal correctness:null?,memory leaks? not initialized? pe
   * outdated:how? or better update pe everytime inner_molecule is
   * changed[better!!!]
   * ;*/
  b_rank_t bp;
  t_rank_t tp;  // a,t_rank_t tp;
  l_rank_t lp;
  std::vector<priority_cmp_t<float> > priorities(3);
  priorities[0].set_criterion(&bp);
  priorities[1].set_criterion(&tp);
  priorities[2].set_criterion(&lp);

  std::string uid = "uid_nu_123456";
  std::vector<hcro_molecule_t> model_molecs;
  hcro_molecule_t other_molec;
  hms_param_t custom_param;
  custom_param = hms_params_from_json_file("data/nu_params.json", uid);

  if(custom_param.is_empty()){
    custom_param = hms_param_t::random_generate(5, 30, 0.4f, 1.5f, 9.5f, 1.0f, 10.0f);
    auto res = persist_hms_params_to_json_file("data/nu_params.json", custom_param, uid);
    std::cout << "persist_hms_params_to_json_file => res.first:" << res.first << " res.second:" << res.second << std::endl; 
    hms_param_t param_b = hms_params_from_json_file("data/nu_params.json", uid);
    std::cout << "param_b empty?:"<< param_b.is_empty() << std::endl;
    std::cout << "param == param_b?:" << (custom_param == param_b) << std::endl;
  }
  

  std::cout << "custom_param.edges==>" << custom_param.get_num_edges() << std::endl;

  std::cout << custom_param << std::endl;

  try {
    hcro_algorithm_t alg1(200,   // num_iterations
                       3,     // initial_num_groups : aka groups of sections
                       8,     // max_num_groups
                       30,    // section_amount aka elements per section
                       0.5f,  // percent elements per section initially created
                       7,     // num_candidates
                       priorities,  // priorities
                       2000.0f,     // buffer_init_val
                       0.0f,        // mu
                       1.0f,        // sigma
                       0.5f,        // collision_rate
                       200,         // alfa:char
                       100.0f,      // beta
                       10000.0f,    // ke_init
                       0.5f         // ke_loss_rate
    );

    alg1.set_param(&custom_param);

    hcro_molecule_t molec = hcro_molecule_t::random_generate(
      500,
      10000,
      200.0f,
      500.0f,
      250.0f,
      1000.0f,
      true,
      true);

    std::cout << "-----------------[c-readable-model-molecs]---------------------" << std::endl;

    int i;
    bool molecs_persisted = false;
    {
      model_molecs = hcro_molecule_t::generate_model_molecules(priorities);
      for(hcro_molecule_t& m : model_molecs){
        m.calculate_pe(); //to avoid trash values as pe
        m.set_inner_min_default();
      }
      i = rand_inclusive(0, model_molecs.size() - 1);
      molec = model_molecs[i];

      if(persist_molecules_to_c_readable_file("data/model_molecs.txt", model_molecs)){
        std::cout << "model molecs were persisted!!!" << std::endl;     
        molecs_persisted = true;
      }
      else{
        std::cout << "model molecs could not be persisted!!!" << std::endl;
      }      
    }

    if(molecs_persisted){
      std::vector<hcro_molecule_t> molecs = molecules_from_c_readable_file("data/model_molecs.txt");
      other_molec = molecs[i];
      
      if(other_molec.is_empty()){
        std::cout << "other_molec is empty" << std::endl;
      }
      else{
        std::cout << "other_molec is not empty" << std::endl;
        if(molec == other_molec){
          std::cout << "molec & other_molec are equal" << std::endl;
        }
        else{
          std::cout << "molec & other_molec are NOT equal" << std::endl;
        }
      }
    }
    std::cout << "-----------------[c-readable-molecs]---------------------" << std::endl;
    {
      std::vector<hcro_molecule_t> generated_molecs(100);
      for(hcro_molecule_t& m : generated_molecs){
          m = hcro_molecule_t::random_generate(
            500,
            10000,
            200.0f,
            500.0f,
            250.0f,
            1000.0f,
            true,
            true);
      }
      
      i = rand_inclusive(0, generated_molecs.size() - 1);
      molec = generated_molecs[i];

      if(persist_molecules_to_c_readable_file("data/molecs.txt", generated_molecs)){
        std::cout << "molecs were persisted!!!" << std::endl;     
        molecs_persisted = true;
      }
      else{
        std::cout << "molecs could not be persisted!!!" << std::endl;
      }      
    }

    if(molecs_persisted){
      std::vector<hcro_molecule_t> molecs = molecules_from_c_readable_file("data/molecs.txt");
      other_molec = molecs[i];
      
      if(other_molec.is_empty()){
        std::cout << "other_molec is empty" << std::endl;
      }
      else{
        std::cout << "other_molec is not empty" << std::endl;
        if(molec == other_molec){
          std::cout << "molec & other_molec are equal" << std::endl;
        }
        else{
          std::cout << "molec & other_molec are NOT equal" << std::endl;
        }
      }
    }
    std::cout << "-----------------[c-readable-unified-params]-----------" << std::endl;

    unified_params_t<default_traits_t> u_params = custom_param.to_unified_params_t<default_traits_t>(
      alg1.get_buffer_value(),
      alg1.get_mu(),
      alg1.get_sigma(),
      alg1.get_collision_rate(),
      alg1.get_alpha(),
      alg1.get_beta(),
      alg1.get_ke_init_value(),
      alg1.get_ke_loss_rate(),
      true,
      get_curr_epoch_ns());

    if(persist_params_to_c_readable_file("data/params.txt", u_params)){
      std::cout << "params were persisted!!!" << std::endl;     
      
      unified_params_t<default_traits_t> other_params = params_from_c_readable_file("data/params.txt");

      if(other_params.is_empty()){
        std::cout << "other_params is empty" << std::endl;
      }
      else{
        std::cout << "other_params is not empty" << std::endl;
        if(u_params == other_params){
          std::cout << "u_params & other_params are equal" << std::endl;
        }
        else{
          std::cout << "u_params & other_params are NOT equal" << std::endl;
        }
      }

    }
    else{
      std::cout << "u_params could not be persisted!!!" << std::endl;
    }         

    std::cout << "ended hcro molecule experiment++" << std::endl;
  } catch (const std::exception& e) {
    std::cout << "failed@hcro molecule experiment++:" << e.what() << std::endl;
    return 0;
  }  
  
  std::cout << "fin" << std::endl;
  return 0;
}
