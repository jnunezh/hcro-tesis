/*
 * File:   main.cpp
 * Author: Alexander
 *
 * Created on 21 de mayo de 2015, 01:56 PM
 */

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <list>
#include <sstream>
#include <map>

#include "BHEFTAlgorithm.h"
#include "CPOPAlgorithm.h"
#include "HCROAlgorithm.h"
#include "Priorities.h"
#include "Stats.h"
#include "file_persist.h"

using namespace std;


void place_into_proper_slot(std::map<float, float>& m, float pctg){

    for(std::map<float, float>::iterator iter = m.begin(); iter != m.end(); ++iter) {
        if(iter->first > pctg){
          (iter->second) += 1.0;
          break;
        }
    }
}

int main(int argc, char** argv) {
  std::srand(unsigned(std::time(0)));

#ifdef CPU_MULTITHREADED
  std::cout << "==================[Multi-thread Mode]=======================" << std::endl;
#else
  std::cout << "==================[Single-thread Mode]=======================" << std::endl;
#endif

  /*TODO LIST: set priority vector;verify constraints of setnodes_info;(15 & 16
   * alfa y beta); minlocal correctness:null?,memory leaks? not initialized? pe
   * outdated:how? or better update pe everytime inner_molecule is
   * changed[better!!!]
   * ;*/
  b_rank_t bp;
  t_rank_t tp;  // a,t_rank_t tp;
  l_rank_t lp;
  std::vector<priority_cmp_t<float> > priorities(3);
  priorities[0].set_criterion(&bp);
  priorities[1].set_criterion(&tp);
  priorities[2].set_criterion(&lp);

  schedule_t s1;
  
  size_t selected_num_procs, selected_num_tasks, accum_sched_num = 0;
  time_unit_t comm_lb, comm_ub, weights_lb, weights_ub;
  float max_occupancy_pctg = 0.0f, selected_density, tmp;

  comm_lb = 1.5f;
  comm_ub = 9.5f;
  weights_lb = 1.0f;
  weights_ub = 10.0f;

  std::map<float, float> m{ 
    { 0.1, 0.0 },
    { 0.2, 0.0 },
    { 0.3, 0.0 },
    { 0.4, 0.0 },
    { 0.5, 0.0 },
    { 0.6, 0.0 },
    { 0.7, 0.0 },
    { 0.8, 0.0 },
    { 0.9, 0.0 },
    { 1.0, 0.0 }
  };

  for(float curr_density = 0.2f; curr_density <= 1.0f ; curr_density += 0.2f){

    for(size_t num_tasks = 10; num_tasks < 100; num_tasks *= 2){

      for(size_t num_procs = 4; num_procs < 20; num_procs *= 2){


        for(weights_lb = 100.0f; weights_lb < 20000.0f; weights_lb *= 10.0f){

          weights_ub = 10.0f * weights_lb;

          for(comm_lb = 200.0f; comm_lb < 21000.0f; comm_lb *= 10.0f){

            comm_ub = 10.0f * comm_lb;

            hms_param_t param = hms_param_t::random_generate2(
              num_procs,
              num_tasks,
              curr_density,
              comm_lb,
              comm_ub,
              weights_lb,
              weights_ub);  
            try {
              hcro_algorithm_t alg1(100,   // num_iterations
                                 3,     // initial_num_groups : aka groups of sections
                                 8,     // max_num_groups
                                 30,    // section_amount aka elements per section
                                 0.5f,  // percent elements per section initially created
                                 7,     // num_candidates
                                 priorities,  // priorities
                                 2000.0f,     // buffer_init_val
                                 0.0f,        // mu
                                 1.0f,        // sigma
                                 0.5f,        // collision_rate
                                 200,         // alfa:char
                                 100.0f,      // beta
                                 10000.0f,    // ke_init
                                 0.5f         // ke_loss_rate
              );
              // std::cout << "ended hcro A" << std::endl;
              alg1.set_param(&param);
              // std::cout << "ended hcro B" << std::endl;
              s1 = alg1.run();
              tmp = get_max_proc_runway_occupancy(s1);

              place_into_proper_slot(m, tmp);

              if(tmp > max_occupancy_pctg){
                max_occupancy_pctg = tmp;
                selected_density = curr_density;
                selected_num_tasks = num_tasks;
                selected_num_procs = num_procs;
              }

              ++accum_sched_num;
            }
            catch (const std::exception& e){
              std::cout << "failed@hcro:" << e.what() << std::endl;
              return 0;
            }
          }
        }  
      }
    }
  }

  /*
  max_occupancy_pctg:0.3
  selected_density:0.3
  selected_num_tasks:30
  selected_num_procs:20
  */
  std::cout << "max_occupancy_pctg:" << max_occupancy_pctg << std::endl;
  std::cout << "selected_density:" << selected_density << std::endl;
  std::cout << "selected_num_tasks:" << selected_num_tasks << std::endl;
  std::cout << "selected_num_procs:" << selected_num_procs << std::endl;
  
  for(auto& entry : m){
    entry.second = entry.second / static_cast<float>(accum_sched_num);
    std::cout << entry.first << "=>" << entry.second << std::endl;
  }

  std::cout << "fin" << std::endl;
  return 0;
}
