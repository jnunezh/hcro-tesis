#ifndef STATS_H
#define STATS_H
#include "Utils.h"

std::string procs_runway_population_pctgs(
	const schedule_t& s,
	size_t num_tasks);

float get_max_proc_runway_occupancy(const schedule_t& s);

#endif /* STATS_H */