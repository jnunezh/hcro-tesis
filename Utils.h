/*
 * File:   Utils.h
 * Author: Alexander
 *
 * Created on 21 de mayo de 2015, 01:58 PM
 */

#ifndef UTILS_H
#define UTILS_H
#include <random>
#include <algorithm>
#include <cassert>
#include <cstdarg>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <limits>
#include <list>
#include <map>
#include <numeric>
#include <set>
#include <sstream>
#include <string>
#include <cstring>
#include <vector>
#include <queue>
#include <tuple>
#include <functional>
#include <utility>
#include <bitset>
#include <chrono>

typedef int task_t;
typedef int proc_t;
typedef float time_unit_t;

struct schedule_t;

struct crit_path_out_arg_t {
  std::list<task_t> critical_path;
  std::vector<float> priorities;
  proc_t cpp;
};

struct reaction_min_t{
  int molecule_index;
  time_unit_t makespan;
  bool ml_min;
};

struct ordered_excluded_t{
  std::vector<size_t>* candidates;
  std::vector<size_t>::const_iterator geq_it;
  bool solution_found;
};


template <class TContainer>
void print(const TContainer& ls) {
  for (typename TContainer::const_iterator it = ls.begin(); it != ls.end();
       ++it) {
    std::cout << *it << " ";
  }
  std::cout << std::endl;
}


template <class TContainer>
void iprint(const TContainer& ls) {
  size_t i = 0;
  for (typename TContainer::const_iterator it = ls.begin(); it != ls.end();
       ++it, ++i) {
    std::cout << i << "->" << *it << std::endl;
  }
}

template <class T, class Cont, class Cmp>
void print_queue(std::priority_queue<T, Cont, Cmp> pq){
	while (!pq.empty()){
    std::cout << pq.top() << std::endl;
		pq.pop();
	}
}
//----------------------------------------------------------------------------------------
template<class T> 
T sto_type(const std::string& str);

template<>
int sto_type<int>(const std::string& str);

template<>
short sto_type<short>(const std::string& str);

template<>
unsigned short sto_type<unsigned short>(const std::string& str);

template<>
long sto_type<long>(const std::string& str);

template<>
long long sto_type<long long>(const std::string& str);

template<>
unsigned long sto_type<unsigned long>(const std::string& str);

template<>
unsigned long long sto_type<unsigned long long>(const std::string& str);

template<>
float sto_type<float>(const std::string& str);

template<>
double sto_type<double>(const std::string& str);

template<>
long double sto_type<long double>(const std::string& str);


template<typename T>
std::vector<T> split_str_into_vector(std::string str, const std::string& delimiter){
  std::vector<T> res;
  size_t pos;
  std::string token;
  while ((pos = str.find(delimiter)) != std::string::npos){
      token = str.substr(0, pos);
      res.push_back(sto_type<T>(token)); 
      str.erase(0, pos + delimiter.length());
  }
  return res;
}

//--------------------------------TupleIntoStream-----------------------------------------

template<std::size_t> struct int_{};

template <class StreamType, class Tuple, size_t Pos>
StreamType& print_tuple(StreamType& out, const Tuple& t, int_<Pos> ) {
  out << std::get< std::tuple_size<Tuple>::value-Pos >(t) << ',';
  return print_tuple(out, t, int_<Pos-1>());
}

template <class StreamType, class Tuple>
StreamType& print_tuple(StreamType& out, const Tuple& t, int_<1> ) {
  out << std::get<std::tuple_size<Tuple>::value-1>(t);
  return out;
}

template <class StreamType, class... Args>
StreamType& operator<<(StreamType& out, const std::tuple<Args...>& t) {
  out << '('; 
  print_tuple(out, t, int_<sizeof...(Args)>()); 
  out << ')';
  return out;
}

#define add_entry_to_file(first, out, ...) \
{ \
    if(first){ \
     const char* args = #__VA_ARGS__  ; \
     out << args << "\n"; \
    } \
    first = false; \
    auto t = std::make_tuple(__VA_ARGS__); \
    out << t << "\n"; \
} \

//----------------------------------MeasuringTime------------------------------------------

#define print_elapsed_ms(...) \
{ \
    std::clock_t _____begin = clock(); \
    {\
     __VA_ARGS__;\
    } \
    std::clock_t _____end = clock(); \
    double _____elapsed_ms = double((_____end - _____begin) * 1000) / CLOCKS_PER_SEC; \
    std::cout << "diff(ms):" << _____elapsed_ms << std::endl; \
} \

#define assign_elapsed_ms(var, ...) \
{ \
    std::clock_t _____begin = clock(); \
    {\
     __VA_ARGS__;\
    } \
    std::clock_t _____end = clock(); \
    var = double((_____end - _____begin) * 1000) / CLOCKS_PER_SEC; \
} \

//----------------------------------UtilFunctions------------------------------------------

template <typename TDecimal>
inline bool almost_equal(TDecimal x, TDecimal y, int ulp = 2) {
  return abs(x - y) <
             std::numeric_limits<TDecimal>::epsilon() * abs(x + y) * ulp ||
         abs(x - y) < std::numeric_limits<TDecimal>::min();
}

inline bool almost_equal(unsigned char x, unsigned char y, int ulp = 2) {
  return x == y;
}

inline bool almost_equal(char x, char y, int ulp = 2) {
  return x == y;
}

inline bool almost_equal(unsigned int x, unsigned int y, int ulp = 2) {
  return x == y;
}

inline bool almost_equal(int x, int y, int ulp = 2) {
  return x == y;
}

inline bool almost_equal(short x, short y, int ulp = 2) {
  return x == y;
}

inline bool almost_equal(unsigned short x, unsigned short y, int ulp = 2) {
  return x == y;
}

inline bool almost_equal(long x, long y, int ulp = 2) {
  return x == y;
}

inline bool almost_equal(unsigned long x, unsigned long y, int ulp = 2) {
  return x == y;
}

inline unsigned long get_curr_epoch_ns(){
  return std::chrono::system_clock::now().time_since_epoch() / std::chrono::milliseconds(1);
}

/*

===================[old implementatio]====================

template <typename TDecimal>
inline TDecimal rand_inclusive(TDecimal lb, TDecimal ub){
  assert(lb <= ub);
  return lb + ((TDecimal)rand() / RAND_MAX) * (ub - lb);
}

inline int rand_inclusive(int lb, int ub){
  assert(lb <= ub);
  return rand() % ((ub + 1) - lb) + lb;
}
*/

template <typename TDecimal>
inline TDecimal rand_inclusive(TDecimal lb, TDecimal ub){
  assert(lb <= ub);
#ifndef CPU_MULTITHREADED
  static std::random_device rd;
#else
  thread_local std::random_device rd;
#endif
  std::mt19937 gen(rd());
  TDecimal coeff, incl_lb = 0.0, excl_ub = 1.0 +  std::numeric_limits<TDecimal>::epsilon();
  std::uniform_real_distribution<> dis(incl_lb, excl_ub);
  coeff = dis(gen);
  return lb +  coeff * (ub - lb);
}

template <typename IterType>
void custom_random_shuffle(IterType begin, IterType end){
    std::random_device rd;
    std::mt19937 g(rd());
    std::shuffle(begin, end, g);
}

inline int rand_inclusive(int lb, int ub){
  double coeff = rand_inclusive(0.0, 1.0);  
  int diff = ub - lb;
  return lb +  static_cast<int>(coeff * static_cast<double>(diff));
}

inline short rand_inclusive(short lb, short ub) {
  assert(lb <= ub);
  return rand_inclusive((int) lb, (int) ub);
}

inline unsigned short rand_inclusive(unsigned short lb, unsigned short ub) {
  assert(lb <= ub);
  return rand_inclusive((int) lb, (int) ub);
}

inline char rand_inclusive(char lb, char ub) {
  assert(lb <= ub);
  return rand_inclusive((int) lb, (int) ub);
}

inline unsigned char rand_inclusive(unsigned char lb, unsigned char ub) {
  assert(lb <= ub);
  return rand_inclusive((int) lb, (int) ub);
}

template <typename T>
inline T default_zero_value() {
  T obj = T();
  return obj;
}


template <typename T>
inline T plus_infinity() {
  return std::numeric_limits<T>::max();
}

template<typename T>
T value_wo_first_bit(T value){
  int k = sizeof(T) * 8 - 1;
  return (value & ~(1 << k));
}

template<typename T>
void set_on_first_bit(T* ptr){  //turn on leftmost bit 
  int k = sizeof(T) * 8 - 1;
  T n = *ptr;
  n = ((1 << k) | n);
  *ptr = n;
}

template<typename T>
bool get_first_bit(T value){  //leftmost bit
  int k = (sizeof(T) * 8) - 1;
  return (value & ( 1 << k )) >> k;
}

template<typename T>
std::string as_binary_string(T value){
  std::bitset<sizeof(T) * 8> x(value);
  return x.to_string();
}


typedef struct max_entry{
  time_unit_t first_max, second_max;
  proc_t fm_pid, sm_pid;

  max_entry(){
    first_max = second_max = default_zero_value<time_unit_t>();
    fm_pid = sm_pid = plus_infinity<proc_t>();
  }

} max_entry_t;


//-----------------------------------------------------------------------------------------

class hms_param_t;

class priority_criterion_t {
 protected:
  bool asc;

 public:
  virtual void generate(const hms_param_t& param, std::vector<float>& buffer) = 0;
  inline void set_ascending_order(bool value) { asc = value; }
  inline bool get_ascending_order() const { return asc; }
};

template <typename OutType>
class priority_cmp_t {
  std::vector<OutType> priorities_buff;
  priority_criterion_t* criterion;

 public:
  inline void set_criterion(priority_criterion_t* ptr) { criterion = ptr; }
  inline priority_criterion_t* get_criterion() { return criterion; }
  virtual void generate(const hms_param_t& param, size_t buff_size) {
    assert(criterion);
    priorities_buff.resize(buff_size);
    criterion->generate(param, priorities_buff);
  }
  bool operator()(task_t first, task_t second) {
    OutType& fd = priorities_buff.at(first);
    OutType& sd = priorities_buff.at(second);
    if (criterion->get_ascending_order()) return fd < sd;
    return fd > sd;
  }
};

//------------------------------------------------------------------------------------------

struct node_info_t {
  std::vector<time_unit_t> task_weights;
  std::vector<std::pair<task_t, time_unit_t> > descendents;
  task_t tid;
};

struct default_traits_t{
  typedef float cont_float;
  typedef short cont_short;
  typedef bool cont_bool;
  typedef unsigned long cont_unsigned_long;
  typedef size_t cont_int;
  typedef task_t cont_task_t;
  typedef time_unit_t cont_time_unit_t;  
};

template<typename TCont = default_traits_t>
struct unified_params_t{
  
  typedef typename TCont::cont_float up_float;
  typedef typename TCont::cont_short up_short;
  typedef typename TCont::cont_bool up_bool;
  typedef typename TCont::cont_unsigned_long up_unsigned_long;
  typedef typename TCont::cont_int up_int;
  typedef typename TCont::cont_task_t up_task_t;
  typedef typename TCont::cont_time_unit_t up_time_unit_t;

  up_float collision_rate;
  up_float ke_loss_rate;
  up_float ke_init;
  up_float beta;
  up_float buffer_value;
  up_float mu;
  up_float sigma;
  up_short alpha;
  up_bool init_pop;
  up_unsigned_long seed;

  std::vector<std::pair<up_int, up_int> > limit_offsets; /*index =>(succesors_offset,predecessors_offset)*/
  std::vector<std::pair<up_task_t, up_time_unit_t> > comm_successors, comm_predecessors; //sorted by task_t
  std::vector<up_time_unit_t> weights_matrix;

  bool is_empty() const{
  return (
    this->limit_offsets.size() < 1 || 
    this->comm_successors.size() < 1 ||
    this->comm_predecessors.size() < 1 ||
    this->weights_matrix.size() < 1);
  }

  bool operator==(const unified_params_t<TCont>& other) const{

    if(this->limit_offsets.size() != other.limit_offsets.size()){
      return false;
    }

    typename std::vector<std::pair<up_int, up_int> >::const_iterator off_it = other.limit_offsets.begin();

    for(const std::pair<up_int, up_int>& p : this->limit_offsets){
      if(p.first != off_it->first){
        return false;
      }
      if(p.second != off_it->second){
        return false;
      }
      ++off_it;
    }

    if(this->comm_successors.size() != other.comm_successors.size()){
      return false;
    }

    typename std::vector<std::pair<up_task_t, up_time_unit_t> >::const_iterator rel_it = other.comm_successors.begin();

    for(const std::pair<up_task_t, up_time_unit_t>& p : this->comm_successors){
      if(!almost_equal(p.first, rel_it->first)){
        return false;
      }
      if(!almost_equal(p.second, rel_it->second)){
        return false;
      }
      ++rel_it;
    }  

    if(this->comm_predecessors.size() != other.comm_predecessors.size()){
      return false;
    }

    rel_it = other.comm_predecessors.begin();

    for(const std::pair<up_task_t, up_time_unit_t>& p : this->comm_predecessors){
      if(!almost_equal(p.first, rel_it->first)){
        return false;
      }
      if(!almost_equal(p.second, rel_it->second)){
        return false;
      }
      ++rel_it;
    }

    if(this->weights_matrix.size() != other.weights_matrix.size()){
      return false;
    }

    typename std::vector<up_time_unit_t>::const_iterator wm_it = other.weights_matrix.begin();

    for(up_time_unit_t e : this->weights_matrix){
      if(!almost_equal(e, *wm_it)){
        return false;
      }
      ++wm_it;
    }

    if(!almost_equal(this->collision_rate, other.collision_rate)){
      return false;
    }

    if(!almost_equal(this->ke_loss_rate, other.ke_loss_rate)){
      return false;
    }

    if(!almost_equal(this->ke_init, other.ke_init)){
      return false;
    }

    if(!almost_equal(this->beta, other.beta)){
      return false;
    }

    if(!almost_equal(this->buffer_value, other.buffer_value)){
      return false;
    }

    if(!almost_equal(this->mu, other.mu)){
      return false;
    }

    if(!almost_equal(this->sigma, other.sigma)){
      return false;
    }

    if(!almost_equal(this->alpha, other.alpha)){
      return false;
    }

    if(!almost_equal(this->seed, other.seed)){
      return false;
    }

    if(this->init_pop != other.init_pop){
      return false;
    }

    return true;
  }

  bool operator!=(const unified_params_t<TCont>& other) const{
    return !(this->operator==(other));
  }
};

class hms_param_t {
  size_t num_procs, num_tasks, num_edges;
  std::vector<task_t> successors, predecessors; /*ordered*/
  std::vector<std::pair<size_t, size_t> >
      limit_offsets; /*index =>(succesors_offset,predecessors_offset)*/
  std::vector<time_unit_t> comm_successors, comm_predecessors,
      weights_matrix;  // comm_successors: [tid => predecessors_times];
                       // weights_matrix: [tid => times_per_proc] access via
                       // formula: tid * num_procs
  //  std::vector<Timespan> startup_costs; //Cs
  //  std::map<std::pair<ProcId,ProcId>,Timespan> proc_comm_map;
  void topological_sort_util(task_t v, std::vector<bool>& visited,
                           std::list<task_t>& s) const;

 public:
  hms_param_t();
  hms_param_t(size_t ntasks, size_t nprocs);
  hms_param_t(const hms_param_t& other);
  hms_param_t& operator=(const hms_param_t& other);

  inline void set_num_procs(size_t procs){
    num_procs = procs;
  }

  bool operator==(const hms_param_t& other) const;

  inline bool operator!=(const hms_param_t& other) const {
    return !(this->operator==(other));
  }

  inline bool is_empty() const{
    return (this->num_tasks == 0 || this->num_procs == 0);
  }

  inline void set_num_tasks(size_t tasks){
    num_tasks = tasks;
  }

  inline void set_num_edges(size_t value){ this->num_edges = value; }

  inline size_t get_num_procs() const { return num_procs; }

  inline size_t get_num_tasks() const { return num_tasks; }

  inline size_t get_num_edges() const { return num_edges; }

  inline std::vector<std::pair<size_t, size_t> >& get_limit_offsets(){
    return limit_offsets;
  }

  inline const std::vector<std::pair<size_t, size_t> >& get_limit_offsets() const {
    return limit_offsets;
  }

  inline std::vector<time_unit_t>& get_comm_successors() {
    return comm_successors;
  }

  inline const std::vector<time_unit_t>& get_comm_successors() const {
    return comm_successors;
  }

  inline std::vector<time_unit_t>& get_weights_matrix() {
    return weights_matrix;
  }

  inline const std::vector<time_unit_t>& get_weights_matrix() const {
    return weights_matrix;
  }

  inline const std::vector<time_unit_t>& get_comm_predecessors() const {
    return comm_predecessors;
  }

  inline std::vector<time_unit_t>& get_comm_predecessors() {
    return comm_predecessors;
  }

  inline const std::vector<task_t>& get_predecessors() const {
    return predecessors;
  }

  inline std::vector<task_t>& get_predecessors() {
    return predecessors;
  }

  inline const std::vector<task_t>& get_successors() const {
    return successors;
  }

  inline std::vector<task_t>& get_successors() {
    return successors;
  }

  template<typename TCont = default_traits_t>
  unified_params_t<TCont> to_unified_params_t(
    float buffer_value,
    float mu,
    float sigma,
    float cl_rate,
    short param_alpha,
    float param_beta,
    float ke_init,
    float ke_lrate,
    bool init_pop,
    unsigned long seed) const{

    typename std::vector<std::pair<typename unified_params_t<TCont>::up_int, typename unified_params_t<TCont>::up_int> >::iterator lim_it;
    typename std::vector<std::pair<typename unified_params_t<TCont>::up_task_t, typename unified_params_t<TCont>::up_time_unit_t> >::iterator rel_it;
    typename std::vector<typename unified_params_t<TCont>::up_time_unit_t>::iterator elem_it;

    unified_params_t<TCont> res;
    res.collision_rate = cl_rate;
    res.ke_loss_rate = ke_lrate;
    res.ke_init = ke_init;
    res.beta = param_beta;
    res.buffer_value = buffer_value;
    res.mu = mu;
    res.sigma = sigma;
    res.alpha = param_alpha;
    res.init_pop = init_pop;
    res.seed = seed;

    res.limit_offsets.resize(this->get_limit_offsets().size());

    lim_it = res.limit_offsets.begin();

    for(const std::pair<size_t, size_t>& p : this->get_limit_offsets()){
      lim_it->first = p.first;
      lim_it->second = p.second;
      ++lim_it;
    }
    
    std::vector<std::pair<task_t, time_unit_t> > tmp;
    tmp = this->get_merged_comm_successors();
    res.comm_successors.resize(tmp.size());
    rel_it = res.comm_successors.begin();
    for(const std::pair<task_t, time_unit_t>& p : tmp){
      rel_it->first = p.first;
      rel_it->second = p.second;
      ++rel_it;
    }
    
    tmp = this->get_merged_comm_predecessors();
    res.comm_predecessors.resize(tmp.size());
    rel_it = res.comm_predecessors.begin();
    for(const std::pair<task_t, time_unit_t>& p : tmp){
      rel_it->first = p.first;
      rel_it->second = p.second;
      ++rel_it;
    }

    res.weights_matrix.resize(this->get_weights_matrix().size());
    elem_it = res.weights_matrix.begin();
    for(time_unit_t elem : this->get_weights_matrix()){
      *elem_it = elem;
      ++elem_it;
    }

    return res;
  }

  void set_nodes_info(const std::vector<node_info_t>& nodes_info);
  //  void setStartUpCosts(Timespan value,...);
  //  void setProcCommCosts(ProcId src,ProcId dest,Timespan ts,...);
  //  /*num_procs^2 tuples;OK*/

  std::list<task_t> to_sorted_topo_list() const;

  void to_hetero_critical_path(crit_path_out_arg_t& arg) const;

  int first_succesor_index_of(int task_index,
                         const std::vector<task_t>& imolecule) const;
  int last_predecessor_index_of(int task_index,
                           const std::vector<task_t>& imolecule) const;

  void sorted_successors(task_t tid,
                            std::vector<task_t>::const_iterator& begin,
                            std::vector<task_t>::const_iterator& end) const;
  void sorted_predecessors(task_t tid,
                              std::vector<task_t>::const_iterator& begin,
                              std::vector<task_t>::const_iterator& end) const;
  void comm_times_of_sorted_successors(
      task_t tid, std::vector<time_unit_t>::const_iterator& begin,
      std::vector<time_unit_t>::const_iterator& end) const;
  void comm_times_of_sorted_predecessors(
      task_t tid, std::vector<time_unit_t>::const_iterator& begin,
      std::vector<time_unit_t>::const_iterator& end) const;
  time_unit_t weight_at(task_t tid, proc_t pid) const;
  void weights_of(task_t tid, std::vector<time_unit_t>::const_iterator& begin,
                    std::vector<time_unit_t>::const_iterator& end) const;
  std::vector<std::set<task_t> > predecessors_as_vector_of_sets() const;
  std::vector<time_unit_t> to_avg_weights() const;
  std::vector<time_unit_t> get_min_weights() const;
  std::vector<time_unit_t> get_avg_communication() const;

  std::vector<std::pair<task_t, time_unit_t> > get_merged_comm_successors() const;
  std::vector<std::pair<task_t, time_unit_t> > get_merged_comm_predecessors() const;

  //false if not passed bound-test
  bool try_insertion_policy(
      task_t tid, proc_t pid,
      const std::vector<time_unit_t>& afts,
      const std::vector<proc_t>& proc_assignmts,
      std::vector<std::pair<task_t, time_unit_t> >::const_iterator& geq_it,
      const std::vector<std::pair<task_t, time_unit_t> >& processor_runway,
      const time_unit_t& min_eft,
      time_unit_t& ast,
      time_unit_t& eft) const;

  //false if not passed bound-test
  bool try_insertion_policy(
    task_t tid,
    proc_t pid,
    const max_entry_t& curr_entry,
    std::vector<std::pair<task_t, time_unit_t> >::const_iterator& geq_itask,
    const std::vector<std::pair<task_t, time_unit_t> >& processor_runway,
    const time_unit_t& min_eft,
    time_unit_t& ast,
    time_unit_t& eft) const;

  void assign_task_to_processor(
      task_t tid,
      proc_t pid,
      std::vector<std::pair<task_t, time_unit_t> >::const_iterator pos_it,
      const time_unit_t& ast,
      schedule_t& sched) const;

  void update_rest_runways_maxs(
      std::vector<max_entry_t>& rest_runways_maxs,
      task_t tid,
      proc_t pid,
      time_unit_t comm_less_ft) const;

  std::string to_string() const;

  std::string stringify_weights() const;

  static hms_param_t random_generate(size_t nprocs, size_t ntasks,
                                  float density, time_unit_t comm_lb,
                                  time_unit_t comm_ub,
                                  time_unit_t weights_lb,
                                  time_unit_t weights_ub); /*density [0,1]*/

  static hms_param_t random_generate2(size_t nprocs, size_t ntasks,
                                float density, time_unit_t comm_lb,
                                time_unit_t comm_ub,
                                time_unit_t weights_lb,
                                time_unit_t weights_ub); /*density [0,1]*/
};

std::ostream& operator<<(std::ostream& os, const hms_param_t& p);

std::ostream& operator<<(std::ostream& os, const max_entry_t& entry);

template <typename KeyType, typename ValueType>
struct asc_first_cmp_t {
  inline bool operator()(const std::pair<KeyType, ValueType>& a,
                         const std::pair<KeyType, ValueType>& b) const {
    return a.first < b.first;
  }
};
template <typename KeyType, typename ValueType>
struct desc_first_cmp_t {
  inline bool operator()(const std::pair<KeyType, ValueType>& a,
                         const std::pair<KeyType, ValueType>& b) const {
    return a.first > b.first;
  }
};
template <typename KeyType, typename ValueType>
struct asc_second_cmp_t {
  inline bool operator()(const std::pair<KeyType, ValueType>& a,
                         const std::pair<KeyType, ValueType>& b) const {
    return a.second > b.second;
  }
};
template <typename KeyType, typename ValueType>
struct desc_second_cmp_t {
  inline bool operator()(const std::pair<KeyType, ValueType>& a,
                         const std::pair<KeyType, ValueType>& b) const {
    return a.second < b.second;
  }
};

class scheduling_algorithm_t {
  const hms_param_t* param;

 public:
  inline const hms_param_t& get_param() const {
     assert(param);
     return *param;
  }

  inline void set_param(const hms_param_t* pms) { param = pms; }
  virtual schedule_t run() = 0; /*ptr ownership acquire here*/
};

struct schedule_t {
  typedef std::vector<std::vector<std::pair<task_t, time_unit_t> > > schedule_vector_t;
  schedule_vector_t sched; /*pidx=>[(tid,time),(tid,time),...]*/
  time_unit_t makespan;
  float calculate_slr(const hms_param_t& g) const;
  float calculate_speedup(const hms_param_t& g) const;
  void init(const hms_param_t& g);
  bool is_valid(const hms_param_t& g, std::string& invalid_msg) const;
  std::string to_string() const;
  bool operator==(const schedule_t& other) const;
  bool operator!=(const schedule_t& other) const;
  
  inline bool is_empty() const{
    return this->sched.size() == 0;
  }

  inline time_unit_t get_makespan() const{
    return this->makespan;
  }
  
  inline void set_makespan(time_unit_t value){
    this->makespan = value;
  }

  inline const schedule_vector_t& get_inner_schedule() const {
    return this->sched;
  }

  inline schedule_vector_t& get_inner_schedule(){
    return this->sched;
  }


};

struct mapping_ret_arg_t {
  size_t min_ms_idx;
  time_unit_t min_ms, sum_fitness;
  schedule_t* ptr_sched;
};

std::ostream& operator<<(std::ostream& os, const schedule_t& s);

template <typename T1, typename T2>
std::ostream& operator<<(std::ostream& os, const std::pair<T1, T2>& p) {
  return os << "(" << p.first << "," << p.second << ")";
}

std::ostream& operator<<(std::ostream& os, const node_info_t& n);

template <typename ConstIt>
void print_it(ConstIt curr, ConstIt end) {
  for (; curr != end; ++curr) {
    std::cout << *curr << std::endl;
  }
}

template<typename T, typename Pred>
typename std::vector<T>::iterator insert_into_sorted(
  std::vector<T>& v,
  const T& item,
  Pred pred){
    return v.insert(
      std::upper_bound(
        v.begin(),
        v.end(),
        item,
        pred),
      item);
}

//--------------[Union Find Set]-------------------------------------
//Useful to find belongness of an object regarding a nested group of sets

class union_find_data_struct_t {
  private:
    std::vector<int> id, sz;
    int cnt;

  public:

  // Create an empty union find data structure with N isolated sets.
  union_find_data_struct_t(int n){
      this->cnt = n;
      this->id.resize(n);
      this->sz.resize(n);
      for (int i = 0; i < n; ++i){
        this->id[i] = i;
        this->sz[i] = 1;
      } 
  }

  // Return the id of component corresponding to object p.
  int find(int p){
      int newp, root = p;
      while (root != this->id[root]){
        root = this->id[root];
      }
      while (p != root){
        newp = this->id[p];
        this->id[p] = root;
        p = newp; 
      }
      return root;
  }

  // Replace sets containing x and y with their union.
  void merge(int x, int y){
      int i = this->find(x);
      int j = this->find(y);
      if (i == j){
        return;
      }
      // make smaller root point to larger one
      if (this->sz[i] < this->sz[j]){ 
        this->id[i] = j;
        this->sz[j] += this->sz[i]; 
      }
      else {
        this->id[j] = i;
        this->sz[i] += this->sz[j]; 
      }
      --(this->cnt);
  }

  // Are objects x and y in the same set?
  bool connected(int x, int y){
    return this->find(x) == this->find(y);
  }
  
  // Return the number of disjoint sets.
  int count(){ 
    return this->cnt; 
  }
};


#endif /* UTILS_H */
