/*
 * File:   Priorities.cpp
 * Author: Alexander
 *
 * Created on 8 de noviembre de 2015, 10:31 PM
 */

#include "Priorities.h"

void l_rank_t::generate(const hms_param_t& param, std::vector<float>& buffer) /*(int:asc,sum:desc);finally ASC*/
{

  std::list<task_t> topolist = param.to_sorted_topo_list();
  int imax;
  std::vector<task_t>::const_iterator curr, end;
  std::vector<float> t_rank_ts(buffer.size());
  
  b_rank_t op;
  op.generate(param, buffer);

  t_rank_t oop;
  oop.generate(param, t_rank_ts);
  
  for (const task_t& tid : topolist) {
    imax = -1;
    param.sorted_predecessors(tid, curr, end);

    for (; curr != end; ++curr) {
      if (buffer.at(*curr) > imax) {
        imax = buffer.at(*curr);
      }
    }
    ++imax;
    buffer.at(tid) = imax + (1.0f / (buffer.at(tid) + t_rank_ts[tid] + 1.1f));
  }
  // std::cout<<"l_rank_t:"<<std::endl;
  // print(*ptr_vec);
  set_ascending_order(true);
}

void t_rank_t::generate(const hms_param_t& param, std::vector<float>& buffer) {
  
  std::list<task_t> topolist = param.to_sorted_topo_list();
  std::vector<time_unit_t> avg_weights = param.to_avg_weights();
  time_unit_t smax, tmp;
  std::vector<task_t>::const_iterator curr, end;
  std::vector<time_unit_t>::const_iterator ccurr, cend;
  for (const task_t& tid : topolist) {
    smax = 0.0;
    param.sorted_predecessors(tid, curr, end);
    param.comm_times_of_sorted_predecessors(tid, ccurr, cend);
    for (; curr != end; ++curr, ++ccurr) {
      tmp = avg_weights[*curr] + *ccurr + buffer.at(*curr);
      if (tmp > smax) {
        smax = tmp;
      }
    }
    // std::cout<<std::endl;
    buffer.at(tid) = smax;
  }
  //   std::cout<<"t_rank_t:"<<std::endl;
  //   print(*ptr_vec);
  set_ascending_order(true);
}

void b_rank_t::generate(const hms_param_t& param, std::vector<float>& buffer) {

  std::list<task_t> topolist = param.to_sorted_topo_list();
  std::vector<time_unit_t> avg_weights = param.to_avg_weights();
  time_unit_t smax, tmp;
  std::vector<task_t>::const_iterator curr, end;
  std::vector<time_unit_t>::const_iterator ccurr, cend;

  for (std::list<task_t>::const_reverse_iterator it = topolist.rbegin();
       it != topolist.rend(); ++it) {
    task_t tid = *it;
    smax = 0.0;
    param.sorted_successors(tid, curr, end);
    param.comm_times_of_sorted_successors(tid, ccurr, cend);
    for (; curr != end; ++curr, ++ccurr) {
      tmp = *ccurr + buffer.at(*curr);
      if (tmp > smax) {
        smax = tmp;
      }
    }
    smax += avg_weights[tid];
    buffer.at(tid) = smax;
  }
  //   std::cout<<"b_rank_t:"<<std::endl;
  //   print(*ptr_vec);
  set_ascending_order(false);
}
/*drank es t_rank_t*/
/*urank es b_rank_t*/

//--------------------------------------------------------------------------------------------

void a_rank_t::generate(const hms_param_t& param, std::vector<float>& buffer) {
  time_unit_t min_ft, tmp, cp_along_value;
  std::vector<task_t>::const_iterator curr, end;
  std::vector<time_unit_t>::const_iterator ccurr, cend;
  
  std::list<task_t> topolist = param.to_sorted_topo_list();
  std::vector<time_unit_t> avg_weights = param.to_avg_weights();
  crit_path_out_arg_t arg;
  param.to_hetero_critical_path(arg);
  cp_along_value = arg.priorities[*topolist.begin()];
  for (std::list<task_t>::const_reverse_iterator it = topolist.rbegin();
       it != topolist.rend(); ++it) {
    min_ft = cp_along_value;
    param.sorted_successors(*it, curr, end);
    param.comm_times_of_sorted_successors(*it, ccurr, cend);
    for (; curr != end; ++curr, ++ccurr) {
      tmp = buffer.at(*curr) - *ccurr;
      if (tmp < min_ft) {
        min_ft = tmp;
      }
    }
    buffer.at(*it) = min_ft - avg_weights[*it];
  }
  //   std::cout<<"b_rank_t:"<<std::endl;
  //   print(*ptr_vec);
  set_ascending_order(true);
}

void cldd_rank_t::generate(const hms_param_t& param, std::vector<float>& buffer) {
  time_unit_t max_comm;
  std::vector<time_unit_t>::const_iterator ccurr, cend;

  std::list<task_t> topolist = param.to_sorted_topo_list();
  std::vector<time_unit_t> min_weights = param.get_min_weights();
  std::vector<time_unit_t> avg_comm = param.get_avg_communication();
  for (std::list<task_t>::const_iterator it = topolist.begin();
       it != topolist.end(); ++it) {
    task_t tid = *it;
    param.comm_times_of_sorted_predecessors(tid, ccurr, cend);
    max_comm = *(std::max_element(ccurr, cend));
    buffer.at(tid) = min_weights[tid] + avg_comm[tid] + max_comm;
  }
  //   std::cout<<"b_rank_t:"<<std::endl;
  //   print(*ptr_vec);
  set_ascending_order(true);
}

// 54.pdf:p.3
void sl_rank_t::generate(const hms_param_t& param, std::vector<float>& buffer) {
  time_unit_t avg_weight, max_sw;
  std::vector<task_t>::const_iterator curr, end;
  std::vector<time_unit_t>::const_iterator ccurr, cend;

  std::list<task_t> topolist = param.to_sorted_topo_list();
  for (std::list<task_t>::const_reverse_iterator it = topolist.rbegin();
       it != topolist.rend(); ++it) {
    avg_weight = max_sw = 0.0;
    param.weights_of(*it, ccurr, cend);
    avg_weight =
        (std::accumulate(ccurr, cend, avg_weight)) / std::distance(ccurr, cend);
    param.sorted_successors(*it, curr, end);
    for (; curr != end; ++curr) {
      if (buffer.at(*curr) > max_sw) max_sw = buffer.at(*curr);
    }
    buffer.at(*it) = avg_weight + max_sw;
  }
  //   std::cout<<"b_rank_t:"<<std::endl;
  //   print(*ptr_vec);
  set_ascending_order(true);
}

void mixed_rank_t::generate(const hms_param_t& param, std::vector<float>& buffer) {

  std::list<task_t> topolist = param.to_sorted_topo_list();
  int imax;
  std::vector<task_t>::const_iterator curr, end;
  std::vector<float> t_rank_ts(buffer.size());
  b_rank_t op;
  op.generate(param, buffer);
  t_rank_t oop;
  oop.generate(param, t_rank_ts);
  for (std::list<task_t>::const_iterator it = topolist.begin();
       it != topolist.end(); ++it) {
    imax = -1;
    param.sorted_predecessors(*it, curr, end);

    for (; curr != end; ++curr) {
      if (buffer.at(*curr) > imax) {
        imax = buffer.at(*curr);
      }
    }
    ++imax;
    buffer.at(*it) = imax + t_rank_ts[*it];
  }
  // std::cout<<"l_rank_t:"<<std::endl;
  // print(*ptr_vec);
  set_ascending_order(true);
}
