/*
 * File:   HCROAlgorithm.h
 * Author: Alexander
 *
 * Created on 31 de mayo de 2015, 08:40 AM
 */

#ifndef HCROALGORITHM_H
#define HCROALGORITHM_H

#include "Utils.h"

#include <set>
#include <vector>

#include <algorithm>
#include <cmath>
#include <cstring>
#include <functional>
#include <iostream>
#include <limits>
#include <map>
#include <numeric>
#include <sstream>
#include <string>
#include <iomanip>

//#define CPU_MULTITHREADED 1

#define EXISTENT true
#define NON_EXISTENT false

#ifdef CPU_MULTITHREADED
#include "ThreadPool.h"
#define _thread_info()                       \
  pool(std::thread::hardware_concurrency()), \
      num_threads(std::thread::hardware_concurrency())
#else
#define _thread_info() num_threads(1)
#endif

//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&66

class hcro_molecule_t;
class hcro_algorithm_t;

struct storing_cmp_t {
  const hcro_molecule_t* ptr_main_mol;
  const hcro_algorithm_t* ptr_algorithm;
  bool asc_ord;
  std::vector<int>* buff_distances;
  storing_cmp_t();
  bool operator()(size_t ifirst, size_t isecond);
};

class hcro_molecule_t {
 public:
  //-----------------------------------------------
  typedef unsigned short iprint_flags_t;
  enum print_flags_t{ USE_HEADINGS = 1, SCALAR_FIELDS = 2, INNER_MOLEC = 4, ML_SCALAR_FIELDS = 8, ML_INNER_MOLEC = 16 };

  //-----------------------------------------------
  hcro_molecule_t();
  hcro_molecule_t(const hcro_molecule_t& other);
  void init(bool inner_min_init = true, bool set_molec_values = true);
  hcro_molecule_t& operator=(const hcro_molecule_t& other);
  void generate_owi_candidate(hcro_molecule_t& first) const;
  void generate_decomp_candidates(hcro_molecule_t& first, hcro_molecule_t& second) const;
  void generate_imi_candidates(const hcro_molecule_t& omolecule, hcro_molecule_t& first,
                             hcro_molecule_t& second) const;
  bool generate_synth_candidate(const hcro_molecule_t& other,
                             hcro_molecule_t& candidate) const; /*returns if generated */
  bool xor_range_bounds(const hcro_molecule_t& other_molecule, int& begin,
                      int& end) const;  // returns identical?
  int distance_to(const hcro_molecule_t& other_molecule) const;
  bool operator==(const hcro_molecule_t& other_molecule) const;
  bool operator!=(const hcro_molecule_t& other_molecule) const;
  bool inner_molecule_equals(const hcro_molecule_t& other_molecule, bool ml_eval = false) const;
  void set_inner_min_default();
  void init_inner_sequence(const hcro_molecule_t& other_molecule, int ifirst, int num_rotations = -1);
  time_unit_t get_pe() const;
  time_unit_t get_ml_pe() const;
  void set_pe(time_unit_t pe_value);
  void set_ml_pe(time_unit_t pe_value);
  float get_ke() const;
  void set_ke(float ke_value);
  void set_num_hit(short value);
  void set_ml_num_hit(short value);
  void set_print_flags(iprint_flags_t flags);
  iprint_flags_t get_print_flags() const;
  short get_num_hit() const;
  short get_ml_num_hit() const;
  time_unit_t calculate_pe(schedule_t* sched = 0, bool set_property = true);
  std::string to_string() const;
  bool inner_molecule_is_sorted() const;
  bool is_valid() const;
  bool is_empty() const;
  bool has_min_local() const;
  void assign_non_ml_fields(const hcro_molecule_t& other);
  const std::vector<task_t>& get_inner_molecule() const;
  std::vector<task_t>& get_inner_molecule();
  const std::vector<task_t>& get_ml_inner_molecule() const;
  const hcro_molecule_t& get_min_local() const;
  hcro_molecule_t& get_min_local();
  std::vector<task_t>& get_ml_inner_molecule();
  ~hcro_molecule_t();
  static void set_curr_algorithm(hcro_algorithm_t* ptr);
  static hcro_algorithm_t* get_curr_algorithm();
  static std::vector<hcro_molecule_t> generate_model_molecules(std::vector<priority_cmp_t<float> >& ordering_criteria);
  static hcro_molecule_t random_generate(
    short nh_lb,
    short nh_ub,
    time_unit_t pe_lb,
    time_unit_t pe_ub,
    float ke_lb,
    float ke_ub,
    bool ml_init = false,
    bool use_priorities = true);

  static hcro_molecule_t from_string(
    std::string str,
    short nh_lb,
    short nh_ub,
    time_unit_t pe_lb,
    time_unit_t pe_ub,
    bool ml_init = false,
    bool calc_pe = false);

  void left_rotate_by_one(int incl_begin, int incl_end);
  void right_rotate_by_one(int incl_begin, int incl_end);
  hcro_molecule_t& select_candidate(bool asc_order, int molecule_index, int& selected_index, size_t thread_idx);
  void exchange_segments(const hcro_molecule_t& other_molecule, int cut_index,
                        hcro_molecule_t& first, hcro_molecule_t& second) const;

  int get_inner_molec_digest() const; //it is not known if collision-free but helpful here
 private:
  iprint_flags_t p_flags;
  short num_hit;
  time_unit_t pe;
  float ke;
  std::vector<task_t> inner_molecule; //inner_molecule is not strictly ordered
  static hcro_algorithm_t* ptr_algorithm;
  hcro_molecule_t* min_local;
  friend class hcro_algorithm_t;
};

std::ostream& operator<<(std::ostream& os, const hcro_molecule_t& m);

//-------------------------------------------------------------------------------

class hcro_algorithm_t : public scheduling_algorithm_t {
 public:
  hcro_algorithm_t(
    size_t iterations,
    size_t inum_groups,
    size_t max_n_groups,
    size_t sect_amount, 
    float pic,
    size_t num_candidates,
    std::vector<priority_cmp_t<float> >& p_ordering,
    float buffer_value,
    float mu,
    float sigma,
    float cl_rate,
    short param_alpha,
    float param_beta,
    float ke_init,
    float ke_lrate);

  hcro_algorithm_t();
  hcro_algorithm_t(const hcro_algorithm_t& other);
  hcro_algorithm_t& operator=(const hcro_algorithm_t& other);
  schedule_t run();
  virtual ~hcro_algorithm_t();

  size_t get_num_iterations() const;
  size_t get_section_amount() const;
  size_t get_max_num_groups() const;
  size_t get_num_groups() const;
  size_t get_num_threads() const;

  float get_mu() const;
  float get_sigma() const;
  float get_collision_rate() const;
  float get_buffer_value() const;
  float get_ke_loss_rate() const;
  float get_ke_init_value() const;
  float get_beta() const;
  float get_percentage_init_created() const;

  short get_alpha() const;
 
  void set_buffer_value(float value);

  reaction_min_t do_reactions();
  void do_single_reaction(reaction_min_t& selected_ms, size_t thread_idx);
  void set_init_population();
  inline void enlarge_population();
  void do_decomposition(hcro_molecule_t& molecule, int index,
                       reaction_min_t& result, size_t thread_idx);
  void do_on_wall_ineffect_collision(hcro_molecule_t& molecule, int index,
                                 reaction_min_t& result,
                                 size_t thread_idx);
  void do_inter_molec_ineffect_collision(hcro_molecule_t& molecule, int index,
                                     reaction_min_t& result,
                                     size_t thread_idx);
  bool do_synthesis(
      hcro_molecule_t& molecule, int m_index, reaction_min_t& result,
      size_t thread_idx); /*returns done if proper candidates selected*/
  float gen_new_random_x();
  hcro_molecule_t& select_individual(int& individual_index, bool existent, size_t thread_idx, ordered_excluded_t* ptr = 0);
  hcro_molecule_t& create_individual(int& individual_index, size_t thread_idx);
  void remove_individual(int individual_index, size_t thread_idx);
  
  private:
    
  std::vector<hcro_molecule_t> population;
  size_t candidates_sz, num_iterations, section_amount, max_num_groups,
      num_groups, num_threads;
  float nd_mu, nd_sigma, collision_rate, buffer, ke_loss_rate, ke_init_value,
      beta, percentage_initially_created;
  short alpha;
  std::vector<priority_cmp_t<float> > ordering_criteria;
  std::vector<bool> indexes;
  std::vector<size_t> num_available;
  friend class hcro_molecule_t;
  friend class storing_cmp_t;

#ifdef CPU_MULTITHREADED
  ThreadPool pool;
  std::mutex mp_mutex, buffer_mutex;

#endif
};

#endif /* HCROALGORITHM_H */
