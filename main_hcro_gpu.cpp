#define M 30  //tasks
#define P 5  //processors
#define W 5  //num workers
#define POP_SZ  100 //population size

#include <iostream>
#include <algorithm>
#include <iterator>
#ifdef __APPLE__
    #include <OpenCL/cl.hpp>
#else
    #include <CL/cl.hpp>
#endif


#include <cstdlib>
#include <fstream>
#include <iostream>
#include <list>
#include <sstream>

#include "Utils.h"
#include "file_persist.h"
#include "HCROAlgorithm.h"
#include "Priorities.h"


using namespace std;
using namespace cl;


typedef cl_float custom_time_unit_t;
typedef cl_short custom_proc_t;
typedef cl_short custom_task_t;

typedef cl_int custom_int;
typedef bool custom_bool;
typedef cl_float custom_float;
typedef cl_short custom_short;
typedef cl_ulong custom_ul;

typedef struct my_task_time_pair{
    custom_time_unit_t ttime;
    custom_task_t tid;
} custom_task_time_pair_t;

typedef struct my_worker_output{
    custom_int molecule_index;
    custom_time_unit_t makespan;
    custom_bool ml_min;
    custom_float coeff, added;
} custom_worker_output_t;

typedef struct my_max_entry{
  custom_time_unit_t first_max, second_max;
  custom_proc_t fm_pid, sm_pid;

} custom_max_entry_t;


typedef struct my_hcro_base_params{
    custom_float collision_rate, ke_loss_rate, ke_init; // [0.0, 1.0 >  TODO: explicar
    custom_float beta; // TODO: explicar
    custom_float nd_sigma, nd_mu; // TODO: explicar
    custom_float buffer;
    custom_short alpha; // TODO: explicar
    custom_bool init_pop;
    custom_ul random_seed;
} custom_hcro_base_params_t;

typedef struct half_molecule{
    custom_time_unit_t pe, ml_pe;
    custom_float ke;
    custom_int num_hit, ml_num_hit;
} custom_half_molecule_t;

struct custom_traits_t{
  typedef custom_float cont_float;
  typedef custom_short cont_short;
  typedef custom_bool cont_bool;
  typedef custom_ul cont_unsigned_long;
  typedef custom_int cont_int;
  typedef custom_task_t cont_task_t;
  typedef custom_time_unit_t cont_time_unit_t;  
};

Platform get_platform() {
    /* Returns the first platform found. */
    std::vector<Platform> all_platforms;
    Platform::get(&all_platforms);

    if (all_platforms.size() == 0) {
        std::cout << "No platforms found. Check OpenCL installation!\n";
        exit(1);
    }
    return all_platforms[0];
}


Device get_device(Platform platform, int i, bool display = false) {
    /* Returns the deviced specified by the index i on platform.
     * If display is true, then all of the platforms are listed.
     */
    std::vector<Device> all_devices;
    platform.getDevices(CL_DEVICE_TYPE_ALL, &all_devices);
    if(all_devices.size() == 0){
        cout << "No devices found. Check OpenCL installation!\n";
        exit(1);
    }

    if(display){
        for(int j = 0; j < all_devices.size(); j++)
            printf("Device %d: %s\n", j, all_devices[j].getInfo<CL_DEVICE_NAME>().c_str());
    }
    return all_devices[i];
}

//also defines should be overriden
std::string get_final_src_code(const std::string& filepath){

}

custom_worker_output_t* allocate_workers_output(std::size_t workers_output_byte_sz){
    return (custom_worker_output_t*)malloc(workers_output_byte_sz);
}

custom_task_t* init_model_molecs(std::size_t num_criteria, const std::vector<hcro_molecule_t>& model_molecs){
    assert(model_molecs.size() == num_criteria);
    custom_task_t* ptr = (custom_task_t*)malloc(num_criteria * M * sizeof(custom_task_t));
    for(const hcro_molecule_t& m : model_molecs){

        for(task_t tid : m.get_inner_molecule()){
            *ptr = tid;
            ++ptr;
        }
    }
}


void* init_graph_info(std::size_t cgi_byte_sz, std::size_t num_edges, const unified_params_t<custom_traits_t>& params){
    void* original_ptr;
    void* ptr;
    original_ptr = malloc(cgi_byte_sz);
    ptr = original_ptr;
    custom_hcro_base_params_t* param_ptr = (custom_hcro_base_params_t*)ptr;
    param_ptr->collision_rate = params.collision_rate;
    param_ptr->ke_loss_rate = params.ke_loss_rate;
    param_ptr->ke_init = params.ke_init;
    param_ptr->beta = params.beta;
    param_ptr->nd_sigma = params.sigma;
    param_ptr->nd_mu = params.mu;
    param_ptr->buffer = params.buffer_value;
    param_ptr->alpha = params.alpha;
    param_ptr->init_pop = params.init_pop;
    param_ptr->random_seed = params.seed;

    ptr = ptr + sizeof(custom_hcro_base_params_t);

    custom_task_time_pair_t* pair_ptr = (custom_task_time_pair_t*)ptr;

    for(const std::pair<custom_task_t, custom_time_unit_t>& p : params.comm_successors){
        pair_ptr->tid = p.first;
        pair_ptr->ttime = p.second;
        ++pair_ptr;
    }

    ptr = ptr + num_edges * sizeof(custom_task_time_pair_t);

    pair_ptr = (custom_task_time_pair_t*)ptr;

    for(const std::pair<custom_task_t, custom_time_unit_t>& p : params.comm_predecessors){
        pair_ptr->tid = p.first;
        pair_ptr->ttime = p.second;
        ++pair_ptr;
    }

    ptr = ptr + num_edges * sizeof(custom_task_time_pair_t);
    custom_int* limit_ptr = (custom_int*)ptr;

    for(const std::pair<custom_int, custom_int>& p : params.limit_offsets){
        *limit_ptr = p.second;
        *(limit_ptr + 1) = p.first;
        limit_ptr += 2;
    }

    ptr = ptr + M * 2 * sizeof(custom_int);
    custom_time_unit_t* weight_ptr = (custom_time_unit_t*)ptr;

    for(custom_time_unit_t elem : params.weights_matrix){
        *weight_ptr = elem;
        ++weight_ptr;
    }

    return original_ptr;
}

custom_task_t* allocate_pop_sec_half(){
    return (custom_task_t*)malloc(POP_SZ * 2 * M * sizeof(custom_task_t));
}

void release(void* ptr){
    free(ptr);
}

//find_min makespan & generate schedule using conventional methods
void process_workers_output(custom_worker_output_t* w_ptr, custom_task_t* pop_sec_half){
    custom_time_unit_t min_makespan = std::numeric_limits<custom_float>::infinity();
    custom_int min_index = -1;
    custom_worker_output_t* curr = w_ptr;
    hcro_molecule_t m;
    schedule_t sched;
    for(custom_int i = 0; i < W; ++i, ++curr){
        if(curr->makespan < min_makespan){
            min_makespan = curr->makespan;
            min_index = i;
        }
    }

    if(min_index >= 0){
        custom_task_t* min_molec = pop_sec_half + min_index * 2 * M;
        curr = w_ptr + min_index;
        if(curr->ml_min){
            min_molec += M;
        }
        m.init(false, false);
        custom_task_t* ocurr = min_molec;
        for(task_t& ref : m.get_inner_molecule()){
            ref = *ocurr;
            ++ocurr;
        }
        time_unit_t pe = m.calculate_pe(&sched, false);
        std::cout << "min_schedule:" << std::endl;
        std::cout << sched << std::endl;
    }

}

int main() {

    std::size_t num_edges, num_criteria;
    void* graph_info;
    custom_task_t* model_molecs;
    custom_task_t* pop_sec_half;
    custom_worker_output_t* w_output;
    std::string src_code_fp = "opencl/hcro_algorithm.cl";

    std::size_t wm_entry_byte_sz  = M * sizeof(custom_task_t) + M * sizeof(custom_task_t);
    wm_entry_byte_sz += M * sizeof(custom_max_entry_t);
    wm_entry_byte_sz += M * sizeof(custom_task_time_pair_t) + P * sizeof(custom_task_t);

    std::size_t cgi_byte_sz = sizeof(custom_hcro_base_params_t) + 2 * num_edges * sizeof(custom_task_time_pair_t);
    cgi_byte_sz += (M * 2 * sizeof(custom_int) + P * M * sizeof(custom_time_unit_t));


    std::size_t m_molecs_byte_sz = num_criteria * M * sizeof(custom_task_t);
    std::size_t workers_output_byte_sz = W * sizeof(custom_worker_output_t);

    //------------------------------------------------------------------------------------------

    {

        b_rank_t bp;
        t_rank_t tp;  // a,t_rank_t tp;
        l_rank_t lp;
        std::vector<priority_cmp_t<float> > priorities(3);
        priorities[0].set_criterion(&bp);
        priorities[1].set_criterion(&tp);
        priorities[2].set_criterion(&lp);

        std::string uid = "uid_final_123456";
        std::vector<hcro_molecule_t> model_molecules;
        hms_param_t custom_param;
        custom_param = hms_params_from_json_file("data/final_params.json", uid);

        if(custom_param.is_empty()){
            custom_param = hms_param_t::random_generate(P, M, 0.4f, 1.5f, 9.5f, 1.0f, 10.0f);
            auto res = persist_hms_params_to_json_file("data/nu_params.json", custom_param, uid);
        }

        num_edges = custom_param.get_num_edges();
        num_criteria = priorities.size();

        hcro_algorithm_t alg1(200,   // num_iterations
                           3,     // initial_num_groups : aka groups of sections
                           8,     // max_num_groups
                           30,    // section_amount aka elements per section
                           0.5f,  // percent elements per section initially created
                           7,     // num_candidates
                           priorities,  // priorities
                           2000.0f,     // buffer_init_val
                           0.0f,        // mu
                           1.0f,        // sigma
                           0.5f,        // collision_rate
                           200,         // alfa:char
                           100.0f,      // beta
                           10000.0f,    // ke_init
                           0.5f         // ke_loss_rate
        );

        alg1.set_param(&custom_param);
        model_molecules = hcro_molecule_t::generate_model_molecules(priorities);

        unified_params_t<custom_traits_t> u_params = custom_param.to_unified_params_t<custom_traits_t>(
          alg1.get_buffer_value(),
          alg1.get_mu(),
          alg1.get_sigma(),
          alg1.get_collision_rate(),
          alg1.get_alpha(),
          alg1.get_beta(),
          alg1.get_ke_init_value(),
          alg1.get_ke_loss_rate(),
          true,
          get_curr_epoch_ns());

        graph_info = init_graph_info(cgi_byte_sz, num_edges, u_params);
        model_molecs = init_model_molecs(num_criteria, model_molecules);
        w_output = allocate_workers_output(workers_output_byte_sz);
    }

    //------------------------------------------------------------------------------------------

    Platform default_platform = get_platform();
    Device default_device     = get_device(default_platform, 0);  //0 y no 1; porque habia ya probado con 1 antes
    Context context({default_device});
    Program::Sources sources;

    std::string kernel_name = "";
    std::string kernel_code = get_final_src_code(src_code_fp);
    sources.push_back({kernel_code.c_str(), kernel_code.length()});

    Program program(context, sources);
    if (program.build({default_device}) != CL_SUCCESS) {
        cout << "Error building: " << program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(default_device) << std::endl;
        exit(1);
    }

    Buffer fh_population(context, CL_MEM_READ_WRITE, POP_SZ * sizeof(custom_half_molecule_t));
    Buffer sh_population(context, CL_MEM_READ_WRITE, POP_SZ * M * 2 * sizeof(custom_task_t));
    Buffer workers_memory(context, CL_MEM_READ_WRITE, W * wm_entry_byte_sz);
    Buffer common_graph_info(context, CL_MEM_READ_ONLY, cgi_byte_sz);
    Buffer model_molecules(context, CL_MEM_READ_ONLY, m_molecs_byte_sz);
    Buffer workers_output(context, CL_MEM_WRITE_ONLY, workers_output_byte_sz);

    CommandQueue queue(context, default_device);
    queue.enqueueWriteBuffer(common_graph_info, CL_TRUE, 0, cgi_byte_sz, graph_info);
    queue.enqueueWriteBuffer(model_molecules, CL_TRUE, 0, m_molecs_byte_sz, model_molecs);

    Kernel kernel = Kernel(program, kernel_name.c_str());
    kernel.setArg(0, fh_population);
    kernel.setArg(1, sh_population);
    kernel.setArg(2, workers_output);
    kernel.setArg(3, common_graph_info);
    kernel.setArg(4, model_molecules);
    kernel.setArg(5, workers_output);

    queue.enqueueNDRangeKernel(kernel, NullRange, NDRange(W), NDRange(32)); //TODO: setter en numero de workers pensado

    queue.enqueueReadBuffer(workers_output, CL_TRUE, 0, workers_output_byte_sz, w_output);
    
    pop_sec_half = allocate_pop_sec_half();

    queue.enqueueReadBuffer(sh_population, CL_TRUE, 0, POP_SZ * M * 2 * sizeof(custom_task_t), pop_sec_half);

    process_workers_output(w_output, pop_sec_half);

    if(pop_sec_half){
        release(pop_sec_half);
    }
    
    if(graph_info){
        release(graph_info);
    }

    if(model_molecs){
        release(model_molecs);
    }

    if(w_output){
        release(w_output);
    }

    return 0;
}