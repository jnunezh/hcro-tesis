/*
 * File:   file_persist.h
 * Author: Alexander
 *
 * Created on 21 de mayo de 2015, 01:58 PM
 */

#ifndef FILE_PERSIST_H
#define FILE_PERSIST_H

#include <fstream>
#include "Utils.h"
#include "HCROAlgorithm.h"

schedule_t schedule_from_json_file(
    const std::string& filepath,
    const std::string& uid = std::string());

//(success, generated_uid if so)
std::pair<bool, std::string> persist_schedule_to_json_file(
    const std::string& filepath,
    const schedule_t& sc,
    const std::string& uid = std::string());

hms_param_t hms_params_from_json_file(
    const std::string& filepath,
    const std::string& uid = std::string());

//(success, generated_uid if so)
std::pair<bool, std::string> persist_hms_params_to_json_file(
    const std::string& filepath,
    const hms_param_t& params,
    const std::string& uid = std::string());

bool create_dag_gexf_file_from(
    const std::string& filepath,
    const hms_param_t& params);

bool truncate_plot_json_file(
    const std::string& filepath,
    schedule_t& bheft_sc,
    schedule_t& cpop_sc,
    schedule_t& hcro_sc,
    const hms_param_t& param);

hcro_molecule_t molecule_from_json_file(
    const std::string& filepath,
    const std::string& uid = std::string());

std::pair<bool, std::string> persist_molecule_to_json_file(
    const std::string& filepath,
    const hcro_molecule_t& molecule,
    const std::string& uid = std::string());

bool persist_params_to_c_readable_file(
    const std::string& filepath,
    const unified_params_t<default_traits_t>& params);

unified_params_t<default_traits_t> params_from_c_readable_file(
    const std::string& filepath);

void process_on_split_str(std::string s, const std::string& delimiter, std::function<void(const std::string&)> f);

bool persist_molecules_to_c_readable_file(
    const std::string& filepath,
    const std::vector<hcro_molecule_t>& molecules);

std::vector<hcro_molecule_t> molecules_from_c_readable_file(const std::string& filepath);

std::vector<std::pair<int, int> > indexes_from_file(const std::string& filepath);

std::vector<hcro_molecule_t> imolecules_from_file(const std::string& filepath);

void empty_file(const std::string& filepath);

bool append_ivalues_to_file(
    const std::string& filepath,
    int res,
    int pre_molec,
    int pre_ml,
    int pre_omolec,
    int pre_oml,
    int post_molec,
    int post_ml,
    int post_omolec,
    int post_oml);

template<typename T>
std::vector<T> split_values_from_file(const std::string& filepath){
   std::vector<T> res;
   try{
    std::ifstream myfile(filepath);
    std::string line, delimiter;
    std::ostringstream str;
    delimiter = ",";
    while(std::getline(myfile, line)){
      str << line;
    }
    res = split_str_into_vector<T>(str.str(), delimiter);
    myfile.close();
  }
  catch (const std::exception& e){
    std::cout << "exception@split_values_from_file:" << e.what() << std::endl;
    res.clear();
  }
  return res;
}

#endif /* FILE_PERSIST_H */