#include <iostream>
#include <fstream>
#include <cstring>
#include <functional>
#include <algorithm>
#include <iterator>
#include <iomanip>
#include <cmath>
#include <ctgmath>
#include <memory>
#include <cstdio>
#include "file_persist.h"
#include "jsoncpp/json/json.h"

unsigned get_num_digits(unsigned i){
    return i > 0 ? (int) log10 ((double) i) + 1 : 1;
}

std::string random_string(size_t length){
    auto randchar = []() -> char{
        const char charset[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";
        const size_t max_index = (sizeof(charset) - 1);
        return charset[ rand() % max_index ];
    };
    std::string str(length,0);
    std::generate_n( str.begin(), length, randchar );
    return str;
}

/*
{
  "entries":
  [
    {
      "uid":"testie",
      "num_procs": 2,
      "num_tasks": 5,
      "ordered_successors": [2, 5, 15, 3, 22],
      "ordered_predecessors": [2, 5, 15, 3, 22],
      "successors_limit_offsets": [0, 0, 0, 0, 0],
      "predecessors_limit_offsets": [0, 0, 0, 0, 0],
      "comm_times_successors": [2.1, 3.1, 3.4, 5.5],
      "comm_times_predecessors": [2.1, 3.1, 3.4, 5.5],
      "weights_matrix": [2.1, 3.1, 3.4]
    }
  ]
}
*/
hms_param_t hms_params_from_json_file(
  const std::string& filepath,
  const std::string& uid){
/*
size_t num_procs, num_tasks;
  std::vector<task_t> successors, predecessors; ---ordered
  std::vector<std::pair<size_t, size_t> >
      limit_offsets;  index =>(succesors_offset,predecessors_offset)
  std::vector<time_unit_t> comm_successors, comm_predecessors,
      weights_matrix;  // comm_successors: [tid => predecessors_times];
                       // weights_matrix: [tid => times_per_proc] access via
                       // formula: tid * num_procs
*/
  hms_param_t params;

  try {

    int chosen_index = -1;
    std::ifstream ifs(filepath);
    Json::CharReaderBuilder builder;
    Json::Value obj;
    std::string errs;
    bool ok = Json::parseFromStream(builder, ifs, &obj, &errs);
    
    if(!ok){
      throw std::exception();
    }
    
    if(!uid.empty()){
      for(int i = 0; i < obj["entries"].size(); ++i){
        if(std::strcmp(obj["entries"][i]["uid"].asCString(), uid.c_str()) == 0){
          chosen_index = i;
          break;
        }
      }
      if(chosen_index == -1){
        throw std::exception(); //"uid not found!!!";
      }
    }
    else{
      chosen_index = 0;
    }

    const Json::Value& chosen_entry = obj["entries"][chosen_index];

    params.set_num_procs(chosen_entry["num_procs"].asUInt());
    params.set_num_tasks(chosen_entry["num_tasks"].asUInt());

    std::vector<task_t>& ref_succ = params.get_successors();
    const Json::Value& ordered_successors = chosen_entry["ordered_successors"];
    for(int i = 0; i < ordered_successors.size(); ++i){
      ref_succ.push_back(static_cast<task_t>(ordered_successors[i].asUInt()));
    }

    std::vector<task_t>& ref_pred = params.get_predecessors();
    const Json::Value& ordered_predecessors = chosen_entry["ordered_predecessors"];
    for(int i = 0; i < ordered_predecessors.size(); ++i){
      ref_pred.push_back(static_cast<task_t>(ordered_predecessors[i].asUInt()));
    }

    const Json::Value& successors_limit_offsets = chosen_entry["successors_limit_offsets"];
    const Json::Value& predecessors_limit_offsets = chosen_entry["predecessors_limit_offsets"];
    std::vector<std::pair<size_t, size_t> >& ref_limit_offsets = params.get_limit_offsets();

    for(int i = 0; i < successors_limit_offsets.size(); ++i){
      ref_limit_offsets.push_back(std::pair<size_t, size_t>(successors_limit_offsets[i].asUInt(), predecessors_limit_offsets[i].asUInt()));
    }  

    std::vector<time_unit_t>& ref_comm_succs = params.get_comm_successors();
    const Json::Value& comm_times_successors = chosen_entry["comm_times_successors"];
    for(int i = 0; i < comm_times_successors.size(); ++i){
      ref_comm_succs.push_back(static_cast<time_unit_t>(comm_times_successors[i].asFloat()));
    }

    std::vector<time_unit_t>& ref_comm_preds = params.get_comm_predecessors();
    const Json::Value& comm_times_predecessors = chosen_entry["comm_times_predecessors"];
    for(int i = 0; i < comm_times_predecessors.size(); ++i){
      ref_comm_preds.push_back(static_cast<time_unit_t>(comm_times_predecessors[i].asFloat()));
    }

    std::vector<time_unit_t>& ref_weight_m = params.get_weights_matrix();
    const Json::Value& weights = chosen_entry["weights_matrix"];
    for(int i = 0; i < weights.size(); ++i){
      ref_weight_m.push_back(static_cast<time_unit_t>(weights[i].asFloat()));
    }
    params.set_num_edges(ref_comm_succs.size());
  } 
  catch (const std::exception& e){
    std::cout << "exception@hms_params_from_json_file:" << e.what() << std::endl;
    params = hms_param_t();
  }
  return params;
}

/*
{
  "entries":
  [
    {
      "uid":"testie",
      "num_procs": 2,
      "num_tasks": 5,
      "ordered_successors": [2, 5, 15, 3, 22],
      "ordered_predecessors": [2, 5, 15, 3, 22],
      "successors_limit_offsets": [0, 0, 0, 0, 0],
      "predecessors_limit_offsets": [0, 0, 0, 0, 0],
      "comm_times_successors": [2.1, 3.1, 3.4, 5.5],
      "comm_times_predecessors": [2.1, 3.1, 3.4, 5.5],
      "weights_matrix": [2.1, 3.1, 3.4]
    }
  ]
}
*/

//(success, generated_uid if so)
std::pair<bool, std::string> persist_hms_params_to_json_file(
    const std::string& filepath,
    const hms_param_t& params,
    const std::string& uid){

  bool file_exists = true;
  Json::Value obj;
  std::pair<bool, std::string> result = std::make_pair(false, uid);
    
  try{
    Json::CharReaderBuilder builder;
    std::ifstream ifs(filepath);
    std::string errs;
    bool ok = Json::parseFromStream(builder, ifs, &obj, &errs);
    if(!ok){
      throw std::exception();
    }
  }
  catch (...){
    file_exists = false;
    obj["entries"] = Json::Value(Json::arrayValue);
  }
  try {

    int chosen_index = -1;

    if(file_exists && !uid.empty()){
      for(int i = 0; i < obj["entries"].size(); ++i){
        if(std::strcmp(obj["entries"][i]["uid"].asCString(), uid.c_str()) == 0){
          chosen_index = i;
          break;
        }
      }
    }

    Json::Value nnode;

    std::reference_wrapper<Json::Value> node_ref = std::ref(nnode);

    if(chosen_index >= 0){
      node_ref = std::ref(obj["entries"][chosen_index]);
    }
    else{
      if(!uid.empty()){
        node_ref.get()["uid"] = uid;
      }
      else{
        result.second = random_string(15);
        node_ref.get()["uid"] = result.second; 
      }
    }
    
    Json::Value& ref = node_ref.get(); 

    ref["num_procs"] = params.get_num_procs();
    ref["num_tasks"] = params.get_num_tasks();
    ref["num_edges"] = params.get_successors().size();

    const std::vector<task_t>& ref_succ = params.get_successors();
    Json::Value& ordered_successors = ref["ordered_successors"] = Json::Value(Json::arrayValue);

    for(task_t tid : ref_succ){
      ordered_successors.append(tid);
    }

    const std::vector<task_t>& ref_pred = params.get_predecessors();
    Json::Value& ordered_predecessors = ref["ordered_predecessors"] = Json::Value(Json::arrayValue);
    
    for(task_t tid : ref_pred){
      ordered_predecessors.append(tid);
    }

    Json::Value& successors_limit_offsets = ref["successors_limit_offsets"] = Json::Value(Json::arrayValue);
    Json::Value& predecessors_limit_offsets = ref["predecessors_limit_offsets"] = Json::Value(Json::arrayValue);
    
    const std::vector<std::pair<size_t, size_t> >& ref_limit_offsets = params.get_limit_offsets();

    for(const std::pair<size_t, size_t>& curr_pair : ref_limit_offsets){
      successors_limit_offsets.append(curr_pair.first);
      predecessors_limit_offsets.append(curr_pair.second);
    }  

    const std::vector<time_unit_t>& ref_comm_succs = params.get_comm_successors();
    Json::Value& comm_times_successors = ref["comm_times_successors"] = Json::Value(Json::arrayValue);
    for(time_unit_t value : ref_comm_succs){
      comm_times_successors.append(value);
    }

    const std::vector<time_unit_t>& ref_comm_preds = params.get_comm_predecessors();
    Json::Value& comm_times_predecessors = ref["comm_times_predecessors"] = Json::Value(Json::arrayValue);
    for(time_unit_t value : ref_comm_preds){
      comm_times_predecessors.append(value);
    }

    const std::vector<time_unit_t>& ref_weight_m = params.get_weights_matrix();
    Json::Value& weights = ref["weights_matrix"] = Json::Value(Json::arrayValue);
    for(time_unit_t value : ref_weight_m){
      weights.append(value);
    }

    if(chosen_index < 0){
      obj["entries"].append(ref);
    }

    Json::StreamWriterBuilder builder;
    std::unique_ptr<Json::StreamWriter> writer(builder.newStreamWriter());
    std::ofstream out(filepath);
    writer->write(obj, &out);
    result.first = true;
  } 
  catch (const std::exception& e){
    std::cout << "exception@persist_hms_params_to_json_file:" << e.what() << std::endl;
  }
  return result;
 
}

/*
{
  "entries":
  [
    {
      "uid": "testiee",
      "makespan": 12.4,
      "schedule_entries":
      [
        {
          "processor_id": 0,
          "processor_schedule":
          [
            {
              "start_time": 2.13,
              "task_id": 4
            }
          ]
        }
      ]
    }
  ]
}
*/
schedule_t schedule_from_json_file(const std::string& filepath, const std::string& uid){
  schedule_t sc;
  try {
    int chosen_index = -1;
    std::ifstream ifs(filepath);
    Json::CharReaderBuilder builder;
    Json::Value obj;
    std::string errs;
    bool ok = Json::parseFromStream(builder, ifs, &obj, &errs);
    
    if(!ok){
      throw std::exception();
    }

    if(!uid.empty()){
      for(int i = 0; i < obj["entries"].size(); ++i){
        if(std::strcmp(obj["entries"][i]["uid"].asCString(), uid.c_str()) == 0){
          chosen_index = i;
          break;
        }
      }
      if(chosen_index == -1){
        throw std::exception(); //"uid not found!!!";
      }
    }
    else{
      chosen_index = 0;
    }

    task_t tid;
    time_unit_t ts;
    proc_t pid;
    const Json::Value& chosen_entry = obj["entries"][chosen_index];

    int num_procs = chosen_entry["schedule_entries"].size();
    sc.sched.resize(num_procs);

    for(int i = 0; i < num_procs; ++i){
        
        const Json::Value& schedule_entry = chosen_entry["schedule_entries"][i];
        pid = schedule_entry["processor_id"].asUInt();
        for(int j = 0; j < schedule_entry["processor_schedule"].size(); ++j){
          tid = static_cast<task_t>(schedule_entry["processor_schedule"][j]["task_id"].asUInt());
          ts = static_cast<time_unit_t>(schedule_entry["processor_schedule"][j]["start_time"].asFloat());
          sc.sched[pid].push_back(std::pair<task_t, time_unit_t>(tid, ts));
        }
    }
    sc.set_makespan(static_cast<time_unit_t>(chosen_entry["makespan"].asFloat()));
    
  }
  catch (const std::exception& e){
    std::cout << "exception@schedule_from_json_file:" << e.what() << std::endl;
    sc = schedule_t();
  }  
  return sc;
}

/*
{
  "entries":
  [
    {
      "uid": "testiee",
      "makespan": 12.4,
      "schedule_entries":
      [
        {
          "processor_id": 0,
          "processor_schedule":
          [
            {
              "start_time": 2.13,
              "task_id": 4
            }
          ]
        }
      ]
    }
  ]
}
*/

//(success, generated_uid if so)
std::pair<bool, std::string> persist_schedule_to_json_file(
    const std::string& filepath,
    const schedule_t& sc,
    const std::string& uid){

  bool file_exists = true;
  Json::Value obj;
  std::pair<bool, std::string> result = std::make_pair(false, uid);
    
  try{

    std::ifstream ifs(filepath);
    Json::CharReaderBuilder builder;
    std::string errs;
    bool ok = Json::parseFromStream(builder, ifs, &obj, &errs);
    
    if(!ok){
      throw std::exception();
    }  
  }
  catch (...){
    file_exists = false;
    obj["entries"] = Json::Value(Json::arrayValue);
  }
  try {

    int chosen_index = -1;

    if(file_exists && !uid.empty()){
      for(int i = 0; i < obj["entries"].size(); ++i){
        if(std::strcmp(obj["entries"][i]["uid"].asCString(), uid.c_str()) == 0){
          chosen_index = i;
          break;
        }
      }
    }

    Json::Value nnode;

    std::reference_wrapper<Json::Value> node_ref = std::ref(nnode);

    if(chosen_index >= 0){
      node_ref = std::ref(obj["entries"][chosen_index]);
    }
    else{
      if(!uid.empty()){
        node_ref.get()["uid"] = uid;
      }
      else{
        result.second = random_string(15);
        node_ref.get()["uid"] = result.second; 
      }
    }
    
    Json::Value& ref = node_ref.get();

    ref["makespan"] = sc.get_makespan();

    
    const std::vector<std::vector<std::pair<task_t, time_unit_t> > >& ref_inner_sched = sc.get_inner_schedule();
    Json::Value& schedule_entries = ref["schedule_entries"] = Json::Value(Json::arrayValue);
    proc_t pid = 0;
    for(auto processor_schedule : ref_inner_sched){
      Json::Value entry;
      entry["processor_id"] = pid++;
      Json::Value& proc_sched_entries = entry["processor_schedule"] = Json::Value(Json::arrayValue);
      for(auto schedule_entry : processor_schedule){
        Json::Value element;
        element["task_id"] = schedule_entry.first;
        element["start_time"] = schedule_entry.second;
        proc_sched_entries.append(element);
      }
      schedule_entries.append(entry);
    }


    if(chosen_index < 0){
      obj["entries"].append(ref);
    }

    Json::StreamWriterBuilder builder;
    std::unique_ptr<Json::StreamWriter> writer(builder.newStreamWriter());
    std::ofstream out(filepath);
    writer->write(obj, &out);

    result.first = true;

  } 
  catch (const std::exception& e){
    std::cout << "exception@persist_schedule_to_json_file:" << e.what() << std::endl;
  }
  return result;
}

/*
<?xml version="1.0" encoding="UTF-8"?>
<gexf xmlns:viz="http:///www.gexf.net/1.1draft/viz" version="1.1" xmlns="http://www.gexf.net/1.1draft">
<meta lastmodifieddate="2010-03-03+23:44">
<creator>Gephi 0.7</creator>
</meta>
<graph defaultedgetype="undirected" idtype="string" type="static">
<nodes count="77">
  <node id="0.0" label="Myriel"/>

  <node id="76.0" label="MmeHucheloup"/>
</nodes>
<edges count="254">
  <edge id="0" source="1.0" target="0.0"/>

  <edge id="253" source="76.0" target="58.0"/>
</edges>
</graph>
</gexf>
*/
bool create_dag_gexf_file_from(
    const std::string& filepath,
    const hms_param_t& params){
  size_t num_digits = get_num_digits(params.get_num_tasks());
  try{

    std::ofstream myfile;
    myfile.open(filepath);
    
    myfile << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" << std::endl;
    myfile << "<gexf xmlns:viz=\"http:///www.gexf.net/1.1draft/viz\" version=\"1.1\" xmlns=\"http://www.gexf.net/1.1draft\">" << std::endl;
    myfile << "\t<meta>" << std::endl;
    myfile << "\t\t<creator>hms tesis</creator>" << std::endl;
    myfile << "\t</meta>" << std::endl;
    myfile << "\t<graph defaultedgetype=\"directed\" idtype=\"string\" type=\"static\">" << std::endl;
    myfile << "\t\t<nodes count=\"" << params.get_num_tasks() << "\">" << std::endl;

    for(task_t tid = 0; tid < params.get_num_tasks(); ++tid){
      myfile << "\t\t\t<node id=\"" << std::fixed << std::setw(num_digits) << std::setprecision(1); 
      myfile << static_cast<float>(tid) << "\" label=\"" << tid << "\"/>" << std::endl;
    }
    myfile << "\t\t</nodes>" << std::endl;

    myfile << "\t\t<edges count=\"" << params.get_successors().size() << "\">" << std::endl;
    size_t id = 0;
    for(task_t tid = 0; tid < params.get_num_tasks(); ++tid){
      std::vector<task_t>::const_iterator curr, begin, end;
      params.sorted_successors(tid, begin, end);
      for(curr = begin; curr != end; ++curr){
        myfile << "\t\t\t<edge id=\"" << id++ << "\" source=\"" << std::fixed << std::setw(num_digits) << std::setprecision(1);
        myfile << static_cast<float>(tid) << "\" target=\"" << std::fixed << std::setw(num_digits) << std::setprecision(1);
        myfile << static_cast<float>(*curr) << "\"/>" << std::endl;
      }
    }
    myfile << "\t\t</edges>" << std::endl;
    myfile << "\t</graph>" << std::endl;
    myfile << "</gexf>" << std::endl;
    
    myfile.close();
    return true;
  }
  catch (const std::exception& e){
    std::cout << "exception@create_dag_gexf_file_from:" << e.what() << std::endl;
  }
  return false;  
}

bool truncate_plot_json_file(
    const std::string& filepath,
    schedule_t& bheft_sc,
    schedule_t& cpop_sc,
    schedule_t& hcro_sc,
    const hms_param_t& params){
  try{
    Json::Value obj;
    
    obj["num_procs"] = params.get_num_procs();
    obj["num_tasks"] = params.get_num_tasks();
    Json::Value& times_tasks = obj["times_tasks"] = Json::Value(Json::arrayValue);
    
    std::vector<time_unit_t>::const_iterator begin, end, curr;
    for (task_t tid = 0; tid < params.get_num_tasks(); ++tid) {
        Json::Value entry;
        entry["task_id"] = tid;
        params.weights_of(tid, begin, end);
        Json::Value& weights = entry["weights"] = Json::Value(Json::arrayValue);
        for (curr = begin; curr != end; ++curr){
          weights.append(*curr);
        }
        times_tasks.append(entry);
        
    }
    
    Json::Value& algo_results = obj["algo_results"] = Json::Value(Json::arrayValue);

    {
      Json::Value entry;

      entry["algorithm"] = "bheft";
      entry["makespan"] = bheft_sc.get_makespan();
      entry["speedup"] = bheft_sc.calculate_speedup(params);
      entry["slr"] = bheft_sc.calculate_slr(params);
      Json::Value& schedule = entry["schedule"] = Json::Value(Json::arrayValue);
      proc_t pid = 0;
      for (const std::vector<std::pair<task_t, time_unit_t> >& proc_schedule : bheft_sc.get_inner_schedule()){
        Json::Value proc_entry;
        proc_entry["processor_id"] = pid;
        Json::Value& proc_schedule_ref = proc_entry["processor_schedule"] = Json::Value(Json::arrayValue);
        for (const std::pair<task_t, time_unit_t>& schedule_entry : proc_schedule){
          Json::Value element;
          element["task_id"] = schedule_entry.first;
          element["start_time"] = schedule_entry.second;
          proc_schedule_ref.append(element);
        }
        schedule.append(proc_entry);
        ++pid;
      }
      algo_results.append(entry);
    }

    {
      Json::Value entry;

      entry["algorithm"] = "hcro";
      entry["makespan"] = hcro_sc.get_makespan();
      entry["speedup"] = hcro_sc.calculate_speedup(params);
      entry["slr"] = hcro_sc.calculate_slr(params);
      Json::Value& schedule = entry["schedule"] = Json::Value(Json::arrayValue);
      proc_t pid = 0;
      for (const std::vector<std::pair<task_t, time_unit_t> >& proc_schedule : hcro_sc.get_inner_schedule()){
        Json::Value proc_entry;
        proc_entry["processor_id"] = pid;
        Json::Value& proc_schedule_ref = proc_entry["processor_schedule"] = Json::Value(Json::arrayValue);
        for (const std::pair<task_t, time_unit_t>& schedule_entry : proc_schedule){
          Json::Value element;
          element["task_id"] = schedule_entry.first;
          element["start_time"] = schedule_entry.second;
          proc_schedule_ref.append(element);
        }
        schedule.append(proc_entry);
        ++pid;
      }
      algo_results.append(entry);
    }

    {
      Json::Value entry;

      entry["algorithm"] = "cpop";
      entry["makespan"] = cpop_sc.get_makespan();
      entry["speedup"] = cpop_sc.calculate_speedup(params);
      entry["slr"] = cpop_sc.calculate_slr(params);
      Json::Value& schedule = entry["schedule"] = Json::Value(Json::arrayValue);
      proc_t pid = 0;
      for (const std::vector<std::pair<task_t, time_unit_t> >& proc_schedule : cpop_sc.get_inner_schedule()){
        Json::Value proc_entry;
        proc_entry["processor_id"] = pid;
        Json::Value& proc_schedule_ref = proc_entry["processor_schedule"] = Json::Value(Json::arrayValue);
        for (const std::pair<task_t, time_unit_t>& schedule_entry : proc_schedule){
          Json::Value element;
          element["task_id"] = schedule_entry.first;
          element["start_time"] = schedule_entry.second;
          proc_schedule_ref.append(element);
        }
        schedule.append(proc_entry);
        ++pid;
      }
      algo_results.append(entry);
    }
    
    Json::StreamWriterBuilder builder;
    std::unique_ptr<Json::StreamWriter> writer(builder.newStreamWriter());
    std::ofstream out(filepath);
    writer->write(obj, &out);

    return true;
  }
  catch (const std::exception& e){
    std::cout << "exception@truncate_plot_json_file:" << e.what() << std::endl;
  }
  return false;
}

/*
{
  "entries":
  [
    {
      "uid": "testiee",
      "pe": 12.4,
      "num_hit" : 120,
      "ke": 230.5,
      "inner_molecule" : [2, 4, 6, 11, 24, 32],
      "ml_pe" : 44.99,
      "ml_num_hit" : 331,
      "ml_inner_molecule" : [2, 4, 6, 11, 24, 32]
    }
  ]
}
*/
// at least one instance of hcro_algorithm must exist, should be our instance
hcro_molecule_t molecule_from_json_file(
    const std::string& filepath,
    const std::string& uid){
  
  hcro_molecule_t m;
  m.init(true, false);

  try {
    int chosen_index = -1;
    std::ifstream ifs(filepath);
    Json::CharReaderBuilder builder;
    Json::Value obj;
    std::string errs;
    bool ok = Json::parseFromStream(builder, ifs, &obj, &errs);
    
    if(!ok){
      throw std::exception();
    }

    if(!uid.empty()){
      for(int i = 0; i < obj["entries"].size(); ++i){
        if(std::strcmp(obj["entries"][i]["uid"].asCString(), uid.c_str()) == 0){
          chosen_index = i;
          break;
        }
      }
      if(chosen_index == -1){
        throw std::exception(); //"uid not found!!!";
      }
    }
    else{
      chosen_index = 0;  
    }

    const Json::Value& chosen_entry = obj["entries"][chosen_index];

    m.set_pe(chosen_entry["pe"].asFloat());
    m.set_num_hit(chosen_entry["num_hit"].asInt());
    m.set_ke(chosen_entry["ke"].asFloat());
    m.set_ml_num_hit(chosen_entry["ml_num_hit"].asInt());
    m.set_ml_pe(chosen_entry["ml_pe"].asFloat());

    int i = 0;
    for(task_t& tid : m.get_inner_molecule()){
      tid = chosen_entry["inner_molecule"][i++].asInt();
    }

    i = 0;
    for(task_t& tid : m.get_ml_inner_molecule()){
      tid = chosen_entry["ml_inner_molecule"][i++].asInt();
    }    
  }
  catch (const std::exception& e){
    std::cout << "exception@molecule_from_json_file:" << e.what() << std::endl;
    m = hcro_molecule_t();
  }  
  return m;
}



/*
{
  "entries":
  [
    {
      "uid": "testiee",
      "pe": 12.4,
      "num_hit" : 120,
      "ke": 230.5,
      "inner_molecule" : [2, 4, 6, 11, 24, 32],
      "ml_pe" : 44.99,
      "ml_num_hit" : 331,
      "ml_inner_molecule" : [2, 4, 6, 11, 24, 32]
    }
  ]
}
*/
//(success, generated_uid if so)
std::pair<bool, std::string> persist_molecule_to_json_file(
    const std::string& filepath,
    const hcro_molecule_t& molecule,
    const std::string& uid){
  
  bool file_exists = true;
  Json::Value obj;
  std::pair<bool, std::string> result = std::make_pair(false, uid);
    
  try{

    std::ifstream ifs(filepath);
    Json::CharReaderBuilder builder;
    std::string errs;
    bool ok = Json::parseFromStream(builder, ifs, &obj, &errs);
    
    if(!ok){
      throw std::exception();
    }  
  }
  catch (...){
    file_exists = false;
    obj["entries"] = Json::Value(Json::arrayValue);
  }
  try {

    int chosen_index = -1;

    if(file_exists && !uid.empty()){
      for(int i = 0; i < obj["entries"].size(); ++i){
        if(std::strcmp(obj["entries"][i]["uid"].asCString(), uid.c_str()) == 0){
          chosen_index = i;
          break;
        }
      }
    }

    Json::Value nnode;

    std::reference_wrapper<Json::Value> node_ref = std::ref(nnode);

    if(chosen_index >= 0){
      node_ref = std::ref(obj["entries"][chosen_index]);
    }
    else{
      if(!uid.empty()){
        node_ref.get()["uid"] = uid;
      }
      else{
        result.second = random_string(15);
        node_ref.get()["uid"] = result.second; 
      }
    }
    
    Json::Value& ref = node_ref.get();

    ref["pe"] = molecule.get_pe();
    ref["num_hit"] = molecule.get_num_hit();
    ref["ke"] = molecule.get_ke();

    int i = 0;
    ref["inner_molecule"] = Json::Value(Json::arrayValue);
    for(const task_t& tid : molecule.get_inner_molecule()){
      ref["inner_molecule"][i++] = tid;
    }

    ref["ml_pe"] = molecule.get_ml_pe();
    ref["ml_num_hit"] = molecule.get_ml_num_hit();

    i = 0;
    ref["ml_inner_molecule"] = Json::Value(Json::arrayValue);
    for(const task_t& tid : molecule.get_ml_inner_molecule()){
      ref["ml_inner_molecule"][i++] = tid;
    }

    if(chosen_index < 0){
      obj["entries"].append(ref);
    }

    Json::StreamWriterBuilder builder;
    std::unique_ptr<Json::StreamWriter> writer(builder.newStreamWriter());
    std::ofstream out(filepath);
    writer->write(obj, &out);

    result.first = true;

  } 
  catch (const std::exception& e){
    std::cout << "exception@persist_molecule_to_json_file:" << e.what() << std::endl;
  }
  return result;
}

/*
<collision_rate:float>
##########
<ke_loss_rate:float>
##########
<ke_init:float>
##########
<beta:float>
##########
<buffer_value:float>
##########
<mu:float>
##########
<sigma:float>
##########
<alpha:short>
##########
<init_pop:int>
##########
<seed:unsigned long>
##########
<limit_offsets as <succesor_offset, pred_offsets>> ==> 2,3;4,5;8,8;
##########
<comm_successors> ==> 2, 3.11;5, 9.455;7, 11.77;10, 9.781;
##########
<comm_predecessors> ==> 2, 3.11;5, 9.455;7, 11.77;10, 9.781;
##########
<weights_matrix> ==> 2.5; 1.2; 3.1; 4.8; 1.66; 9.99; 14.66; 7.1; 1.66; 9.99; 14.66; 7.1; 
*/
bool persist_params_to_c_readable_file(
    const std::string& filepath,
    const unified_params_t<default_traits_t>& params){
  
  try{

    std::ofstream myfile;
    myfile.open(filepath);
    
    myfile << std::fixed;
    myfile << std::setprecision(5) << params.collision_rate << std::endl;
    myfile << "##########" << std::endl;
    myfile << std::setprecision(5) << params.ke_loss_rate << std::endl;
    myfile << "##########" << std::endl;
    myfile << std::setprecision(5) << params.ke_init << std::endl;
    myfile << "##########" << std::endl;
    myfile << std::setprecision(5) << params.beta << std::endl;
    myfile << "##########" << std::endl;
    myfile << std::setprecision(5) << params.buffer_value << std::endl;
    myfile << "##########" << std::endl;
    myfile << std::setprecision(5) << params.mu << std::endl;
    myfile << "##########" << std::endl;
    myfile << std::setprecision(5) << params.sigma << std::endl;
    myfile << "##########" << std::endl;
    myfile << params.alpha << std::endl;
    myfile << "##########" << std::endl;
    myfile << static_cast<int>(params.init_pop) << std::endl;
    myfile << "##########" << std::endl;  
    myfile << params.seed << std::endl;
    myfile << "##########" << std::endl;

    for(const std::pair<size_t, size_t>& p : params.limit_offsets){
      myfile << p.first << "," << p.second << ";";
    }
    myfile << std::endl;
    myfile << "##########" << std::endl;

    for(const std::pair<task_t, time_unit_t>& p : params.comm_successors){
      myfile << p.first << "," << p.second << ";";
    }
    myfile << std::endl;
    myfile << "##########" << std::endl;

    for(const std::pair<task_t, time_unit_t>& p : params.comm_predecessors){
      myfile << p.first << "," << p.second << ";";
    }
    myfile << std::endl;
    myfile << "##########" << std::endl;

    for(time_unit_t e : params.weights_matrix){
      myfile << e << ";";
    }
    myfile << std::endl;
    
    myfile.close();
    return true;
  }
  catch (const std::exception& e){
    std::cout << "exception@persist_params_to_c_readable_file:" << e.what() << std::endl;
  }
  return false;
}

void process_on_split_str(std::string s, const std::string& delimiter, std::function<void(const std::string&)> f){
  size_t pos;
  std::string token;
  while ((pos = s.find(delimiter)) != std::string::npos){
      token = s.substr(0, pos);
      f(token);
      s.erase(0, pos + delimiter.length());
  }
}

/*
<collision_rate:float>
##########
<ke_loss_rate:float>
##########
<ke_init:float>
##########
<beta:float>
##########
<buffer_value:float>
##########
<mu:float>
##########
<sigma:float>
##########
<alpha:short>
##########
<init_pop:int>
##########
<seed:unsigned long>
##########
<limit_offsets as <succesor_offset, pred_offsets>> ==> 2,3;4,5;8,8;
##########
<comm_successors> ==> 2, 3.11;5, 9.455;7, 11.77;10, 9.781;
##########
<comm_predecessors> ==> 2, 3.11;5, 9.455;7, 11.77;10, 9.781;
##########
<weights_matrix> ==> 2.5; 1.2; 3.1; 4.8; 1.66; 9.99; 14.66; 7.1; 1.66; 9.99; 14.66; 7.1; 
*/
unified_params_t<default_traits_t> params_from_c_readable_file(
    const std::string& filepath){

  unified_params_t<default_traits_t> params;
  try{

    std::ifstream myfile(filepath);

    int i = 0;
    std::string line, delimiter;
    delimiter = ";";
    
    while(std::getline(myfile, line)){
        if(line.rfind("###", 0) == 0){ //starts with
          continue;
        }
        else{
          switch(i++){
            case 0:
              params.collision_rate = std::stof(line);
              break;
            case 1:
              params.ke_loss_rate = std::stof(line);
              break;
            case 2:
              params.ke_init = std::stof(line);
              break;
            case 3:
              params.beta = std::stof(line);
              break;
            case 4:
              params.buffer_value = std::stof(line);
              break;
            case 5:
              params.mu = std::stof(line);
              break;
            case 6:
              params.sigma = std::stof(line);
              break;
            case 7:
              params.alpha = std::stoi(line);
              break;
            case 8:
              params.init_pop = static_cast<bool>(std::stoi(line));
              break;
            case 9:
              params.seed = std::stoul(line);
              break;
            case 10:
              {
                std::vector<std::pair<size_t, size_t> >& ref = params.limit_offsets;
                ref.clear();
                process_on_split_str(line, delimiter, [&ref](const std::string& token) {
                  std::pair<size_t, size_t> p;
                  sscanf(token.c_str(), "%lu,%lu", &(p.first), &(p.second));
                  ref.push_back(p);
                });
              }
              break;
            case 11:
              {
                std::vector<std::pair<task_t, time_unit_t> >& ref = params.comm_successors;
                ref.clear();
                process_on_split_str(line, delimiter, [&ref](const std::string& token) {
                  std::pair<task_t, time_unit_t> p;
                  sscanf(token.c_str(), "%d,%f", &(p.first), &(p.second));
                  ref.push_back(p);
                });
              }
              break;
            case 12:
              {
                std::vector<std::pair<task_t, time_unit_t> >& ref = params.comm_predecessors;
                ref.clear();
                process_on_split_str(line, delimiter, [&ref](const std::string& token) {
                  std::pair<task_t, time_unit_t> p;
                  sscanf(token.c_str(), "%d,%f", &(p.first), &(p.second));
                  ref.push_back(p);
                });
              }
              break;
            case 13:
              {
                std::vector<time_unit_t>& ref = params.weights_matrix;
                ref.clear();
                process_on_split_str(line, delimiter, [&ref](const std::string& token) { ref.push_back(std::stof(token)); });
              }
              break;
          }
        }
        
    }  
    
    myfile.close();
  }
  catch (const std::exception& e){
    std::cout << "exception@params_from_c_readable_file:" << e.what() << std::endl;
    params = unified_params_t<default_traits_t>();
  }
  return params;
}

/*
<num_hit>
##########
<pe>
##########
<ke>
##########
<comma-sep-tasks>
##########
<ml_num_hit>
##########
<ml_pe>
##########
<comma-sep-tasks-in-ml_molecule>
========================================
(igual que la cadena antes de los iguales)
========================================
(igual que la cadena antes de los iguales)
*/

bool persist_molecules_to_c_readable_file(
    const std::string& filepath,
    const std::vector<hcro_molecule_t>& molecules){
  try{


    std::ofstream myfile;
    myfile.open(filepath);
    myfile << std::fixed;

    for(const hcro_molecule_t& molecule : molecules){
    
      myfile << molecule.get_num_hit() << std::endl;
      myfile << "##########" << std::endl;
      myfile << std::setprecision(5) << molecule.get_pe() << std::endl;
      myfile << "##########" << std::endl;
      myfile << std::setprecision(5) << molecule.get_ke() << std::endl;
      myfile << "##########" << std::endl;

      for(const task_t& tid : molecule.get_inner_molecule()){
        myfile << tid << ";";
      }

      myfile << std::endl;
      myfile << "##########" << std::endl;
      myfile << molecule.get_ml_num_hit() << std::endl;
      myfile << "##########" << std::endl;
      myfile << std::setprecision(5) << molecule.get_ml_pe() << std::endl;
      myfile << "##########" << std::endl;
      
      for(const task_t& tid : molecule.get_ml_inner_molecule()){
        myfile << tid << ";";
      }
      myfile << std::endl;
      myfile << "========================================" << std::endl;
    }

    myfile.close();
    return true;
  }
  catch (const std::exception& e){
    std::cout << "exception@persist_molecules_to_c_readable_file:" << e.what() << std::endl;
  }
  return false;
}

/*
<num_hit>
##########
<pe>
##########
<ke>
##########
<comma-sep-tasks>
##########
<ml_num_hit>
##########
<ml_pe>
##########
<comma-sep-tasks-in-ml_molecule>
========================================
(igual que la cadena antes de los iguales)
========================================
(igual que la cadena antes de los iguales)
*/

// at least one instance of hcro_algorithm must exist, should be our instance
std::vector<hcro_molecule_t> molecules_from_c_readable_file(
    const std::string& filepath){
  

  hcro_molecule_t m;
  std::vector<hcro_molecule_t> res;
  m.init(true, false);

  try{

    std::ifstream myfile(filepath);
    int i = 0;
    std::string line, delimiter;
    delimiter = ";";

    
    while(std::getline(myfile, line)){
        if(line.rfind("###", 0) == 0){ //starts with
          continue;
        }
        else if(line.rfind("===", 0) == 0){
          i = 0;
        }
        else{

          switch(i++){
            case 0:
              m.set_num_hit(std::stoi(line));
              break;
            case 1:
              m.set_pe(std::stof(line));
              break;
            case 2:
              m.set_ke(std::stof(line));
              break;
            case 3:
              {
               std::vector<task_t>& ref = m.get_inner_molecule();
               ref.clear();
               process_on_split_str(line, delimiter, [&ref](const std::string& token) { ref.push_back(std::stoi(token)); });
              }
              break;
            case 4:
              m.set_ml_num_hit(std::stoi(line));
              break;
            case 5:
              m.set_ml_pe(std::stof(line));
              break;
            default:
              {
                std::vector<task_t>& ref = m.get_ml_inner_molecule();
                ref.clear();
                process_on_split_str(line, delimiter, [&ref](const std::string& token) { ref.push_back(std::stoi(token)); });
              }
              res.push_back(m);
              break;
          }
        }
        
    }
    
    myfile.close();
  }
  catch (const std::exception& e){
    std::cout << "exception@molecules_from_c_readable_file:" << e.what() << std::endl;
    res.clear();
  }
  return res;
}



std::vector<hcro_molecule_t> imolecules_from_file(const std::string& filepath){
  std::vector<hcro_molecule_t> res;

  try{

    std::ifstream myfile(filepath);
    std::string line;
    while(std::getline(myfile, line)){
      hcro_molecule_t molec = hcro_molecule_t::from_string(
          line,
          0,
          0,
          0.0f,
          0.0f,
          true,
          true);
        molec.set_print_flags(hcro_molecule_t::print_flags_t::INNER_MOLEC);
        res.push_back(molec);
    }
    
    myfile.close();
  }
  catch (const std::exception& e){
    std::cout << "exception@imolecules_from_file:" << e.what() << std::endl;
    res.clear();
  }
  return res;
}

std::vector<std::pair<int, int> > indexes_from_file(const std::string& filepath){
  std::vector<std::pair<int, int> > res;
  std::pair<int, int> p;
  try{

    std::ifstream myfile(filepath);
    std::string line;
    while(std::getline(myfile, line)){
      sscanf(line.c_str(), "%d;%d;\n", &(p.first), &(p.second));
      res.push_back(p);
    }
    
    myfile.close();
  }
  catch (const std::exception& e){
    std::cout << "exception@indexes_from_file:" << e.what() << std::endl;
    res.clear();
  }
  return res;
}

bool append_ivalues_to_file(
    const std::string& filepath,
    int res,
    int pre_molec,
    int pre_ml,
    int pre_omolec,
    int pre_oml,
    int post_molec,
    int post_ml,
    int post_omolec,
    int post_oml){
  try{

    std::ofstream myfile;
    myfile.open(filepath, std::ios_base::app);
    myfile << res << ";" << pre_molec << ";" << pre_ml << ";" << pre_omolec << ";" << pre_oml << ";" << post_molec << ";";
    myfile <<  post_ml << ";" << post_omolec << ";" << post_oml << ";" << std::endl;
    myfile.close();
    return true;
  }
  catch (const std::exception& e){
    std::cout << "exception@append_ivalues_to_file:" << e.what() << std::endl;
  }
  return false;
}

void empty_file(const std::string& filepath){
    fclose(fopen(filepath.c_str(), "w"));
}