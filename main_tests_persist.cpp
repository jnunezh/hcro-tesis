/*
 * File:   main.cpp
 * Author: Alexander
 *
 * Created on 21 de mayo de 2015, 01:56 PM
 */

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <list>
#include <sstream>

#include "BHEFTAlgorithm.h"
#include "CPOPAlgorithm.h"
#include "HCROAlgorithm.h"
#include "Priorities.h"
#include "file_persist.h"
using namespace std;

int main(int argc, char** argv) {
  std::srand(unsigned(std::time(0)));

#ifdef CPU_MULTITHREADED
  std::cout << "==================[Multi-thread Mode]=======================" << std::endl;
#else
  std::cout << "==================[Single-thread Mode]=======================" << std::endl;
#endif

  /*TODO LIST: set priority vector;verify constraints of setnodes_info;(15 & 16
   * alfa y beta); minlocal correctness:null?,memory leaks? not initialized? pe
   * outdated:how? or better update pe everytime inner_molecule is
   * changed[better!!!]
   * ;*/
  b_rank_t bp;
  t_rank_t tp;  // a,t_rank_t tp;
  l_rank_t lp;
  std::vector<priority_cmp_t<float> > priorities(3);
  priorities[0].set_criterion(&bp);
  priorities[1].set_criterion(&tp);
  priorities[2].set_criterion(&lp);

  schedule_t s0, s1, s2, s3;
  // randomGenerate(size_t nprocs = 3, size_t ntasks = 10, float density = 0.5f,
  // float comm_lb = 1.0f, float comm_ub = 10.0f, float weights_lb = 1.0f, float
  // weights_ub = 10.0f);


  // ideas:{mean costs and values @ CPOP/HEFTB}
  // check if there's overflow;correcteness of priorities
  // check if graph generated is ok
  // prove using a custom-made graph from papers & check if priorities values
  // are correct

  // std::cout<<param.to_string()<<std::endl;

  size_t curr_procs = 30, curr_tasks = 100;  // 4, 35
  std::cout << "num_procs:" << curr_procs << ",num_tasks:" << curr_tasks << std::endl;
  /*
  ofstream myfile;
  myfile.open ("results.dat");



  size_t half;
  for(size_t curr_tasks = 10;curr_tasks < max_num_tasks;curr_tasks+=10 )
  {
      half = curr_tasks >> 1;
      for(size_t curr_procs = 3;curr_procs < max_num_processors && curr_procs <
  half;curr_procs+=2){
    */
  hms_param_t param = hms_param_t::random_generate(curr_procs, curr_tasks, 0.4f,
                                               1.5f, 9.5f, 1.0f, 10.0f);

  try {
    bheft_algorithm_t alg0;
    alg0.set_param(&param);
    s0 = alg0.run();
    std::cout << "ended bhefty" << std::endl;
  } catch (const std::exception& e) {
    std::cout << "failed@bheft:" << e.what() << std::endl;
    return 0;
  }

  try {
    cpop_algorithm_t alg2;
    alg2.set_param(&param);
    s2 = alg2.run();
    std::cout << "ended cpopy" << std::endl;
  } catch (const std::exception& e) {
    std::cout << "failed@cpop:" << e.what() << std::endl;
    return 0;
  }

  try {
    hcro_algorithm_t alg1(200,   // num_iterations
                       3,     // initial_num_groups : aka groups of sections
                       8,     // max_num_groups
                       30,    // section_amount aka elements per section
                       0.5f,  // percent elements per section initially created
                       7,     // num_candidates
                       priorities,  // priorities
                       2000.0f,     // buffer_init_val
                       0.0f,        // mu
                       1.0f,        // sigma
                       0.5f,        // collision_rate
                       200,         // alfa:char
                       100.0f,      // beta
                       10000.0f,    // ke_init
                       0.5f         // ke_loss_rate
    );
    // std::cout << "ended hcro A" << std::endl;
    alg1.set_param(&param);
    // std::cout << "ended hcro B" << std::endl;
    s1 = alg1.run();
    std::cout << "ended hcro Cy" << std::endl;
  } catch (const std::exception& e) {
    std::cout << "failed@hcro:" << e.what() << std::endl;
    return 0;
  }


  std::string inv_msg;

  if(s0.is_valid(param, inv_msg)){
    std::cout << "b-heft schedule valid" << std::endl;
  }
  else{
    std::cout << "b-heft schedule invalid:" << inv_msg << std::endl;
  }
  
  if(s1.is_valid(param, inv_msg)){
    std::cout << "hcro schedule valid" << std::endl;
  }
  else{
    std::cout << "hcro schedule invalid:" << inv_msg << std::endl;
  }
  
  if(s2.is_valid(param, inv_msg)){
    std::cout << "cpop schedule valid" << std::endl;
  }
  else{
    std::cout << "cpop schedule invalid:" << inv_msg << std::endl;
  }  
  
  std::string uid = "main000xxx";

  bool success = truncate_plot_json_file(
    "plot/plot22.json",
    s0,
    s2,
    s1,
    param);

  std::cout << "truncate_plot_json_file success:" << success << std::endl;

  if(create_dag_gexf_file_from("plot/other_gexf.gexf", param)){
    std::cout << "gexf plot file generated" << std::endl;
  }
  else{
    std::cout << "gexf plot file NOT generated" << std::endl;
  }
  
  auto res = persist_hms_params_to_json_file("data/hms_6996.json", param, uid);
  std::cout << "persist_hms_params_to_json_file => res.first:" << res.first << " res.second:" << res.second << std::endl; 
  hms_param_t param_b = hms_params_from_json_file("data/hms_6996.json", uid);
  std::cout << "param_b empty?:"<< param_b.is_empty() << std::endl;
  std::cout << "param == param_b?:" << (param == param_b) << std::endl;

  std::pair<bool, std::string> ores = persist_schedule_to_json_file("data/test_schedule.json", s0, uid);  

  std::cout << "schedule persistance result.first=>" << ores.first << " result.second=>" << ores.second << std::endl;

  s3 = schedule_from_json_file("data/test_schedule.json", uid);

  if(s3.is_empty()){
    std::cout << "s3 is empty" << std::endl;
  }
  else{
    std::cout << "s3 is not empty" << std::endl;
    if(s3 == s0){
      std::cout << "s0 & s3 are equal" << std::endl;
    }
    else{
      std::cout << "s0 & s3 are NOT equal" << std::endl; 
    }
  }

  //---------------------[new ones]----------------------------------------

  std::cout << "-----------------[new ones]---------------------" << std::endl;

  hms_param_t custom_param = hms_param_t::random_generate(5, 30, 0.4f,
                                               1.5f, 9.5f, 1.0f, 10.0f);
  
  std::cout << "custom_param.edges==>" << custom_param.get_num_edges() << std::endl;  

  try {
    hcro_algorithm_t alg1(200,   // num_iterations
                       3,     // initial_num_groups : aka groups of sections
                       8,     // max_num_groups
                       30,    // section_amount aka elements per section
                       0.5f,  // percent elements per section initially created
                       7,     // num_candidates
                       priorities,  // priorities
                       2000.0f,     // buffer_init_val
                       0.0f,        // mu
                       1.0f,        // sigma
                       0.5f,        // collision_rate
                       200,         // alfa:char
                       100.0f,      // beta
                       10000.0f,    // ke_init
                       0.5f         // ke_loss_rate
    );

    alg1.set_param(&custom_param);

    hcro_molecule_t molec = hcro_molecule_t::random_generate(
      500,
      10000,
      200.0f,
      500.0f,
      250.0f,
      1000.0f,
      true,
      true);

    uid = "mymolec1234567";

    ores = persist_molecule_to_json_file("data/test_molecule.json", molec, uid);  

    std::cout << "molecule persistance result.first=>" << ores.first << " result.second=>" << ores.second << std::endl;

    hcro_molecule_t other_molec = molecule_from_json_file("data/test_molecule.json", uid);

    if(other_molec.is_empty()){
      std::cout << "other_molec is empty" << std::endl;
    }
    else{
      std::cout << "other_molec is not empty" << std::endl;
      if(molec == other_molec){
        std::cout << "molec & other_molec are equal" << std::endl;
      }
      else{
        std::cout << "molec & other_molec are NOT equal" << std::endl; 
      }
    }

    std::cout << "-----------------[c-readable-molecule]---------------------" << std::endl;

    int i;
    bool molecs_persisted = false;
    {
      std::vector<hcro_molecule_t> generated_molecs(100);
      for(hcro_molecule_t& m : generated_molecs){
          m = hcro_molecule_t::random_generate(
            500,
            10000,
            200.0f,
            500.0f,
            250.0f,
            1000.0f,
            true,
            true);
      }
      
      i = rand_inclusive(0, generated_molecs.size() - 1);
      molec = generated_molecs[i];

      if(persist_molecules_to_c_readable_file("data/test_cr_molecs0001.txt", generated_molecs)){
        std::cout << "molecs were persisted!!!" << std::endl;     
        molecs_persisted = true;
      }
      else{
        std::cout << "molecs could not be persisted!!!" << std::endl;
      }      
    }

    if(molecs_persisted){
      std::vector<hcro_molecule_t> molecs = molecules_from_c_readable_file("data/test_cr_molecs0001.txt");
      other_molec = molecs[i];
      
      if(other_molec.is_empty()){
        std::cout << "other_molec is empty" << std::endl;
      }
      else{
        std::cout << "other_molec is not empty" << std::endl;
        if(molec == other_molec){
          std::cout << "molec & other_molec are equal" << std::endl;
        }
        else{
          std::cout << "molec & other_molec are NOT equal" << std::endl;
        }
      }
    }

    std::cout << "-----------------[c-readable-unified-params]-----------" << std::endl;

    unified_params_t<default_traits_t> u_params = custom_param.to_unified_params_t<default_traits_t>(
      alg1.get_buffer_value(),
      alg1.get_mu(),
      alg1.get_sigma(),
      alg1.get_collision_rate(),
      alg1.get_alpha(),
      alg1.get_beta(),
      alg1.get_ke_init_value(),
      alg1.get_ke_loss_rate(),
      true,
      get_curr_epoch_ns());

    if(persist_params_to_c_readable_file("data/test_cr_params.txt", u_params)){
      std::cout << "params were persisted!!!" << std::endl;     
      
      unified_params_t<default_traits_t> other_params = params_from_c_readable_file("data/test_cr_params.txt");

      if(other_params.is_empty()){
        std::cout << "other_params is empty" << std::endl;
      }
      else{
        std::cout << "other_params is not empty" << std::endl;
        if(u_params == other_params){
          std::cout << "u_params & other_params are equal" << std::endl;
        }
        else{
          std::cout << "u_params & other_params are NOT equal" << std::endl;
        }
      }

    }
    else{
      std::cout << "u_params could not be persisted!!!" << std::endl;
    }         

    std::cout << "ended hcro molecule experiment++" << std::endl;
  } catch (const std::exception& e) {
    std::cout << "failed@hcro molecule experiment++:" << e.what() << std::endl;
    return 0;
  }  
  
  std::cout << "fin" << std::endl;
  return 0;
}
