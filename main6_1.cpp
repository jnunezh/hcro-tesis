/*
 * File:   main.cpp
 * Author: Alexander
 *
 * Created on 21 de mayo de 2015, 01:56 PM
 */

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <list>
#include <sstream>

#include "Utils.h"
#include "BHEFTAlgorithm.h"
#include "CPOPAlgorithm.h"
#include "HCROAlgorithm.h"
#include "Priorities.h"
#include "file_persist.h"

using namespace std;

int main(int argc, char** argv) {
  std::srand(unsigned(std::time(0)));

#ifdef CPU_MULTITHREADED
  std::cout << "==================[Multi-thread Mode]=======================" << std::endl;
#else
  std::cout << "==================[Single-thread Mode]=======================" << std::endl;
#endif

  size_t curr_procs = 100, curr_tasks = 400;
  int iterations = 100;
  std::string params_filepath, uid;
  params_filepath = "data/hms_params_fail_9867.json";
  uid = "testie0001aqqqqq"; 
  hms_param_t params;
  schedule_t s0, s1;
  float e0, e1;
  
  do{

    params = hms_param_t::random_generate(curr_procs, curr_tasks, 0.4f,
                                               1.5f, 9.5f, 1.0f, 10.0f);
    try {
      bheft_algorithm_t alg0;
      alg0.set_param(&params);
      assign_elapsed_ms(e0, s0 = alg0.old_run());
    } catch (const std::exception& e) {
      std::cout << "failed@bheft_algorithm_t.old_run():" << e.what() << std::endl;
      return 0;
    }

    try {
      bheft_algorithm_t alg0;
      alg0.set_param(&params);
      assign_elapsed_ms(e1, s1 = alg0.run()); //print_elapsed_ms();
    } catch (const std::exception& e) {
      std::cout << "failed@bheft_algorithm_t.run():" << e.what() << std::endl;
      return 0;
    }

    std::cout << "new eq " << e1/e0 << "x old" << std::endl;
  
  }while(s0 == s1 && iterations-- > 0);

  std::string msg;
  if(s0.is_valid(params, msg)){
    std::cout << "s0 is valid" << std::endl;
  }
  else{
    std::cout << "s0 is INVALID:" << msg << std::endl;
  }

  if(s1.is_valid(params, msg)){
    std::cout << "s1 is valid" << std::endl;
  }
  else{
    std::cout << "s1 is INVALID:" << msg << std::endl;
  }
  
  if(s0 != s1){
    std::pair<bool, std::string> res = persist_hms_params_to_json_file(params_filepath, params, uid);
    std::cout << "params persisted!!" << std::endl;

    std::cout << "s0 != s1" << std::endl;

    std::cout << params;

    std::cout << "@@@@@@@@@@@@@@@@[s0]@@@@@@@@@@@@@@@@@@@@@@@" << std::endl;
    std::cout << s0;
    std::cout << "################[s1]#######################" << std::endl;
    std::cout << s1;   
       
  }
  else{
    std::cout << "s0 == s1" << std::endl;    
  }
  std::cout << "num_iterations:" << iterations << std::endl;
  std::cout << "fin" << std::endl;
  return 0;
}
