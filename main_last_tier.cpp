/*
 * File:   main.cpp
 * Author: Alexander
 *
 * Created on 21 de mayo de 2015, 01:56 PM
 */

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <list>
#include <sstream>

#include "Utils.h"
#include "HCROAlgorithm.h"
#include "Utils2.h"
#include "Priorities.h"
#include "file_persist.h"
using namespace std;



int main(int argc, char** argv) {
  std::srand(unsigned(std::time(0)));

#ifdef CPU_MULTITHREADED
  std::cout << "==================[Multi-thread Mode]=======================" << std::endl;
#else
  std::cout << "==================[Single-thread Mode]=======================" << std::endl;
#endif

  //size_t num_procs = 6;
  //size_t num_tasks = 35;
  b_rank_t bp;
  t_rank_t tp;  // a,t_rank_t tp;
  l_rank_t lp;
  std::vector<priority_cmp_t<float> > priorities(3);
  priorities[0].set_criterion(&bp);
  priorities[1].set_criterion(&tp);
  priorities[2].set_criterion(&lp);

  int index, other_index;
  std::string uid = "uid_created_new0002";
  std::string hms_param_path = "./data/created_params.json";
  std::string cr_params_path = "./data/cr_params.txt";
  hms_param_t custom_param;
  custom_param = hms_params_from_json_file(hms_param_path, uid);

  assert(!custom_param.is_empty());
  

  std::cout << "custom_param.edges==>" << custom_param.get_num_edges() << std::endl;

  //std::cout << custom_param << std::endl;


  unified_params_t<default_traits_t> uparams = params_from_c_readable_file(cr_params_path);

  try {
    hcro_algorithm_t alg1(200,   // num_iterations
                       3,     // initial_num_groups : aka groups of sections
                       8,     // max_num_groups
                       30,    // section_amount aka elements per section
                       0.5f,  // percent elements per section initially created
                       7,     // num_candidates
                       priorities,  // priorities
                       uparams.buffer_value,     // buffer_init_val
                       uparams.mu,        // mu
                       uparams.sigma,        // sigma
                       uparams.collision_rate,        // collision_rate
                       uparams.alpha,         // alfa:char
                       uparams.beta,      // beta
                       uparams.ke_init,    // ke_init
                       uparams.ke_loss_rate         // ke_loss_rate
    );

    alg1.set_param(&custom_param);

    std::vector<hcro_molecule_t> section_molecs = imolecules_from_file("./../hcro_tesis_tests/data/worker_molecs.txt");
    empty_file("./data/results.txt");
    reaction_min_t selected_ms, min_ms;
    int pre_molec, pre_ml, pre_omolec, pre_oml, post_molec, post_ml, post_omolec, post_oml, result;
    int index, other_index, lb = 20;
    std::vector<std::pair<int, int> > indexes = indexes_from_file("./../hcro_tesis_tests/data/indexes.txt");
    min_ms.makespan = plus_infinity<time_unit_t>();

    for(int i = 0; i < section_molecs.size(); ++i){

      index = indexes[i].first - lb;
      other_index = indexes[i].second - lb;

      hcro_molecule_t& selected = section_molecs[index];
      hcro_molecule_t& other_selected = section_molecs[other_index];

      pre_molec = selected.get_inner_molec_digest();
      pre_ml = selected.get_min_local().get_inner_molec_digest(); 
      
      pre_omolec = other_selected.get_inner_molec_digest();
      pre_oml = other_selected.get_min_local().get_inner_molec_digest();
      
      do_single_reaction(
        selected,
        index,
        other_selected,
        other_index,
        selected_ms);

      post_molec = selected.get_inner_molec_digest();
      post_ml = selected.get_min_local().get_inner_molec_digest();
      
      post_omolec = other_selected.get_inner_molec_digest();
      post_oml = other_selected.get_min_local().get_inner_molec_digest();

      result = pre_molec + pre_ml + pre_omolec + pre_oml + post_molec + post_ml + post_omolec + post_oml;

      if(selected_ms.makespan < min_ms.makespan){
        min_ms = selected_ms;
      }

      append_ivalues_to_file("./data/results.txt", std::abs(result), std::abs(pre_molec), std::abs(pre_ml), std::abs(pre_omolec), std::abs(pre_oml), std::abs(post_molec), std::abs(post_ml), std::abs(post_omolec), std::abs(post_oml));

    }

    std::cout << "min_ms.makespan:" << min_ms.makespan << ", molecule_index:" << min_ms.molecule_index + lb << ",ml_min:" << min_ms.ml_min << std::endl;

  } catch (const std::exception& e) {
    std::cout << "failed@maim_mem_test:" << e.what() << std::endl;
    return 0;
  }  
  
  std::cout << "fin" << std::endl;
  return 0;
}
