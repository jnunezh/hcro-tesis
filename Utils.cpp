#include "Utils.h"
#include <cmath>
#include <ctgmath>
#include <iostream>
#include <map>
#include <set>
#include "Priorities.h"


template<>
int sto_type<int>(const std::string& str){
  return std::stoi(str);
}

template<>
short sto_type<short>(const std::string& str){
  return std::stoi(str);
}

template<>
unsigned short sto_type<unsigned short>(const std::string& str){
  return std::stoi(str);
}

template<>
long sto_type<long>(const std::string& str){
  return std::stol(str);
}

template<>
long long sto_type<long long>(const std::string& str){
  return std::stoll(str);
}

template<>
unsigned long sto_type<unsigned long>(const std::string& str){
  return std::stoul(str);
}

template<>
unsigned long long sto_type<unsigned long long>(const std::string& str){
  return std::stoull(str);
}

template<>
float sto_type<float>(const std::string& str){
  return std::stof(str);
}

template<>
double sto_type<double>(const std::string& str){
  return std::stod(str);
}

template<>
long double sto_type<long double>(const std::string& str){
  return std::stold(str);
}

hms_param_t::hms_param_t() : num_procs(0), num_tasks(0) {
}

hms_param_t::hms_param_t(size_t ntasks, size_t nprocs) {
  num_procs = nprocs;
  num_tasks = ntasks;
  limit_offsets.resize(num_tasks);
  weights_matrix.reserve(num_procs * num_tasks);
}

hms_param_t::hms_param_t(const hms_param_t& other) {
   this->operator=(other); 
}

hms_param_t& hms_param_t::operator=(const hms_param_t& other) {
  num_procs = other.num_procs;
  num_tasks = other.num_tasks;
  num_edges = other.num_edges;
  comm_successors = other.comm_successors;
  comm_predecessors = other.comm_predecessors;
  weights_matrix = other.weights_matrix;
  predecessors = other.predecessors;
  successors = other.successors;
  limit_offsets = other.limit_offsets;
}


void hms_param_t::set_nodes_info(
    const std::vector<node_info_t>& nodes_info){
  task_t tid = 0;
  size_t distance;
  size_t past_offset = 0;
  assert(num_tasks == limit_offsets.size());
  assert(num_tasks == nodes_info.size());
  std::vector<size_t> predecessors_count(num_tasks, 0);

  for (const node_info_t node : nodes_info) {
    assert(tid == node.tid);

    for (const std::pair<task_t, time_unit_t>& descendent : node.descendents) {
      ++(predecessors_count[descendent.first]); 
    }

    assert(node.task_weights.size() == num_procs);
    weights_matrix.insert(weights_matrix.end(), node.task_weights.begin(),
                          node.task_weights.end());
    limit_offsets[tid].first = past_offset;
    past_offset += node.descendents.size();
    ++tid;
  }
  this->num_edges = past_offset;

  successors.reserve(past_offset);
  predecessors.resize(past_offset, plus_infinity<task_t>());
  comm_successors.resize(past_offset);
  comm_predecessors.resize(past_offset);

  past_offset = 0;
  for(tid = 0; tid < num_tasks; ++tid){
    limit_offsets[tid].second = past_offset;
    past_offset += predecessors_count[tid];
  }

  std::fill(predecessors_count.begin(), predecessors_count.end(), 0);

  std::vector<task_t>::const_iterator succ_begin, pos, succ_end;
  std::vector<task_t>::iterator last, other_begin, other_end, other_pos, ex;
  std::vector<time_unit_t>::iterator opos;
  
  for (const node_info_t node : nodes_info) {

    for (const std::pair<task_t, time_unit_t>& descendent : node.descendents) {

      succ_begin = successors.begin() + limit_offsets[node.tid].first;
      succ_end = successors.end();

      pos = std::lower_bound(
        succ_begin,
        succ_end,
        descendent.first);  // >=
      
      if(pos != succ_end && *pos == descendent.first){
        assert(0); //repeated task
      }

      distance = std::distance(std::vector<task_t>::const_iterator(successors.begin()), pos);

      successors.insert(pos, descendent.first);
      opos = comm_successors.begin() + distance;
      *opos = descendent.second;

      other_begin = predecessors.begin() + limit_offsets[descendent.first].second;
      other_end = other_begin + predecessors_count[descendent.first];

      other_pos = std::lower_bound(
        other_begin,
        other_end,
        node.tid);  // >=
      
      std::rotate(other_pos, other_end, other_end + 1);
      
      *other_pos = node.tid;     

      distance = std::distance(predecessors.begin(), other_pos);
      opos = comm_predecessors.begin() + distance;
      *opos = descendent.second;   

      ++(predecessors_count[descendent.first]);

    }
  }

  assert(successors.size() == predecessors.size());
  assert(comm_successors.size() == comm_predecessors.size());
  assert(successors.size() == comm_successors.size());
  assert(successors.size() == this->num_edges);
}


void hms_param_t::sorted_predecessors(
    task_t tid, std::vector<task_t>::const_iterator& begin,
    std::vector<task_t>::const_iterator& end) const {
  begin = predecessors.begin() + limit_offsets[tid].second;
  task_t last_tid = num_tasks - 1;
  if (tid < last_tid)
    end = predecessors.begin() + limit_offsets[tid + 1].second;
  else
    end = predecessors.end();
}

void hms_param_t::sorted_successors(
    task_t tid, std::vector<task_t>::const_iterator& begin,
    std::vector<task_t>::const_iterator& end) const {
  begin = successors.begin() + limit_offsets[tid].first;
  task_t last_tid = num_tasks - 1;
  if (tid < last_tid)
    end = successors.begin() + limit_offsets[tid + 1].first;
  else
    end = successors.end();
}

void hms_param_t::comm_times_of_sorted_predecessors(
    task_t tid, std::vector<time_unit_t>::const_iterator& begin,
    std::vector<time_unit_t>::const_iterator& end) const {
  begin = comm_predecessors.begin() + limit_offsets[tid].second;
  task_t last_tid = num_tasks - 1;
  if (tid < last_tid)
    end = comm_predecessors.begin() + limit_offsets[tid + 1].second;
  else
    end = comm_predecessors.end();
}

void hms_param_t::comm_times_of_sorted_successors(
    task_t tid, std::vector<time_unit_t>::const_iterator& begin,
    std::vector<time_unit_t>::const_iterator& end) const {
  begin = comm_successors.begin() + limit_offsets[tid].first;
  task_t last_tid = num_tasks - 1;
  if (tid < last_tid)
    end = comm_successors.begin() + limit_offsets[tid + 1].first;
  else
    end = comm_successors.end();
}

std::vector<std::pair<task_t, time_unit_t> > hms_param_t::get_merged_comm_successors() const{
  std::vector<std::pair<task_t, time_unit_t> > r(this->comm_successors.size());
  
  std::vector<task_t>::const_iterator t_iter = this->successors.begin();
  std::vector<time_unit_t>::const_iterator c_iter = this->comm_successors.begin();

  for(std::pair<task_t, time_unit_t>& p : r){
    p.first = *t_iter;
    p.second = *c_iter;
    ++t_iter;
    ++c_iter;
  }

  return r;
}

std::vector<std::pair<task_t, time_unit_t> > hms_param_t::get_merged_comm_predecessors() const{
  std::vector<std::pair<task_t, time_unit_t> > r(this->comm_successors.size());

  std::vector<task_t>::const_iterator t_iter = this->predecessors.begin();
  std::vector<time_unit_t>::const_iterator c_iter = this->comm_predecessors.begin();

  for(std::pair<task_t, time_unit_t>& p : r){
    p.first = *t_iter;
    p.second = *c_iter;
    ++t_iter;
    ++c_iter;
  }  
  return r;
}

time_unit_t hms_param_t::weight_at(task_t tid, proc_t pid) const {
  return weights_matrix[(tid * num_procs) + pid];
}

void hms_param_t::weights_of(
    task_t tid, std::vector<time_unit_t>::const_iterator& begin,
    std::vector<time_unit_t>::const_iterator& end) const {
  begin = weights_matrix.begin() + (tid * num_procs);
  end = begin + num_procs;
}
int hms_param_t::first_succesor_index_of(int task_index,
                                  const std::vector<task_t>& imolecule) const {
  int q;
  std::vector<task_t>::const_iterator curr, end, ccurr;
  sorted_successors(imolecule[task_index], curr, end);
  ccurr = imolecule.begin() + task_index + 1;
  for (q = task_index + 1; q < imolecule.size(); ++q, ++ccurr) {
    if (std::binary_search(curr, end, *ccurr)) break;
  }
  return q;
}
int hms_param_t::last_predecessor_index_of(
    int task_index, const std::vector<task_t>& imolecule) const {
  int q;
  std::vector<task_t>::const_iterator curr, end, ccurr;
  sorted_predecessors(imolecule[task_index], curr, end);
  ccurr = imolecule.begin() + task_index - 1;
  for (q = task_index - 1; q > 0; --q, --ccurr) {
    if (std::binary_search(curr, end, *ccurr)) break;
  }
  return q;
}

void hms_param_t::topological_sort_util(task_t v, std::vector<bool>& visited,
                                    std::list<task_t>& s) const {
  visited[v] = true;
  std::vector<task_t>::const_iterator curr, end;
  sorted_successors(v, curr, end);
  for (; curr != end; ++curr) {
    if (!visited[*curr]) topological_sort_util(*curr, visited, s);
  }
  s.push_front(v);
}

std::list<task_t> hms_param_t::to_sorted_topo_list() const {
  assert(comm_successors.size() > 0 && weights_matrix.size() > 0);
  std::list<task_t> ret_list;
  std::vector<bool> visited(num_tasks, false);
  task_t end = num_tasks;
  for (task_t tid = 0; tid < end; ++tid) {
    if (!visited[tid]) topological_sort_util(tid, visited, ret_list);
  }
  return ret_list;
}

//@@@@Topcuoglu, H.; Hariri, S.; Min-You Wu, -- Performance-effective and
//low-complexity tid schedulin.pdf:pag7
void hms_param_t::to_hetero_critical_path(crit_path_out_arg_t& arg) const {
  arg.priorities.resize(get_num_tasks(), 0.0f);

  std::vector<time_unit_t> minimized_proc_weights(get_num_procs(), 0.0);
  std::list<task_t> topolist = to_sorted_topo_list();
  time_unit_t cp, min;
  std::vector<float> tps(get_num_tasks()), bps(get_num_tasks());

  t_rank_t op;
  op.generate(*this, tps);
  b_rank_t oop;
  oop.generate(*this, bps);

  std::vector<float>::const_iterator oit, ooit;
  oit = tps.begin();
  ooit = bps.begin();
  for (float& priority : arg.priorities) {
    priority = *oit + *ooit;
    ++oit;
    ++ooit;
  }

  cp = arg.priorities[*(topolist.begin())];
  task_t nj, nk = *(topolist.begin());
  task_t end_task = *(topolist.rbegin());
  size_t proc, num_procs = get_num_procs();
  std::vector<task_t>::const_iterator curr, end;
  while (1) {
    if (nk == end_task) {
      min = plus_infinity<time_unit_t>();
      for (proc = 0; proc < num_procs; ++proc) {
        if (minimized_proc_weights[proc] < min) {
          min = minimized_proc_weights[proc];
          arg.cpp = proc;
        }
      }
      break;
    }
    for (proc = 0; proc < num_procs; ++proc)
      minimized_proc_weights[proc] += weight_at(nk, proc);
    arg.critical_path.push_back(nk);
    nj = plus_infinity<task_t>();
    sorted_successors(nk, curr, end);
    for (; curr != end; ++curr) {
      if (almost_equal(arg.priorities[*curr], cp)) {
        nj = *curr;
        break;
      }
    }

    assert(nj != plus_infinity<task_t>());

    nk = nj;
  }
}

hms_param_t hms_param_t::random_generate(size_t nprocs, size_t ntasks, float density,
                                    time_unit_t comm_lb, time_unit_t comm_ub,
                                    time_unit_t weights_lb, time_unit_t weights_ub) {
  assert(nprocs > 0 && ntasks > 0);
  assert(density > 0.0f && density <= 1.0f);
  assert(comm_lb < comm_ub);
  assert(weights_lb < weights_ub);
  std::vector<node_info_t> nodes_info(ntasks);
  hms_param_t res(ntasks, nprocs);
  int max_num_succ = ntasks - 1;
  task_t tid = 0, tmp, last_tid = ntasks - 1;
  int curr_sz;
  time_unit_t rnd;
  std::vector<std::pair<task_t, time_unit_t> >::const_iterator geq_itask;
  std::vector<int> pred_count(ntasks, 0), succ_count(ntasks, 0);
  for (node_info_t& node : nodes_info) {
    node.tid = tid;
    node.task_weights.resize(nprocs);
    for (time_unit_t& twi : node.task_weights) {
      twi = rand_inclusive<time_unit_t>(weights_lb, weights_ub);
    }
    if (tid > 0 && tid < last_tid) {
      curr_sz = max_num_succ * density;
      succ_count[tid] += curr_sz;
      node.descendents.reserve(curr_sz);
      while(node.descendents.size() < curr_sz){
        do{
          tmp = rand_inclusive(tid + 1, last_tid - 1);
          rnd =  rand_inclusive<time_unit_t>(comm_lb, comm_ub);

          geq_itask = std::lower_bound(
            node.descendents.begin(),
            node.descendents.end(),
            std::pair<task_t, time_unit_t>(tmp, rnd),
            asc_first_cmp_t<task_t, time_unit_t>());

        }while(geq_itask != node.descendents.end() && geq_itask->first == tmp);

        node.descendents.insert(geq_itask, std::pair<task_t, time_unit_t>(tmp, rnd));
      }
    }
    --max_num_succ;
    ++tid;
  }
  std::set<std::pair<task_t, time_unit_t>, asc_first_cmp_t<task_t, time_unit_t> >
      parentless_set;
  for (tid = 1; tid < last_tid; ++tid) {
    if(pred_count[tid] < 1) {
      parentless_set.insert(std::pair<task_t, time_unit_t>(
          tid, rand_inclusive<time_unit_t>(comm_lb, comm_ub)));
    }
    if(succ_count[tid] < 1) {
      nodes_info[tid].descendents.resize(1);
      std::pair<task_t, time_unit_t>& ref = nodes_info[tid].descendents[0];
      ref.first = last_tid;
      ref.second = rand_inclusive<time_unit_t>(comm_lb, comm_ub);
    }
  }
  nodes_info.begin()->descendents.insert(nodes_info.begin()->descendents.end(),
                                         parentless_set.begin(),
                                         parentless_set.end());

  /*via counting find succ-less and predless & link to 0 & last_idx to ensure
   * DAG struct*/

  res.set_nodes_info(nodes_info);
  return res;
}

bool hms_param_t::operator==(const hms_param_t& other) const {
  if(num_procs != other.num_procs){
    return false;
  }
  if(num_tasks != other.num_tasks){
    return false;
  }
  if(successors != other.successors){
    return false;
  }
  if(predecessors != other.predecessors){
    return false;
  }
  if(limit_offsets != other.limit_offsets){
    return false;
  }
  
  if(comm_successors.size() != other.comm_successors.size()){
    return false;
  }
  for(size_t i = 0; i < comm_successors.size(); ++i){
    if(!almost_equal(comm_successors[i], other.comm_successors[i])){
      return false;
    }
  }

  if(comm_predecessors.size() != other.comm_predecessors.size()){
    return false;
  }
  for(size_t i = 0; i < comm_predecessors.size(); ++i){
    if(!almost_equal(comm_predecessors[i], other.comm_predecessors[i])){
      return false;
    }
  }

  if(weights_matrix.size() != other.weights_matrix.size()){
    return false;
  }
  for(size_t i = 0; i < weights_matrix.size(); ++i){
    if(!almost_equal(weights_matrix[i], other.weights_matrix[i])){
      return false;
    }
  }

  return true;
}

hms_param_t hms_param_t::random_generate2(size_t nprocs, size_t ntasks, float density,
                                    time_unit_t comm_lb, time_unit_t comm_ub,
                                    time_unit_t weights_lb, time_unit_t weights_ub) {
  assert(nprocs > 0 && ntasks > 3);
  assert(density > 0.0f && density <= 1.0f);
  assert(comm_lb < comm_ub);
  assert(weights_lb < weights_ub);
  std::vector<node_info_t> nodes_info(ntasks);
  hms_param_t res(ntasks, nprocs);
  size_t bounded_num_succ, accum_sz;
  task_t tid = 0, last_tid = ntasks - 1;
  task_t tmp, mapped_tid, mapped_tid_lb, mapped_tid_ub, original_succ_tid;
  time_unit_t rnd;
  union_find_data_struct_t uf(ntasks - 2);
  float temp;
  std::vector<int> pred_count(ntasks, 0), succ_count(ntasks, 0);
  std::vector<task_t> succ_candidates;
  succ_candidates.reserve(ntasks - 2);
  mapped_tid_lb = 0;
  mapped_tid_ub = ntasks - 3;

  auto custom_less_than_cmp = [](
    const std::pair<task_t, time_unit_t>& a,
    const std::pair<task_t, time_unit_t>& b){
    
    return a.first < b.first;
  };

  for (node_info_t& node : nodes_info){
    node.task_weights.resize(nprocs);
    node.tid = tid;
    for (time_unit_t& twi : node.task_weights) {
      twi = rand_inclusive<time_unit_t>(weights_lb, weights_ub);
    }
    if (tid > 0 && tid < last_tid) {
      mapped_tid = tid - 1;

      succ_candidates.clear();

      for(task_t curr_tid = mapped_tid_lb; curr_tid <= mapped_tid_ub; ++curr_tid){
        if(curr_tid != mapped_tid){
          if(!uf.connected(mapped_tid, curr_tid)){
            succ_candidates.push_back(curr_tid);
          }
        }
      }

      bounded_num_succ = round(static_cast<float>(succ_candidates.size()) * density);
      
      succ_count[tid] += bounded_num_succ;
      
      if(bounded_num_succ > 0){
        node.descendents.reserve(bounded_num_succ);
        custom_random_shuffle(succ_candidates.begin(), succ_candidates.end());
        node.descendents.clear();
        
        accum_sz = 0;

        for(const task_t& successor_mapped_tid : succ_candidates){
          rnd =  rand_inclusive<time_unit_t>(comm_lb, comm_ub);
          original_succ_tid = successor_mapped_tid + 1;
          uf.merge(mapped_tid, successor_mapped_tid);
          
          insert_into_sorted(
            node.descendents,
            std::pair<task_t, time_unit_t>(original_succ_tid, rnd),
            custom_less_than_cmp
          );
          
          ++(pred_count[original_succ_tid]);
          if(++accum_sz >= bounded_num_succ){
            break;
          }

        }        
      }
    }
    ++tid;
  }

  std::set<std::pair<task_t, time_unit_t>, asc_first_cmp_t<task_t, time_unit_t> >
      parentless_set;
  for (tid = 1; tid < last_tid; ++tid) {
    if (pred_count[tid] < 1) {
      parentless_set.insert(std::pair<task_t, time_unit_t>(
          tid, rand_inclusive<time_unit_t>(comm_lb, comm_ub)));
    }
    if (succ_count[tid] < 1) {
      nodes_info[tid].descendents.resize(1);
      std::pair<task_t, time_unit_t>& ref = nodes_info[tid].descendents[0];
      ref.first = last_tid;
      ref.second = rand_inclusive<time_unit_t>(comm_lb, comm_ub);
    }
  }

  std::vector<node_info_t>::iterator first_node_it = nodes_info.begin();
  first_node_it->descendents.insert(
    first_node_it->descendents.end(),
    parentless_set.begin(),
    parentless_set.end());

  /*via counting find succ-less and predless & link to 0 & last_idx to ensure
   * DAG struct*/
  res.set_nodes_info(nodes_info);
  return res;
}

std::vector<std::set<task_t> > hms_param_t::predecessors_as_vector_of_sets() const {
  std::vector<std::set<task_t> > ret(num_tasks);
  std::vector<std::set<task_t> >::iterator curr = ret.begin();
  size_t last_tid = num_tasks - 1;
  for (size_t tid = 0; tid < last_tid; ++tid, ++curr) {
    curr->insert(predecessors.begin() + limit_offsets[tid].second,
                 predecessors.begin() + limit_offsets[tid + 1].second);
  }
  curr->insert(predecessors.begin() + limit_offsets[last_tid].second,
               predecessors.end());
  return ret;
}

std::vector<time_unit_t> hms_param_t::to_avg_weights() const {
  assert(num_tasks > 0);
  std::vector<time_unit_t> res;
  res.resize(num_tasks);
  time_unit_t tmp = 0.0;
  std::vector<time_unit_t>::const_iterator ini, end = weights_matrix.begin();
  for (time_unit_t& slot : res) {
    tmp = 0.0;
    ini = end;
    end = ini + num_procs;
    slot = (std::accumulate(ini, end, tmp)) / ((time_unit_t)num_procs);
  }
  return res;
}
std::vector<time_unit_t> hms_param_t::get_min_weights() const {
  assert(num_tasks > 0);
  std::vector<time_unit_t> res;
  res.resize(num_tasks);
  std::vector<time_unit_t>::const_iterator ini, end = weights_matrix.begin();
  for (time_unit_t& slot : res) {
    ini = end;
    end = ini + num_procs;
    slot = *(std::min_element(ini, end));
  }
  return res;
}
std::vector<time_unit_t> hms_param_t::get_avg_communication() const {
  assert(num_tasks > 0);
  std::vector<time_unit_t> res;
  time_unit_t tmp;
  res.resize(num_tasks);
  std::vector<time_unit_t>::const_iterator begin, end;
  for (const task_t& tid : this->to_sorted_topo_list()) {
    comm_times_of_sorted_successors(tid, begin, end);
    tmp = 0.0;
    std::accumulate(begin, end, tmp);
    res[tid] = tmp / (std::distance(begin, end));
  }
  return res;
}

//------------------------------------------------------------------
std::string hms_param_t::stringify_weights() const {
  std::ostringstream str;
  std::vector<time_unit_t>::const_iterator begin, end;
  str << "[";
  for (task_t tid = 0; tid < num_tasks; ++tid) {
    str << "[";
    weights_of(tid, begin, end);
    for (; begin != end; ++begin) {
      str << *begin;
      if (std::distance(begin, end) > 1) str << ",";
    }
    str << "]";
    if (num_tasks - tid > 1) str << ",";
  }
  str << "]";
  return str.str();
}

std::string hms_param_t::to_string() const {
  std::ostringstream str;

  task_t tid;
  std::vector<task_t>::const_iterator curr, end;
  for (tid = 0; tid < num_tasks; ++tid) {
    sorted_successors(tid, curr, end);
    str << (tid < 10 ? " " : "") << tid << ":";
    for (; curr != end; ++curr) str << (*curr < 10 ? " " : "") << *curr << " ";
    str << "\n";
  }

  return str.str();
}

std::ostream& operator<<(std::ostream& os, const node_info_t& n) {
  os << "tid:" << n.tid << "=>";
  for(const auto& p : n.descendents){
    os << p << " ";
  }
  if(n.descendents.size() < 1){
    os << "[]";
  }
  return os;
}

std::ostream& operator<<(std::ostream& os, const hms_param_t& p) {
  
  std::vector<task_t>::const_iterator curr, end;
  std::vector<time_unit_t>::const_iterator ccurr, cend;

  for(task_t tid = 0; tid < p.get_num_tasks(); ++tid){
    os << "============[tid:" << tid << "]==========" << std::endl;
    os << "children:" << std::endl;

    
    p.sorted_successors(tid, curr, end);
    p.comm_times_of_sorted_successors(tid, ccurr, cend);
    
    for (; curr != end; ++curr, ++ccurr) {
      os << "\t" << "{tid:" << *curr << ", comm_time:" << *ccurr << "}" << std::endl;
    }

    os << "ancestors:" << std::endl;
    p.sorted_predecessors(tid, curr, end);
    p.comm_times_of_sorted_predecessors(tid, ccurr, cend);
    
    for (; curr != end; ++curr, ++ccurr) {
      os << "\t" << "{tid:" << *curr << ", comm_time:" << *ccurr << "}" << std::endl;
    }

    os << "weights:" << std::endl;
    p.weights_of(tid, ccurr, cend);
    
    for (proc_t pid = 0; ccurr != cend; ++ccurr, ++pid) {
      os << "\t" << "{pid:" << pid << ", duration:" << *ccurr << "}" << std::endl;
    }
  }
  return os;
}

std::ostream& operator<<(std::ostream& os, const max_entry_t& entry){
    os << "***************[max_entry_t print]******************" << std::endl;
    os << "first_max:" << entry.first_max << std::endl;
    os << "second_max:" << entry.second_max << std::endl;
    os << "fm_pid:" << entry.fm_pid << std::endl;
    os << "sm_pid:" << entry.sm_pid << std::endl;
    return os;
}

//-----------------------------------------------------------------------
bool hms_param_t::try_insertion_policy(
    task_t tid,
    proc_t pid,
    const std::vector<time_unit_t>& afts,
    const std::vector<proc_t>& proc_assignmts,
    std::vector<std::pair<task_t, time_unit_t> >::const_iterator& geq_itask,
    const std::vector<std::pair<task_t, time_unit_t> >& processor_runway,
    const time_unit_t& min_eft,
    time_unit_t& ast,
    time_unit_t& eft) const {

  time_unit_t est, tmp, avail, duration_on_proc;
  duration_on_proc = weight_at(tid, pid);
  std::vector<task_t>::const_iterator curr, end;
  std::vector<time_unit_t>::const_iterator ccurr, cend;
  est = default_zero_value<time_unit_t>();
  sorted_predecessors(tid, curr, end);
  comm_times_of_sorted_predecessors(tid, ccurr, cend);
  
  time_unit_t aft, comm;
  aft = plus_infinity<time_unit_t>();
  task_t selected = plus_infinity<task_t>();

  for (; curr != end; ++curr, ++ccurr) {
    tmp = afts[*curr];
    
    if (proc_assignmts[*curr] != pid) {
      tmp += *ccurr;
    }

    if (tmp > est) {
      est = tmp;
      
      comm = 0.0;
      if (proc_assignmts[*curr] != pid){
        comm = *ccurr;
      }
      selected = *curr;
      aft = afts[*curr];
    }
  }

  if(est >= min_eft){
    return false;
  }

  std::vector<std::pair<task_t, time_unit_t> >::const_iterator scurr;
  
  geq_itask = std::lower_bound(
      processor_runway.begin(), processor_runway.end(), std::pair<task_t, time_unit_t>(0, est),
      desc_second_cmp_t<task_t, time_unit_t>());  // tareas con inicio >=est
  avail = 0.0;

  if (geq_itask != processor_runway.end()) {

    time_unit_t curr_st, prev_ft = est;

    if(geq_itask != processor_runway.begin()){
      prev_ft = std::max(prev_ft, afts[(geq_itask - 1)->first]);
    }

    for (scurr = geq_itask; scurr != processor_runway.end(); ++scurr){ //[1]
      curr_st = scurr->second;
      if ((curr_st - prev_ft) >= duration_on_proc) {
        avail = prev_ft;
        break;
      }
      prev_ft = afts[scurr->first];

    }

    geq_itask = scurr;

  }
  
  //avail: finish time de primera tarea que quepa en primer espacio vacio
  //est: start_time minimo permitido

  if (geq_itask == processor_runway.end() && processor_runway.size() > 0){// for two cases:[1] no space between contiguous blocks;[2] no tid>=est
    avail = processor_runway.crbegin()->second + weight_at(processor_runway.crbegin()->first, pid);
  }
  ast = std::max(est, avail);
  eft = ast + duration_on_proc;
  return true;
}

bool hms_param_t::try_insertion_policy(
    task_t tid,
    proc_t pid,
    const max_entry_t& curr_entry,
    std::vector<std::pair<task_t, time_unit_t> >::const_iterator& geq_itask,
    const std::vector<std::pair<task_t, time_unit_t> >& processor_runway,
    const time_unit_t& min_eft,
    time_unit_t& ast,
    time_unit_t& eft) const {

  time_unit_t est, tmp, avail, duration_on_proc;
  duration_on_proc = weight_at(tid, pid);
  std::vector<task_t>::const_iterator curr, end;
  std::vector<time_unit_t>::const_iterator ccurr, cend;
  bool geq_itask_undef = true;
  est = default_zero_value<time_unit_t>();
  geq_itask = processor_runway.end();

  if(curr_entry.fm_pid != pid){
    est = curr_entry.first_max;
  }
  else if(processor_runway.size() > 0){
    std::vector<task_t>::const_iterator curr, end;
    std::vector<std::pair<task_t, time_unit_t> >::const_iterator iter, selected_iter;
    sorted_predecessors(tid, curr, end);
    time_unit_t tmp;
    selected_iter = processor_runway.end();
    geq_itask_undef = false;
    est = curr_entry.second_max;
    iter = std::lower_bound(
      processor_runway.begin(),
      processor_runway.end(),
      std::pair<task_t, time_unit_t>(0, est),
      desc_second_cmp_t<task_t, time_unit_t>());

    if(iter != processor_runway.begin()){
      iter = iter - 1;
      selected_iter = iter;
    }
    for(; iter != processor_runway.end(); ++iter){

      if (std::binary_search(
        curr,
        end,
        iter->first)){
        tmp = iter->second + weight_at(iter->first, pid);
        if(tmp > est){
          est = tmp;
          selected_iter = iter;
        }
        
      }
    }

    if(selected_iter != processor_runway.end()){
      geq_itask = selected_iter + 1;
    }

  }

  if(est >= min_eft){
    return false;
  }

  std::vector<std::pair<task_t, time_unit_t> >::const_iterator scurr;
  
  if(geq_itask_undef){
    geq_itask = std::lower_bound(
      processor_runway.begin(),
      processor_runway.end(),
      std::pair<task_t, time_unit_t>(0, est),
      desc_second_cmp_t<task_t, time_unit_t>());  // tareas con inicio >=est
  }

  avail = 0.0;

  if (geq_itask != processor_runway.end()) {

    time_unit_t curr_st, prev_ft = est;

    if(geq_itask != processor_runway.begin()){
      std::vector<std::pair<task_t, time_unit_t> >::const_iterator prev = geq_itask - 1;
      prev_ft = std::max(prev_ft, prev->second + weight_at(prev->first, pid));
    }

    for (scurr = geq_itask; scurr != processor_runway.end(); ++scurr){ //[1]
      curr_st = scurr->second;
      if ((curr_st - prev_ft) >= duration_on_proc) {
        avail = prev_ft;
        break;
      }
      prev_ft = curr_st + weight_at(scurr->first, pid);

    }

    geq_itask = scurr;

  }
  
  //avail: finish time de primera tarea que quepa en primer espacio vacio
  //est: start_time minimo permitido

  if (geq_itask == processor_runway.end() && processor_runway.size() > 0){// for two cases:[1] no space between contiguous blocks;[2] no tid>=est
    avail = processor_runway.crbegin()->second + weight_at(processor_runway.crbegin()->first, pid);
  }
  ast = std::max(est, avail);
  eft = ast + duration_on_proc;
  return true;
}


void hms_param_t::assign_task_to_processor(
    task_t tid,
    proc_t pid,
    std::vector<std::pair<task_t, time_unit_t> >::const_iterator pos_it,
    const time_unit_t& ast,
    schedule_t& sched) const {
  sched.sched[pid].insert(pos_it,
                           std::pair<task_t, time_unit_t>(tid, ast));
}

void hms_param_t::update_rest_runways_maxs(
    std::vector<max_entry_t>& rest_runways_maxs,
    task_t tid,
    proc_t pid,
    time_unit_t comm_less_ft) const {

  time_unit_t comm_ft, old_fm;
  proc_t old_fm_pid;
  std::vector<task_t>::const_iterator curr, end;
  std::vector<time_unit_t>::const_iterator ccurr, cend;
  sorted_successors(tid, curr, end);
  comm_times_of_sorted_successors(tid, ccurr, cend);
  
  for (; curr != end; ++curr, ++ccurr) {
    comm_ft = comm_less_ft + *ccurr;
    max_entry_t& ref = rest_runways_maxs[*curr];
    
    if(comm_ft > ref.first_max){

      old_fm = ref.first_max;
      old_fm_pid = ref.fm_pid;

      ref.first_max = comm_ft;
      ref.fm_pid = pid;

      if(ref.fm_pid == ref.sm_pid){
        ref.second_max = old_fm;
        ref.sm_pid = old_fm_pid;   
      }
      else if(old_fm > ref.second_max && old_fm_pid != ref.fm_pid){
        ref.second_max = old_fm;
        ref.sm_pid = old_fm_pid;
      }
    }
    else if(comm_ft > ref.second_max && pid != ref.fm_pid){
      ref.second_max = comm_ft;
      ref.sm_pid = pid;
    }
  }
}

void schedule_t::init(const hms_param_t& g) {
  sched.resize(g.get_num_procs());
  for (std::vector<std::pair<task_t, time_unit_t> >& vec : sched) {
    vec.reserve(g.get_num_tasks());
    vec.resize(0);
  }
}

/*requires makespan*/
float schedule_t::calculate_slr(const hms_param_t& g) const {
  crit_path_out_arg_t arg;
  g.to_hetero_critical_path(arg);
  time_unit_t denom;
  std::vector<time_unit_t>::const_iterator curr, end;
  denom = 0.0;

  for (const task_t& tid : arg.critical_path) {
    g.weights_of(tid, curr, end);
    denom += *(std::min_element(curr, end));
  }
  return makespan / denom;
}

float schedule_t::calculate_speedup(const hms_param_t& g) const {
  std::vector<time_unit_t> sums(g.get_num_procs(), 0.0);
  task_t tid = 0;
  std::vector<time_unit_t>::const_iterator curr, end;
  for (time_unit_t& sum : sums) {
    g.weights_of(tid, curr, end);
    sum = std::accumulate(curr, end, 0.0);
    ++tid;
  }
  return *(std::min_element(sums.begin(), sums.end())) / makespan;
}

bool schedule_t::operator==(const schedule_t& other) const {
  
  if(sched.size() != other.sched.size()){
    return false;
  }
  if(!almost_equal(makespan, other.makespan)){
    return false;
  }

  std::vector<std::vector<std::pair<task_t, time_unit_t> > >::const_iterator i_other_sched = other.sched.begin();

  for (const std::vector<std::pair<task_t, time_unit_t> >& processor_row : sched) {

    if(i_other_sched->size() != processor_row.size()){
      return false;
    }
    std::vector<std::pair<task_t, time_unit_t> >::const_iterator i_elem = i_other_sched->begin();
    for (const std::pair<task_t, time_unit_t>& slot : processor_row) {
      if(!almost_equal(slot.second, i_elem->second) || slot.first != i_elem->first){
        return false;
      }
      ++i_elem;
    }
    ++i_other_sched;
  }
  return true;
}

bool schedule_t::operator!=(const schedule_t& other) const {
  return !(this->operator==(other));
}

bool schedule_t::is_valid(const hms_param_t& g, std::string& invalid_msg) const {
  if (!sched.size()) {
    invalid_msg = "#empty schedule#";
    return false;
  }
  std::ostringstream str;
  std::vector<task_t>::const_iterator curr, cend;
  size_t count_tasks = 0;
  std::vector<time_unit_t> itimes(g.get_num_tasks());
  time_unit_t prev;
  task_t prev_task = plus_infinity<task_t>();
  proc_t proc = 0;
  for (const std::vector<std::pair<task_t, time_unit_t> >& processor_row : sched) {
    prev = -1.0f;
    for (const std::pair<task_t, time_unit_t>& slot : processor_row) {
      if (slot.second < prev){
        str << " #start time less than previous task's : @" << proc << "@";
        str << "(" << slot.first << "," << slot.second << ")#";
        str << "(" << prev_task << "," << prev << ")";
        invalid_msg = str.str();
        return false;
      }
      prev = slot.second;
      prev_task = slot.first;
      itimes[slot.first] = slot.second;
      ++count_tasks;
    }
    ++proc;
  }
  if (count_tasks != g.get_num_tasks()) {
    invalid_msg = "#not all tasks assigned#";
    return false;
  }
  task_t past;
  proc = 0;
  for (const std::vector<std::pair<task_t, time_unit_t> >& processor_row : sched) {
    for (const std::pair<task_t, time_unit_t>& slot : processor_row) {
      past = slot.first;
      g.sorted_predecessors(slot.first, curr, cend);
      for (; curr != cend; ++curr) {
        if (itimes[*curr] > (slot.second)) {
          str << " #start time less than predecessor task's : @" << proc << "@";
          str << "(" << slot.first << "," << slot.second << ")#";
          str << "(" << *curr << "," << itimes[*curr] << ")";
          invalid_msg = str.str();
          return false;
        }
      }
    }
    ++proc;
  }
  return true;
}

std::string schedule_t::to_string() const {
  std::ostringstream str;
  proc_t proc = 0;
  size_t i = 0, j = 0;
  for (const std::vector<std::pair<task_t, time_unit_t> >& vec : sched) {
    str << "\n============[proc:" << proc << "]=====================\n";
    for (const std::pair<task_t, time_unit_t>& p : vec) {
      str << "(" << p.first << " , " << p.second << ")\n";
    }
    ++proc;
  }
  return str.str();
}

std::ostream& operator<<(std::ostream& os, const schedule_t& s) {
  
  proc_t proc = 0;
  for (const std::vector<std::pair<task_t, time_unit_t> >& vec : s.sched) {
    os << "============[proc:" << proc << "]=====================" << std::endl;
    for (const std::pair<task_t, time_unit_t>& p : vec) {
      os << "(" << p.first << " , " << p.second << ")" << std::endl;
    }
    ++proc;
  }
  return os;
}