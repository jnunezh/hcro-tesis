/*
 * File:   main.cpp
 * Author: Alexander
 *
 * Created on 21 de mayo de 2015, 01:56 PM
 */

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <list>
#include <sstream>


#include "BHEFTAlgorithm.h"
#include "CPOPAlgorithm.h"
#include "HCROAlgorithm.h"
#include "Priorities.h"
#include "file_persist.h"

using namespace std;


int main(int argc, char** argv) {
  std::srand(unsigned(std::time(0)));

#ifdef CPU_MULTITHREADED
  std::cout << "==================[Multi-thread Mode]=======================" << std::endl;
#else
  std::cout << "==================[Single-thread Mode]=======================" << std::endl;
#endif

  /*TODO LIST: set priority vector;verify constraints of setnodes_info;(15 & 16
   * alfa y beta); minlocal correctness:null?,memory leaks? not initialized? pe
   * outdated:how? or better update pe everytime inner_molecule is
   * changed[better!!!]
   * ;*/
 
  // randomGenerate(size_t nprocs = 3, size_t ntasks = 10, float density = 0.5f,
  // float comm_lb = 1.0f, float comm_ub = 10.0f, float weights_lb = 1.0f, float
  // weights_ub = 10.0f);


  // ideas:{mean costs and values @ CPOP/HEFTB}
  // check if there's overflow;correcteness of priorities
  // check if graph generated is ok
  // prove using a custom-made graph from papers & check if priorities values
  // are correct
  size_t curr_procs = 70, curr_tasks = 200;
  std::string params_filepath, uid, schedule_filepath;
  params_filepath = "data/hms_params.json";
  schedule_filepath = "data/bheft_schedule.json";
  uid = "testie0001"; 
  hms_param_t params = hms_params_from_json_file(params_filepath, uid);
  bool already_saved = true;
  
  if(params.is_empty()){
    std::cout << "params is empty" << std::endl;
    already_saved = false;
    params = hms_param_t::random_generate(curr_procs, curr_tasks, 0.4f,
                                               1.5f, 9.5f, 1.0f, 10.0f);
  }
  else{
    std::cout << "params is not empty" << std::endl;
  }

  schedule_t s0, s1;
  try {
    bheft_algorithm_t alg0;
    alg0.set_param(&params);
    print_elapsed_ms(s0 = alg0.run());
    std::cout << "ended bhefty" << std::endl;
  } catch (const std::exception& e) {
    std::cout << "failed@bheft:" << e.what() << std::endl;
    return 0;
  }
  
  std::string inv_msg;

  std::cout << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << std::endl;

  std::cout << "b-heft[heuristico] => makespan:" << s0.makespan << " ";
  if(s0.is_valid(params, inv_msg)){
    std::cout << "b-heft schedule valid" << std::endl;
  }
  else{
    std::cout << "b-heft schedule invalid:" << inv_msg << std::endl;
    std::cout << "b-heft schedule:" << s0.to_string() << std::endl;
  }
  
  std::cout << "b-heft slr:" << s0.calculate_slr(params) << " ";
  std::cout << "b-heft speedup:" << s0.calculate_speedup(params) << std::endl;



  std::pair<bool, std::string> ores = persist_schedule_to_json_file(schedule_filepath, s0, uid);  

  std::cout << "schedule persistance result.first=>" << ores.first << " result.second=>" << ores.second << std::endl;

  s1 = schedule_from_json_file(schedule_filepath, uid);

  if(s1.is_empty()){
    std::cout << "s1 is empty" << std::endl;
  }
  else{
    std::cout << "s1 is not empty" << std::endl;
    if(s1 == s0){
      std::cout << "s0 & s1 are equal" << std::endl;
    }
    else{
      std::cout << "s0 & s1 are not equal" << std::endl; 
    }
  }

  if(!already_saved){
    std::pair<bool, std::string> res = persist_hms_params_to_json_file(params_filepath, params, uid);
    std::cout << "params persisted!!" << std::endl;
    std::cout << "res.first:" << res.first << " res.second:" << res.second << std::endl;   
  }
  std::cout << "fin" << std::endl;
  return 0;
}
