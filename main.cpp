/*
 * File:   main.cpp
 * Author: Alexander
 *
 * Created on 21 de mayo de 2015, 01:56 PM
 */

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <list>
#include <sstream>

#include "BHEFTAlgorithm.h"
#include "CPOPAlgorithm.h"
#include "HCROAlgorithm.h"
#include "Priorities.h"
#include "file_persist.h"
using namespace std;

int main(int argc, char** argv) {
  std::srand(unsigned(std::time(0)));

#ifdef CPU_MULTITHREADED
  std::cout << "==================[Multi-thread Mode]=======================" << std::endl;
#else
  std::cout << "==================[Single-thread Mode]=======================" << std::endl;
#endif

  /*TODO LIST: set priority vector;verify constraints of setnodes_info;(15 & 16
   * alfa y beta); minlocal correctness:null?,memory leaks? not initialized? pe
   * outdated:how? or better update pe everytime inner_molecule is
   * changed[better!!!]
   * ;*/
  b_rank_t bp;
  t_rank_t tp;  // a,t_rank_t tp;
  l_rank_t lp;
  std::vector<priority_cmp_t<float> > priorities(3);
  priorities[0].set_criterion(&bp);
  priorities[1].set_criterion(&tp);
  priorities[2].set_criterion(&lp);

  schedule_t s0, s1, s2;
  float e0, e1, e2;
  // randomGenerate(size_t nprocs = 3, size_t ntasks = 10, float density = 0.5f,
  // float comm_lb = 1.0f, float comm_ub = 10.0f, float weights_lb = 1.0f, float
  // weights_ub = 10.0f);


  // ideas:{mean costs and values @ CPOP/HEFTB}
  // check if there's overflow;correcteness of priorities
  // check if graph generated is ok
  // prove using a custom-made graph from papers & check if priorities values
  // are correct

  // std::cout<<param.to_string()<<std::endl;

  size_t curr_procs = 500, curr_tasks = 1000;  // 4, 35
  std::cout << "num_procs:" << curr_procs << ",num_tasks:" << curr_tasks << std::endl;
  /*
  ofstream myfile;
  myfile.open ("results.dat");



  size_t half;
  for(size_t curr_tasks = 10;curr_tasks < max_num_tasks;curr_tasks+=10 )
  {
      half = curr_tasks >> 1;
      for(size_t curr_procs = 3;curr_procs < max_num_processors && curr_procs <
  half;curr_procs+=2){
    */
  hms_param_t param = hms_param_t::random_generate(curr_procs, curr_tasks, 0.4f,
                                               1.5f, 9.5f, 1.0f, 10.0f);

  try {
    bheft_algorithm_t alg0;
    alg0.set_param(&param);
    assign_elapsed_ms(e0, s0 = alg0.run());
    std::cout << "ended bhefty" << std::endl;
  } catch (const std::exception& e) {
    std::cout << "failed@bheft:" << e.what() << std::endl;
    return 0;
  }

  try {
    cpop_algorithm_t alg2;
    alg2.set_param(&param);
    assign_elapsed_ms(e2, s2 = alg2.run());
    std::cout << "ended cpopy" << std::endl;
  } catch (const std::exception& e) {
    std::cout << "failed@cpop:" << e.what() << std::endl;
    return 0;
  }

  try {
    hcro_algorithm_t alg1(200,   // num_iterations
                       3,     // initial_num_groups : aka groups of sections
                       8,     // max_num_groups
                       30,    // section_amount aka elements per section
                       0.5f,  // percent elements per section initially created
                       7,     // num_candidates
                       priorities,  // priorities
                       2000.0f,     // buffer_init_val
                       0.0f,        // mu
                       1.0f,        // sigma
                       0.5f,        // collision_rate
                       200,         // alfa:char
                       100.0f,      // beta
                       10000.0f,    // ke_init
                       0.5f         // ke_loss_rate
    );
    // std::cout << "ended hcro A" << std::endl;
    alg1.set_param(&param);
    // std::cout << "ended hcro B" << std::endl;
    assign_elapsed_ms(e1, s1 = alg1.run());
    std::cout << "ended hcro Cy" << std::endl;
  } catch (const std::exception& e) {
    std::cout << "failed@hcro:" << e.what() << std::endl;
    return 0;
  }


  std::string inv_msg;

  std::cout << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << std::endl;

  std::cout << "b-heft[heuristico] => makespan:" << s0.makespan << " ";
  if(s0.is_valid(param, inv_msg)){
    std::cout << "b-heft schedule valid" << std::endl;
  }
  else{
    std::cout << "b-heft schedule invalid:" << inv_msg << std::endl;
    std::cout << "b-heft schedule:" << s0.to_string() << std::endl;
  }
  
  std::cout << "b-heft slr:" << s0.calculate_slr(param) << " ";
  std::cout << "b-heft speedup:" << s0.calculate_speedup(param) << std::endl;
  
  std::cout << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << std::endl;

  std::cout << "cpop[heuristico] => makespan:" << s2.makespan << " ";
  if(s2.is_valid(param, inv_msg)){
    std::cout << "cpop schedule valid" << std::endl;
  }
  else{
    std::cout << "cpop schedule invalid:" << inv_msg << std::endl;
    std::cout << "cpop schedule:" << s2.to_string() << std::endl;
  }  
  
  std::cout << "cpop slr:" << s2.calculate_slr(param) << " ";
  std::cout << "cpop speedup:" << s2.calculate_speedup(param) << std::endl;

  std::cout << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << std::endl;

  std::cout << "hcro[propuesta] => makespan:" << s1.makespan << " ";
  if(s1.is_valid(param, inv_msg)){
    std::cout << "hcro schedule valid" << std::endl;
  }
  else{
    std::cout << "hcro schedule invalid:" << inv_msg << std::endl;
    std::cout << "hcro schedule:" << s1.to_string() << std::endl;
  }  
  std::cout << "hcro slr:" << s1.calculate_slr(param) << " ";
  std::cout << "hcro speedup:" << s1.calculate_speedup(param) << std::endl;

  std::cout << "hcro takes " << e1/e0 << "x times than bheft" << std::endl;
  std::cout << "hcro takes " << e1/e2 << "x times than cpop" << std::endl;
  std::cout << "cpop takes " << e2/e0 << "x times than bheft" << std::endl;
  /*
  std::string uid = "main000";

  
  truncate_plot_json_file(
    "plot/plot.json",
    s0,
    s2,
    s1,
    param);

  if(create_dag_gexf_file_from("plot/my_gexf.gexf", param)){
    std::cout << "gexf plot file generated" << std::endl;
  }
  else{
    std::cout << "gexf plot file NOT generated" << std::endl;
  }
  
  auto res = persist_hms_params_to_json_file("data/hms_000.json", param, uid);
  std::cout << "res.first:" << res.first << " res.second:" << res.second << std::endl; 
  hms_param_t param_b = hms_params_from_json_file("data/hms_000.json", uid);
  std::cout << "param_b empty?:"<< param_b.is_empty() << std::endl;
  std::cout << "param == param_b?:" << (param == param_b) << std::endl;
  */
  std::cout << "fin" << std::endl;
  return 0;
}
