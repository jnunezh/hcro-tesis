/*
 * File:   main.cpp
 * Author: Alexander
 *
 * Created on 21 de mayo de 2015, 01:56 PM
 */

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <map>
#include <sstream>

#include "Utils.h"
#include "file_persist.h"

using namespace std;

int main(int argc, char** argv) {
  std::srand(unsigned(std::time(0)));

#ifdef CPU_MULTITHREADED
  std::cout << "==================[Multi-thread Mode]=======================" << std::endl;
#else
  std::cout << "==================[Single-thread Mode]=======================" << std::endl;
#endif
  float pctg, pctg2, bound;
  size_t procs;

  
  std::map<float, std::map<float, float> > stats, stats2;
  for(size_t tasks = 5 ; tasks < 10000; tasks *= 2){
    procs = tasks / 2;
    for(float density = 0.1f; density <= 1.0f; density += 0.2){

      std::cout << "tasks:" << tasks << ",procs:" << procs << ",density:" << density << std::endl;
      {
      
        hms_param_t params = hms_param_t::random_generate(
          procs,
          tasks,
          density,
          1.5f,
          9.5f,
          1.0f,
          10.0f);
        pctg = static_cast<float>(params.get_num_edges()) / static_cast<float>(params.get_num_tasks());
      }
      {
        hms_param_t params2 = hms_param_t::random_generate2(
          procs,
          tasks,
          density,
          1.5f,
          9.5f,
          1.0f,
          10.0f);
        pctg2 = static_cast<float>(params2.get_num_edges()) / static_cast<float>(params2.get_num_tasks());
      }

      if(pctg >= 1.0f && pctg < 2.0f){
        bound = 1.0f;
      }
      else if(pctg >= 2.0f && pctg < 5.0f){
        bound = 2.0f;
      }
      else if(pctg >= 5.0f && pctg < 10.0f){
        bound = 5.0f;
      }
      else if(pctg >= 10.0f && pctg < 20.0f){
        bound = 10.0f;
      }
      else if(pctg >= 20.0f && pctg < 30.0f){
        bound = 20.0f;
      }
      else if(pctg >= 30.0f && pctg < 50.0f){
        bound = 30.0f;
      }
      else if(pctg >= 50.0f){
        bound = 50.0f;
      }

      if(stats[density].find(bound) ==  stats[density].end()){
        stats[density] = {{1.0f, 0.0}, {2.0f, 0.0}, {5.0f, 0.0}, {10.0f, 0.0}, {20.0f, 0.0}, {30.0f, 0.0}, {50.0f, 0.0}};
      }
      
      stats[density][bound] += 1.0;
      
      if(pctg2 >= 1.0f && pctg2 < 2.0f){
        bound = 1.0f;
      }
      else if(pctg2 >= 2.0f && pctg2 < 5.0f){
        bound = 2.0f;
      }
      else if(pctg2 >= 5.0f && pctg2 < 10.0f){
        bound = 5.0f;
      }
      else if(pctg2 >= 10.0f && pctg2 < 20.0f){
        bound = 10.0f;
      }
      else if(pctg2 >= 20.0f && pctg2 < 30.0f){
        bound = 20.0f;
      }
      else if(pctg2 >= 30.0f && pctg2 < 50.0f){
        bound = 30.0f;
      }
      else if(pctg2 >= 50.0f){
        bound = 50.0f;
      }

      if(stats2[density].find(bound) ==  stats2[density].end()){
        stats2[density] = {{1.0f, 0.0}, {2.0f, 0.0}, {5.0f, 0.0}, {10.0f, 0.0}, {20.0f, 0.0}, {30.0f, 0.0}, {50.0f, 0.0}};
      }
      
      stats2[density][bound] += 1.0;

    }
  }

  float accum;
  for(auto& entry : stats){
    accum = 0.0f;
    for(auto& e : entry.second){
      accum += e.second;
    }
    for(auto& e : entry.second){
      if(accum != 0.0f){
        e.second = e.second / accum;
      }
      else{
        e.second = 0.0f;
      }
    }
  }
  
  for(auto& entry : stats2){
    accum = 0.0f;
    for(auto& e : entry.second){
      accum += e.second;
    }
    for(auto& e : entry.second){
      if(accum != 0.0f){
        e.second = e.second / accum;
      }
      else{
        e.second = 0.0f;
      }
    }
  }

  for(auto& entry : stats){
    
    std::cout << " " << entry.first << " => " << std::endl;
    for(auto& e : entry.second){
      std::cout << "\t\t\t" << e.first << " : " << e.second << std::endl;
    }
  }

  std::cout << "------------------------------" << std::endl;

  for(auto& entry : stats2){
    
    std::cout << " " << entry.first << " => " << std::endl;
    for(auto& e : entry.second){
      std::cout << "\t\t\t" << e.first << " : " << e.second << std::endl;
    }
  }
  
  std::cout << "fin" << std::endl;
  return 0;
}
