/*
 * File:   main.cpp
 * Author: Alexander
 *
 * Created on 21 de mayo de 2015, 01:56 PM
 */

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <list>
#include <sstream>

#include "Utils.h"
#include "BHEFTAlgorithm.h"
#include "CPOPAlgorithm.h"
#include "HCROAlgorithm.h"
#include "OldHCROAlgorithm.h" 
#include "Priorities.h"
#include "file_persist.h"

using namespace std;

int main(int argc, char** argv) {
  std::srand(unsigned(std::time(0)));

#ifdef CPU_MULTITHREADED
  std::cout << "==================[Multi-thread Mode]=======================" << std::endl;
#else
  std::cout << "==================[Single-thread Mode]=======================" << std::endl;
#endif

  b_rank_t bp;
  t_rank_t tp;  // a,t_rank_t tp;
  l_rank_t lp;
  std::vector<priority_cmp_t<float> > priorities(3);
  priorities[0].set_criterion(&bp);
  priorities[1].set_criterion(&tp);
  priorities[2].set_criterion(&lp);
  
  size_t procs = 20, tasks = 100;
  float density = 0.2f;

  schedule_t s0, s1;
  
  hms_param_t params = hms_param_t::random_generate(
    procs,
    tasks,
    density,
    1.5f,
    9.5f,
    1.0f,
    10.0f);
  
  //std::cout << params << "---------------------------" << std::endl;

  try {
    cpop_algorithm_t alg0;
    alg0.set_param(&params);
    print_elapsed_ms(s0 = alg0.run());
  } catch (const std::exception& e) {
    std::cout << "cpop@bheft:" << e.what() << std::endl;
    return 0;
  }

  try {
    hcro_algorithm_t alg0(200,   // num_iterations
                       3,     // initial_num_groups : aka groups of sections
                       8,     // max_num_groups
                       30,    // section_amount aka elements per section
                       0.5f,  // percent elements per section initially created
                       7,     // num_candidates
                       priorities,  // priorities
                       2000.0f,     // buffer_init_val
                       0.0f,        // mu
                       1.0f,        // sigma
                       0.5f,        // collision_rate
                       200,         // alfa:char
                       100.0f,      // beta
                       10000.0f,    // ke_init
                       0.5f         // ke_loss_rate
    );
    alg0.set_param(&params);
    print_elapsed_ms(s1 = alg0.run());
  } catch (const std::exception& e) {
    std::cout << "failed@hcro:" << e.what() << std::endl;
    return 0;
  }
  
  std::string msg;
  if(s0.is_valid(params, msg)){
    std::cout << "s0 is valid" << std::endl;
  }
  else{
    std::cout << "s0 is INVALID:" << msg << std::endl;
  }

  if(s1.is_valid(params, msg)){
    std::cout << "s1 is valid" << std::endl;
  }
  else{
    std::cout << "s1 is INVALID:" << msg << std::endl;
  }

  
  std::cout << "@@@@@@@@@@@@@@@@[cpop sched]@@@@@@@@@@@@@@@@@@@@@@@" << std::endl;
  std::cout << "makespan:" << s0.makespan << std::endl;
  //std::cout << s0;
  std::cout << "################[hcro  sched]#######################" << std::endl;
  std::cout << "makespan:" << s1.makespan << std::endl;
  //std::cout << s1;
  
  std::cout << "fin" << std::endl;
  return 0;
}
