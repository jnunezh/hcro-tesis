/*
 * File:   main.cpp
 * Author: Alexander
 *
 * Created on 21 de mayo de 2015, 01:56 PM
 */

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <list>
#include <sstream>

#include "BHEFTAlgorithm.h"
#include "CPOPAlgorithm.h"
#include "HCROAlgorithm.h"
#include "Priorities.h"
#include "file_persist.h"
#include "Utils.h"
using namespace std;

int main(int argc, char** argv) {
  std::srand(unsigned(std::time(0)));

#ifdef CPU_MULTITHREADED
  std::cout << "==================[Multi-thread Mode]=======================" << std::endl;
#else
  std::cout << "==================[Single-thread Mode]=======================" << std::endl;
#endif

  size_t curr_procs = 7, curr_tasks = 50;  // 4, 35
  std::cout << "num_procs:" << curr_procs << ",num_tasks:" << curr_tasks << std::endl;
  hms_param_t param = hms_param_t::random_generate(curr_procs, curr_tasks, 0.4f,
                                               1.5f, 9.5f, 1.0f, 10.0f);
  schedule_t s0;
  try {
    bheft_algorithm_t alg0;
    alg0.set_param(&param);
    print_elapsed_ms(s0 = alg0.run());
    std::cout << "ended bhefty" << std::endl;
  } catch (const std::exception& e) {
    std::cout << "failed@bheft:" << e.what() << std::endl;
    return 0;
  }
  
  std::string inv_msg;

  std::cout << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << std::endl;

  std::cout << "b-heft[heuristico] => makespan:" << s0.makespan << " ";
  if(s0.is_valid(param, inv_msg)){
    std::cout << "b-heft schedule valid" << std::endl;
  }
  else{
    std::cout << "b-heft schedule invalid:" << inv_msg << std::endl;
    std::cout << "b-heft schedule:" << s0.to_string() << std::endl;
  }
  
  std::cout << "b-heft slr:" << s0.calculate_slr(param) << " ";
  std::cout << "b-heft speedup:" << s0.calculate_speedup(param) << std::endl;

  return 0;
}
