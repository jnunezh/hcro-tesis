
#g++ -Wall -g -m64 -std=c++11 -I/./ThreadPool.h example.cpp -o example -lpthread
#-Wall
#-pedantic -ansi -Wall -Wpointer-arith -Wcast-align -Wcast-qual -Wfloat-equal -Wredundant-decls -Wno-long-long

#make MY_MAIN=main1 MY_DEF=-DCPU_MULTITHREADED
$(info $$MY_MAIN [${MY_MAIN}])
$(info $$MY_DEF [${MY_DEF}])
$(info $$JSON_CPP [${JSON_CPP}])

ifeq ($(MY_MAIN),)
 ONLY_MAIN_SRC=main.cpp
else
 ONLY_MAIN_SRC=$(addsuffix .cpp,${MY_MAIN})
endif

CXX= g++
CPPFLAGS= -g -m64 -std=c++11 ${MY_DEF}
INCLUDE= -I/./
SOURCES=$(wildcard *.cpp)

ifeq ($(MY_DEF),-DCPU_MULTITHREADED)
 LIBS=  -pthread
endif

ifeq ($(MY_DEF),-DGPU_MULTITHREADED)
 LIBS=  -lOpenCL
 #SOURCES=
 INCLUDE+= -I/usr/include/CL/
endif

ifdef JSON_CPP
 SOURCES+= ./jsoncpp/jsoncpp.cpp
 INCLUDE+= -I/./jsoncpp/
endif

$(info $$SOURCES [${SOURCES}])

#filter-out => not contain
FILTERED_SOURCES=$(filter-out main%.cpp,$(SOURCES))
FILTERED_SOURCES+=$(ONLY_MAIN_SRC)
OBJS=$(subst .cpp,.o,$(FILTERED_SOURCES))

all: pre $(OBJS) final post 
pre:
	rm -rf *.o
	rm -rf */*.o
%.o: %.cpp
	$(CXX) $(CPPFLAGS) ${MY_DEF} ${INCLUDE} ${LIBS} -c $< -o $@
final: $(OBJS)
	$(CXX) $(CPPFLAGS) ${MY_DEF} ${INCLUDE} $^  ${LIBS} -o $@
post:
	chmod +x final