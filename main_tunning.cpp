/*
 * File:   main.cpp
 * Author: Alexander
 *
 * Created on 21 de mayo de 2015, 01:56 PM
 */

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <list>
#include <sstream>

#include "jsoncpp/json/json.h"
#include "HCROAlgorithm.h"
#include "Priorities.h"
#include "file_persist.h"
using namespace std;

int main(int argc, char** argv) {
  std::srand(unsigned(std::time(0)));

#ifdef CPU_MULTITHREADED
  std::cout << "==================[Multi-thread Mode]=======================" << std::endl;
#else
  std::cout << "==================[Single-thread Mode]=======================" << std::endl;
#endif

  /*TODO LIST: set priority vector;verify constraints of setnodes_info;(15 & 16
   * alfa y beta); minlocal correctness:null?,memory leaks? not initialized? pe
   * outdated:how? or better update pe everytime inner_molecule is
   * changed[better!!!]
   * ;*/
  b_rank_t bp;
  t_rank_t tp;  // a,t_rank_t tp;
  l_rank_t lp;
  std::vector<priority_cmp_t<float> > priorities(3);
  priorities[0].set_criterion(&bp);
  priorities[1].set_criterion(&tp);
  priorities[2].set_criterion(&lp);

  schedule_t s1;
  float e1;
  size_t curr_procs = 20, curr_tasks = 50;  // 4, 35

  Json::Value obj;
  std::string filepath = "data/tunning_results.json";

  obj["num_procs"] = curr_procs;
  obj["num_tasks"] = curr_tasks;

  std::cout << "num_procs:" << curr_procs << ",num_tasks:" << curr_tasks << std::endl;
  std::cout << "====================================" << std::endl;
 
  hms_param_t param = hms_param_t::random_generate(curr_procs, curr_tasks, 0.4f,
                                                           1.5f, 9.5f, 1.0f, 10.0f);
  
  Json::Value& ref_entries = obj["entries"] = Json::Value(Json::arrayValue);

  for(float biv = 50.0f; biv < 60000.0f; biv *= 10.0f){

    for(float cr = 0.1f; cr < 1.0f; cr += 0.3f){

      for(short alfa = 30; alfa < 500; alfa *= 2){

        for(float beta = 30.0f; beta < 500.0f; beta *= 2.0f){

          for(float ki = 50.0f; ki < 60000.0f; ki *= 10.0f){

            for(float klr = 0.1f; klr < 1.0f; klr += 0.3f){



              std::cout << "buffer_init_val:" << biv;
              std::cout << " collision_rate:" << cr;
              std::cout << " alfa:" << alfa;
              std::cout << " beta:" << beta;
              std::cout << " ke_init:" << ki;
              std::cout << " ke_loss_rate:" << klr;
              std::cout << std::endl;

              try {
                hcro_algorithm_t alg1(200,   // num_iterations
                                   3,     // initial_num_groups : aka groups of sections
                                   8,     // max_num_groups
                                   30,    // section_amount aka elements per section
                                   0.5f,  // percent elements per section initially created
                                   7,     // num_candidates
                                   priorities,  // priorities
                                   biv,     // buffer_init_val
                                   0.0f,        // mu
                                   1.0f,        // sigma
                                   cr,        // collision_rate
                                   alfa,         // alfa:char
                                   beta,      // beta
                                   ki,    // ke_init
                                   klr         // ke_loss_rate
                );
                // std::cout << "ended hcro A" << std::endl;
                alg1.set_param(&param);

                assign_elapsed_ms(e1, s1 = alg1.run());
                
                Json::Value entry;
                
                entry["duration_ms"] = e1;
                entry["buffer_init_val"] = biv;
                entry["collision_rate"] = cr;
                entry["alfa"] = alfa;
                entry["beta"] = beta;
                entry["ke_init"] = ki;
                entry["ke_loss_rate"] = klr;
                entry["makespan"] = s1.makespan;
                
                ref_entries.append(entry);

                /*
                std::cout << " duration_ms:" << e1;
                std::cout << " makespan:" << s1.makespan;
                std::cout << std::endl;
                */

              } catch (const std::exception& e) {
                std::cout << "failed@hcro:" << e.what() << std::endl;
                return 0;
              }
            }
          }
        }
      }

    }

  }

  Json::StreamWriterBuilder builder;
  std::unique_ptr<Json::StreamWriter> writer(builder.newStreamWriter());
  std::ofstream out(filepath);
  writer->write(obj, &out);

  std::cout << "fin" << std::endl;
  return 0;
}
