/*
 * File:   HCROAlgorithm.cpp
 * Author: Alexander
 *
 * Created on 31 de mayo de 2015, 08:40 AM
 */

#include "HCROAlgorithm.h"
#include <algorithm>

hcro_algorithm_t* hcro_molecule_t::ptr_algorithm = 0;

hcro_algorithm_t::hcro_algorithm_t(
      size_t iterations,
      size_t inum_groups,
      size_t max_n_groups,
      size_t sect_amount,
      float pic,
      size_t num_candidates,
      std::vector<priority_cmp_t<float> >& p_ordering,
      float buffer_value,
      float mu,
      float sigma,
      float cl_rate,
      short param_alpha,
      float param_beta,
      float ke_init,
      float ke_lrate)
    : num_iterations(iterations),
      _thread_info(),
      num_groups(inum_groups),
      max_num_groups(max_n_groups),
      section_amount(sect_amount),
      percentage_initially_created(pic),
      num_available(num_threads,
                    ((1.0f - percentage_initially_created) * section_amount) *
                        num_groups),
      candidates_sz(num_candidates),
      nd_mu(mu),
      nd_sigma(sigma),
      ordering_criteria(p_ordering),
      collision_rate(cl_rate),
      alpha(param_alpha),
      beta(param_beta),
      ke_init_value(ke_init),
      ke_loss_rate(ke_lrate),
      buffer(buffer_value){

        hcro_molecule_t::set_curr_algorithm(this);

}

hcro_algorithm_t::hcro_algorithm_t()
    : num_iterations(0),
      _thread_info(),
      num_groups(0),
      max_num_groups(0),
      section_amount(0),
      percentage_initially_created(0.0),
      candidates_sz(0),
      nd_mu(0.0),
      nd_sigma(0.0),
      collision_rate(0.0),
      alpha(0),
      beta(0.0),
      ke_init_value(0.0),
      ke_loss_rate(0.0),
      buffer(0.0){
        
        hcro_molecule_t::set_curr_algorithm(this);
}

hcro_algorithm_t& hcro_algorithm_t::operator=(const hcro_algorithm_t& other){
  num_iterations = other.num_iterations;
  num_threads = other.num_threads;
#ifdef CPU_MULTITHREADED
  pool.init(num_threads);
#endif
  num_groups = other.num_groups;
  max_num_groups = other.max_num_groups;
  section_amount = other.section_amount;
  percentage_initially_created = other.percentage_initially_created;
  num_available = other.num_available;
  candidates_sz = other.candidates_sz;
  nd_mu = other.nd_mu;
  nd_sigma = other.nd_sigma;
  ordering_criteria = other.ordering_criteria;
  collision_rate = other.collision_rate;
  alpha = other.alpha;
  beta = other.beta;
  ke_init_value = other.ke_init_value;
  ke_loss_rate = other.ke_loss_rate;
  buffer = other.buffer;
  return *this;

}

hcro_algorithm_t::hcro_algorithm_t(const hcro_algorithm_t& other){
  this->operator=(other);
}



hcro_algorithm_t::~hcro_algorithm_t() {
  if (hcro_molecule_t::ptr_algorithm == this){
    hcro_molecule_t::set_curr_algorithm(0);
  }
}

time_unit_t hcro_molecule_t::calculate_pe(schedule_t* sched, bool set_property) {
  time_unit_t makespan = default_zero_value<time_unit_t>();
  const hms_param_t& ref_param = ptr_algorithm->get_param();
#ifndef CPU_MULTITHREADED
  static std::vector<max_entry_t> rest_runways_maxs;
  static schedule_t tmp;
#else
  thread_local std::vector<max_entry_t> rest_runways_maxs;
  thread_local schedule_t tmp;
#endif
  bool cont;
  tmp.init(ref_param);
  size_t n_tasks = ref_param.get_num_tasks();
  std::vector<std::pair<task_t, time_unit_t> >::const_iterator trial_it, min_trial_it;
  proc_t pid, num_procs = ref_param.get_num_procs(), min_proc;
  time_unit_t ast, eft, min_eft, min_ast;
  rest_runways_maxs.clear();
  rest_runways_maxs.resize(n_tasks);
  if (!sched) {
    sched = &tmp;
  }
  sched->sched.resize(num_procs);

  for(const task_t& tid : inner_molecule){
    
    min_eft = plus_infinity<time_unit_t>();

    for (pid = 0; pid < num_procs; ++pid) {
      eft = plus_infinity<time_unit_t>();
      cont = ref_param.try_insertion_policy(
        tid,
        pid,
        rest_runways_maxs[tid],
        trial_it,
        sched->sched[pid],
        min_eft,
        ast,
        eft);

      if(!cont){
        continue;
      }

      if (eft < min_eft) {
        min_eft = eft;
        min_ast = ast;
        min_proc = pid;
        min_trial_it = trial_it;
      }
    }
    
    ref_param.update_rest_runways_maxs(
      rest_runways_maxs,
      tid,
      min_proc,
      min_eft);

    ref_param.assign_task_to_processor(
      tid,
      min_proc,
      min_trial_it,
      min_ast,
      *sched);

    if (min_eft > makespan){
      makespan = min_eft;
    }
  }
  
  sched->makespan = makespan;

  if (set_property){
    pe = makespan;
  }

  return makespan;
}

time_unit_t hcro_molecule_t::get_pe() const {
  return pe; 
}

void hcro_molecule_t::set_pe(time_unit_t pe_value){
  pe = pe_value;
}

time_unit_t hcro_molecule_t::get_ml_pe() const {
  assert(this->min_local);
  return this->min_local->pe;
}

void hcro_molecule_t::set_ml_pe(time_unit_t pe_value){
  assert(this->min_local);
  this->min_local->pe = pe_value;
}

float hcro_molecule_t::get_ke() const {
  return ke;
}

void hcro_molecule_t::set_ke(float ke_value){
  ke = ke_value;
}

short hcro_molecule_t::get_num_hit() const {
  return num_hit;
}

void hcro_molecule_t::set_num_hit(short value){
  num_hit = value;
}

void hcro_molecule_t::set_print_flags(iprint_flags_t flags){
  this->p_flags = flags;
}

hcro_molecule_t::iprint_flags_t hcro_molecule_t::get_print_flags() const{
  return this->p_flags;
}

short hcro_molecule_t::get_ml_num_hit() const {
  assert(this->min_local);
  return this->min_local->num_hit;
}

void hcro_molecule_t::set_ml_num_hit(short value){
  assert(this->min_local);
  this->min_local->num_hit = value;
}

void hcro_algorithm_t::enlarge_population() {
  size_t old_sz = population.size();
  population.resize(old_sz + (num_threads * section_amount));
  indexes.resize(population.size(), NON_EXISTENT);
  ++num_groups;
  for (auto& e : num_available) {
    e += section_amount;
  }
}

size_t hcro_algorithm_t::get_num_iterations() const{
  return this->num_iterations;
}

size_t hcro_algorithm_t::get_section_amount() const{
  return this->section_amount;
}

size_t hcro_algorithm_t::get_max_num_groups() const{
  return this->max_num_groups;
}

size_t hcro_algorithm_t::get_num_groups() const{
  return this->num_groups;
}

size_t hcro_algorithm_t::get_num_threads() const{
  return this->num_threads;
}

float hcro_algorithm_t::get_mu() const{
  return this->nd_mu;
}

float hcro_algorithm_t::get_sigma() const{
  return this->nd_sigma;
}

float hcro_algorithm_t::get_collision_rate() const{
  return this->collision_rate;
}

float hcro_algorithm_t::get_buffer_value() const{
  return this->buffer;
}

void hcro_algorithm_t::set_buffer_value(float value){
  this->buffer = value;
}

float hcro_algorithm_t::get_ke_loss_rate() const{
  return this->ke_loss_rate;
}

float hcro_algorithm_t::get_ke_init_value() const{
  return this->ke_init_value;
}

float hcro_algorithm_t::get_beta() const{
  return this->beta;
}

float hcro_algorithm_t::get_percentage_init_created() const{
  return this->percentage_initially_created;
}

short hcro_algorithm_t::get_alpha() const{
  return this->alpha;
}

hcro_molecule_t& hcro_algorithm_t::select_individual(
  int& individual_index,
  bool existent,
  size_t thread_idx,
  ordered_excluded_t* ptr) {

  assert(population.size() > 0);
  std::vector<size_t>::const_iterator pos_it;
  bool element_complies, finished = false, exhaustion_mode = false;
  int index, lb, ub, loop_count, g_marker, group_idx;

  do{

    g_marker = group_idx = individual_index = -1;

    #ifdef CPU_MULTITHREADED
    mp_mutex.lock();
    #endif

    if (this->num_available[thread_idx] < 1) {
      if (!existent) {
        enlarge_population();
        group_idx = g_marker = this->num_groups - 1;
      }
    }

    if (g_marker < 0) {
      g_marker = rand_inclusive(0, this->num_groups - 1);
      group_idx = g_marker + 1;
      group_idx = (group_idx < this->num_groups ? group_idx : 0);
    }

    #ifdef CPU_MULTITHREADED
    mp_mutex.unlock();
    #endif

    for (; !finished; ++group_idx) {
  #ifdef CPU_MULTITHREADED
      mp_mutex.lock();
  #endif
      if (group_idx >= this->num_groups){
        group_idx = 0;
      }
  #ifdef CPU_MULTITHREADED
      mp_mutex.unlock();
  #endif
      lb = (group_idx * this->num_threads * this->section_amount) + (thread_idx * this->section_amount);
      ub = lb + this->section_amount - 1;
      index = rand_inclusive(lb, ub) + 1;
      index = (index > ub ? lb : index);
      for (loop_count = 0; loop_count < this->section_amount && !finished; ++index, ++loop_count){

        if (index > ub) {
          index = lb;
        }

        if(ptr){
          ptr->solution_found = false;
        }

        if(!exhaustion_mode){
          element_complies = (indexes[index] == existent);
        }
        else{
          element_complies = (indexes[index] != existent); 
        }
                
        if (element_complies && ptr) {
          pos_it = std::lower_bound(ptr->candidates->begin(), ptr->candidates->end(), index);
          if(pos_it != ptr->candidates->end() && *pos_it == index){
            continue;
          }
          else{
            ptr->geq_it = pos_it;
            ptr->solution_found = true;
          }
        }

        if(element_complies){
          individual_index = index;
          finished = true;
          if(exhaustion_mode){
            indexes[index] = existent;

            if(existent){
              assert(this->num_available[thread_idx] > 0);
              --(this->num_available[thread_idx]);
            }
            else{
              ++(this->num_available[thread_idx]);
            }
          }

        }
      }

      if (group_idx == g_marker) {
        break;
      }
    }
    
    if(!finished){
      exhaustion_mode = true;
    }

  }while(!finished);

  assert(individual_index >= 0);
#ifdef CPU_MULTITHREADED
  mp_mutex.lock();
#endif
  hcro_molecule_t& ret_ref = population[individual_index];
#ifdef CPU_MULTITHREADED
  mp_mutex.unlock();
#endif
  return ret_ref;
}

hcro_molecule_t& hcro_algorithm_t::create_individual(int& individual_index,
                                              size_t thread_idx) {
  hcro_molecule_t& ref =
      select_individual(individual_index, NON_EXISTENT, thread_idx);

  assert(individual_index >= 0);

  indexes[individual_index] = EXISTENT;

  ref.init(true, false);

#ifdef CPU_MULTITHREADED
  mp_mutex.lock();
#endif

  assert(num_available[thread_idx] > 0);
  --(num_available[thread_idx]);

#ifdef CPU_MULTITHREADED
  mp_mutex.unlock();
#endif
  return ref;
}

void hcro_algorithm_t::remove_individual(int individual_index, size_t thread_idx){
  indexes[individual_index] = NON_EXISTENT;
#ifdef CPU_MULTITHREADED
  mp_mutex.lock();
#endif
  ++(num_available[thread_idx]);
#ifdef CPU_MULTITHREADED
  mp_mutex.unlock();
#endif
}

void hcro_algorithm_t::do_single_reaction(
  reaction_min_t& selected_ms,
  size_t thread_idx) {
  
  int index;
  float b = rand_inclusive(0.0f, 1.0f);
  hcro_molecule_t& selected = select_individual(index, EXISTENT, thread_idx);

  if (b > collision_rate) {
    if (selected.num_hit - selected.min_local->num_hit > alpha) {
      do_decomposition(selected, index, selected_ms, thread_idx);  // here
    } else {
      do_on_wall_ineffect_collision(selected, index, selected_ms, thread_idx);
    }
  } else {
    if (!do_synthesis(selected, index, selected_ms, thread_idx)) {
      do_inter_molec_ineffect_collision(selected, index, selected_ms, thread_idx);
    }
  }
}

reaction_min_t hcro_algorithm_t::do_reactions() {
  reaction_min_t selected_ms, obtained;
  selected_ms.makespan = plus_infinity<time_unit_t>();

#ifndef CPU_MULTITHREADED

  do_single_reaction(selected_ms, 0);
  
#else
  std::vector<std::future<reaction_min_t> > results;
  for (int i = 0; i < num_threads; ++i) {
    results.emplace_back(pool.enqueue([this, i] {
      reaction_min_t result;
      this->do_single_reaction(result, i);
      return result;
    }));
  }

  for (auto&& result : results) {
    obtained = result.get();
    if (obtained.makespan < selected_ms.makespan){
      selected_ms = obtained;
    }
  }

#endif
  return selected_ms;
}

schedule_t hcro_algorithm_t::run() {
  schedule_t min_schedule;
  const hms_param_t& ref_param = this->get_param();
  int ntasks;
  size_t curr_count = 0;
  reaction_min_t selected_ms, min_ms;
  min_ms.makespan = plus_infinity<time_unit_t>();
  set_init_population();
  ntasks = ref_param.get_num_tasks();

  while (curr_count++ <= num_iterations) {
    selected_ms = do_reactions();

    if (selected_ms.makespan < min_ms.makespan) {
      min_ms = selected_ms;
    }
  }
  if(min_ms.ml_min){
    hcro_molecule_t* inner_min = population[min_ms.molecule_index].min_local;
    inner_min->calculate_pe(&min_schedule, false);
  }
  else{
    population[min_ms.molecule_index].calculate_pe(&min_schedule, false); 
  }
  return min_schedule;
}

void hcro_molecule_t::init_inner_sequence(const hcro_molecule_t& other_molecule, int ifirst, int num_rotations){
  int i, j, num_tasks, ilast_task;
  assert(this->ptr_algorithm);
  const hms_param_t& ref_param = ptr_algorithm->get_param();
  this->assign_non_ml_fields(other_molecule);
  i = ifirst;
  ilast_task = ref_param.get_num_tasks() - 1;
  if(num_rotations < 0){
    float n_rots = 0.2f * static_cast<float>(ref_param.get_num_tasks());
    num_rotations = static_cast<int>(n_rots);
  }
  for(int curr_rotation = 0; curr_rotation < num_rotations; ++curr_rotation){
    j = ref_param.first_succesor_index_of(i, this->inner_molecule);
    this->left_rotate_by_one(i, j - 1);
    i = rand_inclusive(0, ilast_task - 1);
  }
}

const hcro_molecule_t& hcro_molecule_t::get_min_local() const{
  assert(this->min_local);
  return *(this->min_local);
}

hcro_molecule_t& hcro_molecule_t::get_min_local(){
  assert(this->min_local);
  return *(this->min_local);
}

void hcro_algorithm_t::set_init_population() {
  const hms_param_t& ref_param = this->get_param();
  std::vector<hcro_molecule_t> model_molecules;
  int i = 0, u, ilast_task, num_tasks, j, accum_pop_sz = 0;
  size_t max_pop_sz, ini_pop_sz, ini_slots_per_thread, max_slots_per_thread;

  ini_slots_per_thread = num_groups * section_amount;
  assert(ini_slots_per_thread >= 2 * (candidates_sz + 1));
  max_slots_per_thread = max_num_groups * section_amount;
  ini_pop_sz = ini_slots_per_thread * num_threads;
  max_pop_sz = max_slots_per_thread * num_threads;

  population.reserve(max_pop_sz);
  indexes.reserve(max_pop_sz);

  population.resize(ini_pop_sz);
  indexes.resize(ini_pop_sz, NON_EXISTENT);

  int toggled_count, iindex, thread_id, max_toggled;
  max_toggled = section_amount * percentage_initially_created;
  for (int group_index = 0; group_index < num_groups; ++group_index) {
    for (thread_id = 0; thread_id < num_threads; ++thread_id) {
      iindex = (group_index * num_threads * section_amount) + (thread_id * section_amount);
      for (toggled_count = 0; toggled_count < max_toggled; ++toggled_count, ++iindex) {
        indexes[iindex] = EXISTENT;
      }
    }
  }

  model_molecules = hcro_molecule_t::generate_model_molecules(this->ordering_criteria);
  
  num_tasks = ref_param.get_num_tasks();
  ilast_task = num_tasks - 1;
  for (i = 0; i < ilast_task && accum_pop_sz < ini_pop_sz; ++i) {
    
    for (const hcro_molecule_t& model_mol : model_molecules) {
      
      hcro_molecule_t& ref = population[accum_pop_sz];
      ref.init_inner_sequence(model_mol, i);
      ref.calculate_pe();
      ref.set_inner_min_default();
      assert(ref.inner_molecule.size() == num_tasks);
      ++accum_pop_sz;

      if (accum_pop_sz == ini_pop_sz){
        break;
      }
    }
  }

  assert(num_tasks >= 2);
  
  while (accum_pop_sz < ini_pop_sz) {
    u = rand_inclusive(0, accum_pop_sz - 1);
    i = rand_inclusive(0, ilast_task - 1);
    hcro_molecule_t& ref = population[accum_pop_sz];
    ref.init_inner_sequence(population[u], i);
    ref.calculate_pe();
    ref.set_inner_min_default();
    assert(ref.inner_molecule.size() == num_tasks);
    ++accum_pop_sz;
  }

}

void hcro_algorithm_t::do_decomposition(
  hcro_molecule_t& molecule,
  int index,
  reaction_min_t& result,
  size_t thread_idx){

  time_unit_t fpe, spe, ope, ml_ope;
  float oke, delta1, delta2, delta3, edec;
  int fi, si;
#ifndef CPU_MULTITHREADED
  static hcro_molecule_t first;
  static hcro_molecule_t second;
#else
  thread_local hcro_molecule_t first;
  thread_local hcro_molecule_t second;
#endif
  first.init(false, false);
  second.init(false, false);
  molecule.generate_decomp_candidates(first, second);

  fpe = first.calculate_pe();
  spe = second.calculate_pe();
  ope = molecule.get_pe();
  oke = molecule.get_ke();

  if (ope + oke >= fpe + spe) {
    edec = ope + oke - (fpe + spe);
    delta3 = rand_inclusive(0.0f, 1.0f);
    first.set_ke(edec * delta3);
    second.set_ke(edec * (1.0f - delta3));

    hcro_molecule_t& ofirst = create_individual(fi, thread_idx);
    hcro_molecule_t& osecond = create_individual(si, thread_idx);
    ofirst.assign_non_ml_fields(first);
    osecond.assign_non_ml_fields(second);

    ofirst.min_local->assign_non_ml_fields(ofirst); //(1)
    osecond.min_local->assign_non_ml_fields(osecond); //(2)
    // ofirst.min_local->set_pe(ofirst.get_pe()) por (1)
    // osecond.min_local->set_pe(osecond.get_pe()) por (2)
    remove_individual(index, thread_idx);
    result.ml_min = false;
    if (almost_equal(std::min(fpe, spe), fpe)) {
      result.molecule_index = fi;
      result.makespan = fpe;
    } else {
      result.molecule_index = si;
      result.makespan = spe;
    }
  }
  else{

    delta1 = rand_inclusive(0.0f, 1.0f);
    delta2 = rand_inclusive(0.0f, 1.0f);
  #ifdef CPU_MULTITHREADED
    buffer_mutex.lock();
  #endif  
    edec = ope + oke + (delta1 * delta2 * buffer) - (fpe + spe);
  #ifdef CPU_MULTITHREADED
    buffer_mutex.unlock();
  #endif

    if(edec >= 0.0f){
  #ifdef CPU_MULTITHREADED
      buffer_mutex.lock();
  #endif
      buffer = buffer * (1.0f - delta1 * delta2);
  #ifdef CPU_MULTITHREADED
      buffer_mutex.unlock();
  #endif
      delta3 = rand_inclusive(0.0f, 1.0f);
      first.set_ke(edec * delta3);
      second.set_ke(edec * (1.0f - delta3));

      hcro_molecule_t& ofirst = create_individual(fi, thread_idx);
      hcro_molecule_t& osecond = create_individual(si, thread_idx);
      ofirst.assign_non_ml_fields(first);
      osecond.assign_non_ml_fields(second);

      ofirst.min_local->assign_non_ml_fields(ofirst); //(1)
      osecond.min_local->assign_non_ml_fields(osecond); //(2)
      // ofirst.min_local->set_pe(ofirst.get_pe()) por (1)
      // osecond.min_local->set_pe(osecond.get_pe()) por (2)
      remove_individual(index, thread_idx);
      result.ml_min = false;
      if (almost_equal(std::min(fpe, spe), fpe)) {
        result.molecule_index = fi;
        result.makespan = fpe;
      } else {
        result.molecule_index = si;
        result.makespan = spe;
      }
    }
    else{
      molecule.set_num_hit(molecule.get_num_hit() + 1);
      result.molecule_index = index;
      ml_ope = molecule.min_local->get_pe();
      result.makespan = std::min(ope, ml_ope);
      result.ml_min = ml_ope < ope;

    }
  }
}

void hcro_algorithm_t::do_on_wall_ineffect_collision(
  hcro_molecule_t& molecule,
  int index,
  reaction_min_t& result,
  size_t thread_idx) {

#ifndef CPU_MULTITHREADED
  static hcro_molecule_t first;
#else
  thread_local hcro_molecule_t first;
#endif
  time_unit_t fpe, ope, ml_ope, ml_fpe;
  float oke, a;
  short molecule_num_hit;
  first.init(false, false);
  molecule.generate_owi_candidate(first);
  fpe = first.calculate_pe();
  ope = molecule.get_pe();
  oke = molecule.get_ke();
  molecule_num_hit = molecule.get_num_hit() + 1;
  result.molecule_index = index;
  if (ope + oke >= fpe) {
    a = rand_inclusive(ke_loss_rate, 1.0f);
    first.set_ke((ope - fpe + oke) * a);
#ifdef CPU_MULTITHREADED
    buffer_mutex.lock();
#endif
    buffer = buffer + (ope - fpe + oke) * (1.0f - a);
#ifdef CPU_MULTITHREADED
    buffer_mutex.unlock();
#endif
    molecule.assign_non_ml_fields(first); //(1)
    //molecule.set_ke(first.get_ke()) => redundante, (1) ya lo hace
    //molecule.set_pe(first.get_pe()) => redundante, (1) ya lo hace
    molecule.set_num_hit(molecule_num_hit); //xq (1) chanco molecule.num_hit desde first.num_hit
    if (fpe < molecule.min_local->get_pe()) {
      molecule.min_local->assign_non_ml_fields(molecule); //(2) [duda molecule.min_local debe tener num_hit de molecule original + 1? y ke?]
      //molecule.min_local->set_pe(molecule.get_pe()) => redundante, (2) ya lo hace
      //molecule.min_local->set_num_hit(molecule.get_num_hit()) => redundante, (2) ya lo hace
      //molecule.min_local->ke no parece existir segun pdf 
    }
    ml_fpe = molecule.min_local->get_pe();
    result.makespan = std::min(fpe, ml_fpe);
    result.ml_min = ml_fpe < fpe;
    return;
  }
  ml_ope = molecule.min_local->get_pe();
  result.makespan = std::min(ope, ml_ope);
  result.ml_min = ml_ope < ope;
  molecule.set_num_hit(molecule_num_hit);
}

void hcro_algorithm_t::do_inter_molec_ineffect_collision(
    hcro_molecule_t& molecule,
    int index,
    reaction_min_t& result,
    size_t thread_idx) {

  int om_index;
  time_unit_t ffpe, sspe, fpe, spe, ml_fpe, ml_spe, minimum;
  float fke, ske, einter, delta4;
  hcro_molecule_t& omolecule =
      molecule.select_candidate(false, index, om_index, thread_idx);
#ifndef CPU_MULTITHREADED
  static hcro_molecule_t first;
  static hcro_molecule_t second;
#else
  thread_local hcro_molecule_t first;
  thread_local hcro_molecule_t second;
#endif
  first.init(false, false);
  second.init(false, false);
  molecule.generate_imi_candidates(omolecule, first, second);
  ffpe = first.calculate_pe();
  sspe = second.calculate_pe();
  fpe = molecule.get_pe();
  fke = molecule.get_ke();
  spe = omolecule.get_pe();
  ske = omolecule.get_ke();
  molecule.set_num_hit(molecule.get_num_hit() + 1);
  omolecule.set_num_hit(omolecule.get_num_hit() + 1);
  einter = (fpe + spe + fke + ske) - (ffpe + sspe);
  if (einter >= 0.0f){
    delta4 = rand_inclusive(0.0f, 1.0f);
    first.set_ke(einter * delta4);
    second.set_ke(einter * (1.0f - delta4));
    
    first.generate_imi_candidates(second, molecule, omolecule); //(10)
    
    //molecule.set_ke(first.get_ke());  //implicito por (10)
    //omolecule.set_ke(second.get_ke());  //implicito por (10)

    ffpe = molecule.calculate_pe();
    sspe = omolecule.calculate_pe();
    
    if (ffpe < molecule.min_local->get_pe()) { //(5)
      molecule.min_local->assign_non_ml_fields(molecule); //(1)
      //molecule.min_local->set_pe(molecule.get_pe()) hecha implicitamente por (1)
      //molecule.min_local->set_num_hit(molecule.get_num_hit()) hecha implicitamente por (1)
    }
    if (sspe < omolecule.min_local->get_pe()) { //(6)
      omolecule.min_local->assign_non_ml_fields(omolecule); //(2)
      //omolecule.min_local->set_pe(omolecule.get_pe()) hecha implicitamente por (2)
      //omolecule.min_local->set_num_hit(omolecule.get_num_hit()) hecha implicitamente por (2)
    }

    //(5) y (6), aunque el paper lo contradiga como hay cambio estructural, pe cambia

    ml_fpe = molecule.min_local->get_pe();
    ml_spe = omolecule.min_local->get_pe();
    minimum = std::min({ffpe, ml_fpe, sspe, ml_spe});

    if (almost_equal(minimum, ffpe) || almost_equal(minimum, ml_fpe)) {
      result.molecule_index = index;
      result.makespan = minimum;
      result.ml_min = ml_fpe < ffpe;
    } else {
      result.molecule_index = om_index;
      result.makespan = minimum;
      result.ml_min = ml_spe < sspe;
    }
    return;
  }
  ml_fpe = molecule.min_local->get_pe();
  ml_spe = omolecule.min_local->get_pe();
  minimum = std::min({fpe, ml_fpe, spe, ml_spe});

  if (almost_equal(minimum, fpe) || almost_equal(minimum, ml_fpe)) {
    result.molecule_index = index;
    result.makespan = minimum;
    result.ml_min = ml_fpe < fpe;
  } else {
    result.molecule_index = om_index;
    result.makespan = minimum;
    result.ml_min = ml_spe < spe;
  }
}

bool hcro_algorithm_t::do_synthesis(
  hcro_molecule_t& molecule,
  int m_index,
  reaction_min_t& result,
  size_t thread_idx) {

  int fi, om_index;
  time_unit_t fpe, wpe, owpe, ml_wpe, ml_owpe, minimum;
  float wke, owke, rnd;
  rnd = rand_inclusive(0.0f, 1.0f);
  hcro_molecule_t& omolecule =
      molecule.select_candidate(rnd > 0.5f, m_index, om_index, thread_idx);
#ifndef CPU_MULTITHREADED
  static hcro_molecule_t first;
#else
  thread_local hcro_molecule_t first;
#endif
  first.init(false, false);
  if (!molecule.generate_synth_candidate(omolecule, first)) {
    remove_individual(om_index, thread_idx);
    return false;
  }
  wke = molecule.get_ke();
  owke = omolecule.get_ke();

  //beta en paper:2012___art%3A10.1007%2Fs12293-012-0075-1.pdf pagina 10
  if (wke > beta || owke > beta) {
    return false;
  }
  fpe = first.get_pe(); //pe calculated at generate_synth_candidate
  wpe = molecule.get_pe();
  owpe = omolecule.get_pe();

  if (wpe + owpe + wke + owke >= fpe) {
    first.set_ke((wpe + owpe + wke + owke) - fpe);
    hcro_molecule_t& ofirst = create_individual(fi, thread_idx);
    ofirst.assign_non_ml_fields(first);
    ofirst.min_local->assign_non_ml_fields(ofirst); //(1)
    // ofirst.min_local->set_pe(ofirst.get_pe()) implicito por (1)
    remove_individual(m_index, thread_idx);
    remove_individual(om_index, thread_idx);
    result.molecule_index = fi;
    result.makespan = fpe;
    result.ml_min = false;

  }
  else {
    molecule.set_num_hit(molecule.get_num_hit() + 1);
    omolecule.set_num_hit(omolecule.get_num_hit() + 1);

    ml_wpe = molecule.min_local->get_pe();
    ml_owpe = omolecule.min_local->get_pe();
    minimum = std::min({wpe, ml_wpe, owpe, ml_owpe});
    if (almost_equal(minimum, wpe) || almost_equal(minimum, ml_wpe)) {
      result.molecule_index = m_index;
      result.makespan = minimum;
      result.ml_min = ml_wpe < wpe;
    } else {
      result.molecule_index = om_index;
      result.makespan = minimum;
      result.ml_min = ml_owpe < owpe;
    }
  }

  return true;
}

float hcro_algorithm_t::gen_new_random_x() {
  float u1, u2;
  static float pi = atan(1) * 4.0f;
  // std::numeric_limits<float>::epsilon()
  u1 = rand_inclusive(0.01f, 1.0f);
  u2 = rand_inclusive(0.01f, 1.0f);

  return (nd_mu +
          (nd_sigma * sqrt(-2.0f * log(1.0f - u1)) * cos(2.0f * pi * u2)));
}

std::string hcro_molecule_t::to_string() const {
  std::ostringstream str;
  str << "pe:" << get_pe() << ",ke:" << get_ke();
  str << ",nh:" << get_num_hit() << "\ninner:[";
  for (const task_t& pos : inner_molecule) {
    str << pos << " ";
  }
  str << "]\n";
  return str.str();
}

hcro_molecule_t::hcro_molecule_t()
    : pe(plus_infinity<time_unit_t>()),
      min_local(0),
      num_hit(0),
      ke(0.0),
      p_flags(15){}

hcro_molecule_t::hcro_molecule_t(const hcro_molecule_t& other) : min_local(0) {
  this->operator=(other);
}

hcro_molecule_t::~hcro_molecule_t() {
  if (min_local) {
    delete min_local;
  }
}

hcro_molecule_t& hcro_molecule_t::operator=(const hcro_molecule_t& other) /*ok*/ {
  if (&other != this) {
    this->inner_molecule = other.inner_molecule;
    this->pe = other.pe;
    this->ke = other.ke;
    this->num_hit = other.num_hit;
    this->p_flags = other.p_flags;
    if(other.min_local){
      
      if(!(this->min_local)){
        this->min_local = new hcro_molecule_t;
      }

      this->min_local->inner_molecule = other.min_local->inner_molecule;
      this->min_local->pe = other.min_local->pe;
      this->min_local->num_hit = other.min_local->num_hit;
    }
  }
  return *this;
}

void hcro_molecule_t::assign_non_ml_fields(const hcro_molecule_t& other){
  if (&other != this){
    this->inner_molecule = other.inner_molecule;
    this->pe = other.pe;
    this->ke = other.ke;
    this->num_hit = other.num_hit;
  }
}

bool hcro_molecule_t::inner_molecule_equals(
  const hcro_molecule_t& other_molecule,
  bool ml_eval) const{
  if (this->inner_molecule.size() != other_molecule.inner_molecule.size()){
    return false;
  }
  std::vector<task_t>::const_iterator oit = other_molecule.inner_molecule.begin();
  task_t other_slot;
  for(const task_t& slot : this->inner_molecule){
      other_slot = *oit;
      if (slot != other_slot){
        return false;
      }
      ++oit;
  }

  if(ml_eval){

    if(this->min_local && other_molecule.min_local){
        
        if(this->min_local->inner_molecule.size() != other_molecule.min_local->inner_molecule.size()){
          return false;
        }
        
        oit = other_molecule.min_local->inner_molecule.begin();
        for(const task_t& slot : this->min_local->inner_molecule){
            other_slot = *oit;
            if (slot != other_slot){
              return false;
            }
            ++oit;
        }

    }
    else if(!(this->min_local) && !(other_molecule.min_local)){
      //just skip
    }
    else{
      return false;
    }

  }

  return true;
}


bool hcro_molecule_t::operator==(const hcro_molecule_t& other_molecule) const {

  bool inners_cmp = this->inner_molecule_equals(other_molecule, true);
  
  if(!inners_cmp){
    return false;
  }

  if(!almost_equal(this->num_hit, other_molecule.num_hit)){
    return false;
  }

  if(!almost_equal(this->pe, other_molecule.pe)){
    return false;
  }

  if(!almost_equal(this->ke, other_molecule.ke)){
    return false;
  }

  if(this->min_local && other_molecule.min_local){
      
    if(!almost_equal(this->min_local->num_hit, other_molecule.min_local->num_hit)){
      return false;
    }

    if(!almost_equal(this->min_local->pe, other_molecule.min_local->pe)){
      return false;
    }
  }
  else if(!(this->min_local) && !(other_molecule.min_local)){
    //just skip
  }
  else{
    return false;
  }

  return true;
}

bool hcro_molecule_t::operator!=(const hcro_molecule_t& other_molecule) const {
  return !(this->operator==(other_molecule));
}

//always returns false; given inner molecule is almost always not ordered
bool hcro_molecule_t::inner_molecule_is_sorted() const{
  assert(this->inner_molecule.size() > 0);
  task_t prev_tid = *(this->inner_molecule.begin()) + 1;
  for(const task_t& tid : this->inner_molecule){
    if(tid >= prev_tid){
      return false;
    }
    prev_tid = tid;
  }
  return true;
}

bool hcro_molecule_t::is_empty() const{
  return this->inner_molecule.size() < 1;
}

bool hcro_molecule_t::is_valid() const{
  
  assert(ptr_algorithm);
  std::vector<task_t>::const_iterator curr, end, ccurr;
  const hms_param_t& ref_param = ptr_algorithm->get_param();
#ifndef CPU_MULTITHREADED
  static std::vector<task_t> forward_tasks;
#else
  thread_local std::vector<task_t> forward_tasks;
#endif
  size_t offset = 0, rest_sz;
  if(forward_tasks.capacity() < 1){
    forward_tasks.reserve(ref_param.get_num_tasks());
  }

  for(const task_t& tid : this->inner_molecule){
    rest_sz = this->inner_molecule.size() - offset - 1;
    if(rest_sz > 0){
      forward_tasks.resize(rest_sz);
      std::partial_sort_copy(
        inner_molecule.begin() + offset + 1,
        inner_molecule.end(),
        forward_tasks.begin(),
        forward_tasks.end(),
        std::less<task_t>());
      ref_param.sorted_predecessors(tid, curr, end);
      for(; curr != end; ++curr){
        if(std::binary_search(forward_tasks.begin(), forward_tasks.end(), *curr)){
          return false;
        }
      }
    }
    ++offset;
  }
  return true;
}

bool hcro_molecule_t::has_min_local() const{
  return this->min_local != 0;
}

void hcro_molecule_t::set_inner_min_default() {
  if (!min_local) {
    min_local = new hcro_molecule_t;
  }
  min_local->assign_non_ml_fields(*this);
}

void hcro_molecule_t::init(bool inner_min_init, bool set_molec_values) {
  assert(ptr_algorithm);
  const hms_param_t& ref_param = ptr_algorithm->get_param();
  inner_molecule.resize(ref_param.get_num_tasks());
  if (set_molec_values) {
    task_t tid = 0;
    for (task_t&  slot : inner_molecule) {
      slot = tid++;
    }
  }
  ke = ptr_algorithm->ke_init_value;
  if(inner_min_init){
    set_inner_min_default();
  }
}

bool hcro_molecule_t::xor_range_bounds(
  const hcro_molecule_t& other_molecule,
  int& begin,
  int& end) const {
  
  std::vector<task_t>::const_iterator it, oit;
  assert(ptr_algorithm);
  const hms_param_t& ref_param = ptr_algorithm->get_param();
  int num_tasks = ref_param.get_num_tasks();
  begin = 0;
  end = num_tasks - 1;
  it = inner_molecule.begin();
  oit = other_molecule.inner_molecule.begin();
  while (*it == *oit && it != inner_molecule.end()) {
    ++begin;
    ++it;
    ++oit;
  }
  if (begin == num_tasks) {
    begin = 0;
    end = num_tasks - 1;
    return true;
  }
  it = inner_molecule.end() - 1;
  oit = other_molecule.inner_molecule.end() - 1;
  while (*it == *oit) {
    --end;
    --it;
    --oit;
  }
  return false;
}

int hcro_molecule_t::distance_to(const hcro_molecule_t& other_molecule) const {
  assert(inner_molecule.size() == other_molecule.inner_molecule.size());
  int counter = 0;
  std::vector<task_t>::const_iterator oit = other_molecule.inner_molecule.begin();
  task_t other_slot;
  for (const task_t& slot : inner_molecule) {
    other_slot = *oit;
    if(slot != other_slot){
      ++counter;
    }
    ++oit;
  }
  return counter;
}

void hcro_molecule_t::left_rotate_by_one(int incl_begin, int incl_end) {
  assert(incl_begin <= incl_end);
  std::vector<task_t>::iterator ini_rot = inner_molecule.begin() + incl_begin;
  std::rotate(ini_rot, ini_rot + 1, inner_molecule.begin() + incl_end + 1);
}

void hcro_molecule_t::right_rotate_by_one(int incl_begin, int incl_end) {
  assert(incl_begin <= incl_end);
  std::vector<task_t>::iterator ilast_rot = inner_molecule.begin() + incl_end;
  std::rotate(inner_molecule.begin() + incl_begin, ilast_rot, ilast_rot + 1);
}

void hcro_molecule_t::exchange_segments(const hcro_molecule_t& other_molecule,
                                    int cut_index, hcro_molecule_t& first,
                                    hcro_molecule_t& second) const {
#ifndef CPU_MULTITHREADED
  static std::vector<task_t> tasks_buff;
#else
  thread_local std::vector<task_t> tasks_buff;
#endif
  const hms_param_t& ref_param = ptr_algorithm->get_param();
  int ntasks = ref_param.get_num_tasks();
  if (tasks_buff.capacity() < 1){
    tasks_buff.reserve(ntasks);
  }
  tasks_buff.clear();
  tasks_buff.resize(cut_index);
  
  std::vector<task_t>::const_iterator omol_it, end;
  std::vector<task_t>::iterator mol_it;

  first.assign_non_ml_fields(*this);
  second.assign_non_ml_fields(other_molecule);

  std::partial_sort_copy(inner_molecule.begin(),
                         inner_molecule.begin() + cut_index, tasks_buff.begin(),
                         tasks_buff.end(), std::less<task_t>());
  mol_it = first.inner_molecule.begin() + cut_index;
  omol_it = other_molecule.inner_molecule.begin();
  end = other_molecule.inner_molecule.end();
  while (omol_it != end) {
    if (!std::binary_search(tasks_buff.begin(), tasks_buff.end(), *omol_it)) {
      *mol_it = *omol_it;
      ++mol_it;
    }
    ++omol_it;
  }

  tasks_buff.clear();
  tasks_buff.resize(cut_index);

  std::partial_sort_copy(other_molecule.inner_molecule.begin(),
                         other_molecule.inner_molecule.begin() + cut_index,
                         tasks_buff.begin(), tasks_buff.end(),
                         std::less<task_t>());
  mol_it = second.inner_molecule.begin() + cut_index;
  omol_it = inner_molecule.begin();
  end = inner_molecule.end();
  while (omol_it != end) {
    if (!std::binary_search(tasks_buff.begin(), tasks_buff.end(), *omol_it)) {
      *mol_it = *omol_it;
      ++mol_it;
    }
    ++omol_it;
  }
}

int hcro_molecule_t::get_inner_molec_digest() const{
  int index = 0;
  int accum = 0;
  for(task_t tid : this->inner_molecule){
    
    if(index % 2){
      accum -= (index * tid);
    }
    else{
      accum += (index * tid);
    }
    ++index;
  }
  return accum;
}

hcro_molecule_t& hcro_molecule_t::select_candidate(
  bool asc_order,
  int molecule_index,
  int& selected_index,
  size_t thread_idx) {
  
  float X;
  int x, selected;
  size_t i, csz;
  std::vector<size_t>::const_iterator it;
  csz = ptr_algorithm->candidates_sz;
#ifndef CPU_MULTITHREADED
  static storing_cmp_t cmp;
  static std::vector<size_t> candidates;
  static std::vector<size_t> io_candidates;
  static std::vector<int> buff;
#else
  thread_local storing_cmp_t cmp;
  thread_local std::vector<size_t> candidates;
  thread_local std::vector<size_t> io_candidates;
  thread_local std::vector<int> buff;
#endif
  candidates.reserve(csz);
  io_candidates.reserve(csz);
  candidates.resize(0);
  io_candidates.clear();
  io_candidates.push_back(molecule_index);
#ifdef CPU_MULTITHREADED
  ptr_algorithm->mp_mutex.lock();  // because of population size thread
                                   // sensitive
#endif
  buff.resize(ptr_algorithm->population.size());
#ifdef CPU_MULTITHREADED
  ptr_algorithm->mp_mutex.unlock();
#endif
  if (!cmp.buff_distances) {
    cmp.buff_distances = &buff;
  }
  cmp.asc_ord = asc_order;
  cmp.ptr_algorithm = ptr_algorithm;
  cmp.ptr_main_mol = this;

  ordered_excluded_t except;
  except.candidates = &io_candidates;

  for (i = 0; i < csz;){
    
    hcro_molecule_t& ref = ptr_algorithm->select_individual(selected, EXISTENT, thread_idx, &except);
    
    if (except.solution_found){
      io_candidates.insert(except.geq_it, selected);
      if(!ref.inner_molecule_equals(*this)){
      #ifdef CPU_MULTITHREADED
        ptr_algorithm->mp_mutex.lock();
      #endif
        it = std::lower_bound(candidates.begin(), candidates.end(), selected, cmp);
      #ifdef CPU_MULTITHREADED
        ptr_algorithm->mp_mutex.unlock();
      #endif
        candidates.insert(it, selected);
        ++i;
      }
    }
  }
  
  X = ptr_algorithm->gen_new_random_x();
  x = (csz * abs(X)) / 3;
  if (x >= csz) {
    x = csz - 1;
  }
  assert(x >= 0 && x < csz);

  selected_index = *(candidates.begin() + x);
  for (const size_t& candidate : candidates) {
    cmp.buff_distances->operator[](candidate) = -1;
  }

#ifdef CPU_MULTITHREADED
  ptr_algorithm->mp_mutex.lock();
#endif
  hcro_molecule_t& ret_ref = ptr_algorithm->population[selected_index];
#ifdef CPU_MULTITHREADED
  ptr_algorithm->mp_mutex.unlock();
#endif

  return ret_ref;
}

void hcro_molecule_t::generate_imi_candidates(
    const hcro_molecule_t& omolecule, hcro_molecule_t& first,
    hcro_molecule_t& second) const {
  int i, xor_ibegin, xor_iend;
  this->xor_range_bounds(omolecule, xor_ibegin, xor_iend);
  i = rand_inclusive(xor_ibegin, xor_iend);
  this->exchange_segments(omolecule, i, first, second);
}

void hcro_molecule_t::generate_decomp_candidates(hcro_molecule_t& first,
                                            hcro_molecule_t& second) const {
  int i0, i1, j0, j1, k0, k1, last_index;
  assert(ptr_algorithm);
  const hms_param_t& ref_param = ptr_algorithm->get_param();
  last_index = inner_molecule.size() - 1;
  i0 = rand_inclusive(1, last_index);
  i1 = rand_inclusive(1, last_index);
  j0 = ref_param.last_predecessor_index_of(i0, inner_molecule);
  j1 = ref_param.last_predecessor_index_of(i1, inner_molecule);
  k0 = ref_param.first_succesor_index_of(i0, inner_molecule);
  k1 = ref_param.first_succesor_index_of(i1, inner_molecule);
  first.assign_non_ml_fields(*this);
  second.assign_non_ml_fields(*this);

  if (i0 - j0 < k0 - i0) {
    first.left_rotate_by_one(i0, k0 - 1);

  } else {
    assert(j0 + 1 <= i0);
    first.right_rotate_by_one(j0 + 1, i0);
  }
  if (i1 - j1 < k1 - i1) {
    second.left_rotate_by_one(i1, k1 - 1);

  } else {
    assert(j1 + 1 <= i1);
    second.right_rotate_by_one(j1 + 1, i1);
  }
}

void hcro_molecule_t::generate_owi_candidate(hcro_molecule_t& first) const {
  int i, j, k, x, left_len, right_len, ntasks;
  float X;
  assert(ptr_algorithm);
  const hms_param_t& ref_param = ptr_algorithm->get_param();
  ntasks = ref_param.get_num_tasks();
  i = rand_inclusive(1, ntasks - 1);
  j = ref_param.last_predecessor_index_of(i, inner_molecule);
  k = ref_param.first_succesor_index_of(i, inner_molecule);
  X = ptr_algorithm->gen_new_random_x();
  left_len = i - j;
  right_len = k - i;
  x = (std::max(left_len, right_len) * abs(X)) / 3;
  first.assign_non_ml_fields(*this);
  if (left_len < right_len && (i + x) < k) {
    assert((i + x) < ntasks);
    first.left_rotate_by_one(i, i + x);

  } else {
    if ((i - x) <= j) {
      x = i - j - 1;
    }
    first.right_rotate_by_one(i - x, i);  //#
  }
}

void hcro_molecule_t::set_curr_algorithm(hcro_algorithm_t* ptr){
  ptr_algorithm = ptr;
}

hcro_algorithm_t* hcro_molecule_t::get_curr_algorithm(){
  return hcro_molecule_t::ptr_algorithm;
}

std::vector<hcro_molecule_t> hcro_molecule_t::generate_model_molecules(std::vector<priority_cmp_t<float> >& ordering_criteria){
  std::vector<hcro_molecule_t> model_molecules;
  const hms_param_t& ref_param = hcro_molecule_t::get_curr_algorithm()->get_param();
  model_molecules.resize(ordering_criteria.size());
  std::vector<hcro_molecule_t>::iterator imolecule = model_molecules.begin();
  for (priority_cmp_t<float>& cmp : ordering_criteria){
    imolecule->init(false);
    cmp.generate(ref_param, ref_param.get_num_tasks());
    std::sort(imolecule->inner_molecule.begin(),
              imolecule->inner_molecule.end(), cmp);
    ++imolecule;
  }
  return model_molecules;
}

hcro_molecule_t hcro_molecule_t::random_generate(
  short nh_lb,
  short nh_ub,
  time_unit_t pe_lb,
  time_unit_t pe_ub,
  float ke_lb,
  float ke_ub,
  bool ml_init,
  bool use_priorities){

  hcro_algorithm_t* ptr = hcro_molecule_t::get_curr_algorithm();
  const hms_param_t& ref_param = ptr->get_param();
  hcro_molecule_t m, model_molec;

  model_molec.init(ml_init, true);
  m.init(ml_init, true);

  m.set_num_hit(rand_inclusive(nh_lb, nh_ub));
  m.set_ke(rand_inclusive(ke_lb, ke_ub));

  if(use_priorities){
    assert(ptr->ordering_criteria.size() > 0);
    int i, j, ilast_task;
    ilast_task = ref_param.get_num_tasks() - 1;
    i = rand_inclusive(0, ptr->ordering_criteria.size() - 1);
    priority_cmp_t<float>& cmp_ref = ptr->ordering_criteria[i];
    cmp_ref.generate(ref_param, ref_param.get_num_tasks());
    std::sort(model_molec.inner_molecule.begin(), model_molec.inner_molecule.end(), cmp_ref);
    
    i = rand_inclusive(0, ilast_task - 1);
    m.init_inner_sequence(model_molec, i);
    m.calculate_pe();
  }
  else{
    m.set_pe(rand_inclusive(pe_lb, pe_ub));
    custom_random_shuffle(m.inner_molecule.begin(), m.inner_molecule.end());
  }

  if(ml_init){
    m.set_ml_num_hit(m.get_num_hit());
    m.set_ml_pe(m.get_pe());
    
    std::vector<task_t>& ml_ref = m.get_ml_inner_molecule();
    ml_ref = m.get_inner_molecule();
  }

  return m;
}

hcro_molecule_t hcro_molecule_t::from_string(
  std::string str,
  short nh_lb,
  short nh_ub,
  time_unit_t pe_lb,
  time_unit_t pe_ub,
  bool ml_init,
  bool calc_pe){

  hcro_algorithm_t* ptr_algorithm = hcro_molecule_t::get_curr_algorithm();
  const hms_param_t& ref_param = ptr_algorithm->get_param();
  hcro_molecule_t m;
  std::string delimiter = ";";
  m.init(ml_init, true);

  m.set_num_hit(rand_inclusive(nh_lb, nh_ub));
  m.set_ke(ptr_algorithm->get_ke_init_value());
  m.get_inner_molecule() = split_str_into_vector<task_t>(str, delimiter);
  assert(m.get_inner_molecule().size() == ref_param.get_num_tasks());


  if(!calc_pe){
    m.set_pe(rand_inclusive(pe_lb, pe_ub));
  }
  else{
    m.calculate_pe();
  }  

  if(ml_init){
    m.set_ml_num_hit(m.get_num_hit());
    m.set_ml_pe(m.get_pe());
    
    std::vector<task_t>& ml_ref = m.get_ml_inner_molecule();
    ml_ref = m.get_inner_molecule();
  }

  return m;
}

std::ostream& operator<<(std::ostream& os, const hcro_molecule_t& m){

    int num_digits = 3;
    int i;
    if(m.get_print_flags() & hcro_molecule_t::print_flags_t::SCALAR_FIELDS){
      
      if(m.get_print_flags() & hcro_molecule_t::print_flags_t::USE_HEADINGS){
        os << "==========[molecule_fields]==========" << std::endl;
      }

      os << "num_hit:" << m.get_num_hit() << std::endl;
      os << "pe:" << m.get_pe() << std::endl;
      os << "ke:" << m.get_ke() << std::endl;
    }
    
    if(m.get_print_flags() & hcro_molecule_t::print_flags_t::INNER_MOLEC){
      
      if(m.get_print_flags() & hcro_molecule_t::print_flags_t::USE_HEADINGS){
        os << "==========[inner_molecule]==========" << std::endl;
      }
      i = 0;
      for(const task_t& tid : m.get_inner_molecule()){
        os << "i:" << std::setw(num_digits) << i++;
        os << " => " << std::setw(num_digits) << tid << std::endl;
      }
    }

    bool has_mlflags = (m.get_print_flags() & hcro_molecule_t::print_flags_t::ML_SCALAR_FIELDS);
    has_mlflags = has_mlflags || (m.get_print_flags() & hcro_molecule_t::print_flags_t::ML_INNER_MOLEC);

    if(has_mlflags && m.has_min_local()){

      if(m.get_print_flags() & hcro_molecule_t::print_flags_t::ML_SCALAR_FIELDS){
        
        if(m.get_print_flags() & hcro_molecule_t::print_flags_t::USE_HEADINGS){
          os << "==========[inner_molecule_fields]==========" << std::endl;
        }
        os << "ml_num_hit:" << m.get_ml_num_hit() << std::endl;
        os << "ml_pe:" << m.get_ml_pe() << std::endl;
      }
      
      if(m.get_print_flags() & hcro_molecule_t::print_flags_t::ML_INNER_MOLEC){

        if(m.get_print_flags() & hcro_molecule_t::print_flags_t::USE_HEADINGS){
          os << "==========[inner_ml_molecule]==========" << std::endl;
        }
        i = 0;
        for(const task_t& tid : m.get_ml_inner_molecule()){
          os << "i:" << std::setw(num_digits) << i++;
          os << " => " << std::setw(num_digits) << tid << std::endl;
        }
      }
    }

    return os;
}

const std::vector<task_t>& hcro_molecule_t::get_inner_molecule() const {
  return this->inner_molecule;
}

std::vector<task_t>& hcro_molecule_t::get_inner_molecule(){
  return this->inner_molecule;
}

const std::vector<task_t>& hcro_molecule_t::get_ml_inner_molecule() const {
  assert(this->min_local);
  return this->min_local->inner_molecule;
}

std::vector<task_t>& hcro_molecule_t::get_ml_inner_molecule(){
  assert(this->min_local);
  return this->min_local->inner_molecule;
}

bool hcro_molecule_t::generate_synth_candidate(
    const hcro_molecule_t& other, hcro_molecule_t& candidate) const {
  
  int i, xor_ibegin, xor_iend, ntasks;
#ifndef CPU_MULTITHREADED
  static hcro_molecule_t first;
  static hcro_molecule_t second;
#else
  thread_local hcro_molecule_t first;
  thread_local hcro_molecule_t second;
#endif
  assert(ptr_algorithm);
  const hms_param_t& ref_param = ptr_algorithm->get_param();
  ntasks = ref_param.get_num_tasks();
  first.inner_molecule.reserve(ntasks);
  second.inner_molecule.reserve(ntasks);
  if (!(this->inner_molecule_equals(other))) {
    this->xor_range_bounds(other, xor_ibegin, xor_iend);
    i = rand_inclusive(xor_ibegin, xor_iend);
    this->exchange_segments(other, i, first, second);
    assert(first.inner_molecule.size() == ntasks);
    assert(second.inner_molecule.size() == ntasks);
    if (first.calculate_pe() > second.calculate_pe()) {
      candidate.assign_non_ml_fields(second);
    } else {
      candidate.assign_non_ml_fields(first);
    }
    return true;
  }
  return false;
}

storing_cmp_t::storing_cmp_t()
    : ptr_algorithm(0), ptr_main_mol(0), asc_ord(true), buff_distances(0) {}

bool storing_cmp_t::operator()(size_t ifirst, size_t isecond) {
  int& fd = buff_distances->at(ifirst);
  int& sd = buff_distances->at(isecond);
  if (fd == -1) {
    fd = ptr_main_mol->distance_to(ptr_algorithm->population[ifirst]);
  }
  if (sd == -1) {
    sd = ptr_main_mol->distance_to(ptr_algorithm->population[isecond]);
  }
  if (asc_ord) return fd < sd;
  return fd > sd;
}
