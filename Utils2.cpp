/*
 * File:   HCROAlgorithm.cpp
 * Author: Alexander
 *
 * Created on 31 de mayo de 2015, 08:40 AM
 */

#include "Utils2.h"
#include <algorithm>

float gen_new_random_x(hcro_algorithm_t* ptr) {
  float u1, u2;
  static float pi = atan(1) * 4.0f;
  // std::numeric_limits<float>::epsilon()
  u1 = my_rand_inclusive(0.01f, 1.0f);
  u2 = my_rand_inclusive(0.01f, 1.0f);

  return (ptr->get_mu() + (ptr->get_sigma() * sqrt(-2.0f * log(1.0f - u1)) * cos(2.0f * pi * u2)));
}

void generate_owi_candidate(
  const hcro_molecule_t& molecule,
  hcro_molecule_t& first){
  int i, j, k, x, left_len, right_len, ntasks;
  float X;
  hcro_algorithm_t* ptr_algorithm  = hcro_molecule_t::get_curr_algorithm();
  assert(ptr_algorithm);
  const hms_param_t& ref_param = ptr_algorithm->get_param();
  ntasks = ref_param.get_num_tasks();
  i = my_rand_inclusive(1, ntasks - 1);
  j = ref_param.last_predecessor_index_of(i, molecule.get_inner_molecule());
  k = ref_param.first_succesor_index_of(i, molecule.get_inner_molecule());
  X = gen_new_random_x(ptr_algorithm);
  left_len = i - j;
  right_len = k - i;
  x = (std::max(left_len, right_len) * abs(X)) / 3;
  first.assign_non_ml_fields(molecule);
  if (left_len < right_len && (i + x) < k) {
    assert((i + x) < ntasks);
    first.left_rotate_by_one(i, i + x);

  } else {
    if ((i - x) <= j) {
      x = i - j - 1;
    }
    first.right_rotate_by_one(i - x, i);  //#
  }
}

void generate_decomp_candidates(
  const hcro_molecule_t& molecule,
  hcro_molecule_t& first,
  hcro_molecule_t& second){
  int i0, i1, j0, j1, k0, k1, last_index;
  hcro_algorithm_t* ptr_algorithm  = hcro_molecule_t::get_curr_algorithm();
  assert(ptr_algorithm);
  const hms_param_t& ref_param = ptr_algorithm->get_param();
  last_index = molecule.get_inner_molecule().size() - 1;
  i0 = my_rand_inclusive(1, last_index);
  i1 = my_rand_inclusive(1, last_index);
  j0 = ref_param.last_predecessor_index_of(i0, molecule.get_inner_molecule());
  j1 = ref_param.last_predecessor_index_of(i1, molecule.get_inner_molecule());
  k0 = ref_param.first_succesor_index_of(i0, molecule.get_inner_molecule());
  k1 = ref_param.first_succesor_index_of(i1, molecule.get_inner_molecule());
  first.assign_non_ml_fields(molecule);
  second.assign_non_ml_fields(molecule);

  if (i0 - j0 < k0 - i0) {
    first.left_rotate_by_one(i0, k0 - 1);

  } else {
    assert(j0 + 1 <= i0);
    first.right_rotate_by_one(j0 + 1, i0);
  }
  if (i1 - j1 < k1 - i1) {
    second.left_rotate_by_one(i1, k1 - 1);

  } else {
    assert(j1 + 1 <= i1);
    second.right_rotate_by_one(j1 + 1, i1);
  }
}

void generate_imi_candidates(
  const hcro_molecule_t& molecule,
  const hcro_molecule_t& omolecule,
  hcro_molecule_t& first,
  hcro_molecule_t& second){
  int i, xor_ibegin, xor_iend;
  molecule.xor_range_bounds(omolecule, xor_ibegin, xor_iend);
  i = my_rand_inclusive(xor_ibegin, xor_iend);
  molecule.exchange_segments(omolecule, i, first, second);
}

bool generate_synth_candidate(
  const hcro_molecule_t& molecule,
  const hcro_molecule_t& other,
  hcro_molecule_t& candidate){
  
  int i, xor_ibegin, xor_iend, ntasks;
  hcro_molecule_t first;
  hcro_molecule_t second;
  hcro_algorithm_t* ptr_algorithm  = hcro_molecule_t::get_curr_algorithm();
  assert(ptr_algorithm);
  const hms_param_t& ref_param = ptr_algorithm->get_param();
  ntasks = ref_param.get_num_tasks();
  first.get_inner_molecule().reserve(ntasks);
  second.get_inner_molecule().reserve(ntasks);
  if (!(molecule.inner_molecule_equals(other))) {
    molecule.xor_range_bounds(other, xor_ibegin, xor_iend);
    i = my_rand_inclusive(xor_ibegin, xor_iend);
    molecule.exchange_segments(other, i, first, second);
    assert(first.get_inner_molecule().size() == ntasks);
    assert(second.get_inner_molecule().size() == ntasks);
    if (first.calculate_pe() > second.calculate_pe()) {
      candidate.assign_non_ml_fields(second);
    } else {
      candidate.assign_non_ml_fields(first);
    }
    return true;
  }
  return false;
}



void do_decomposition(
  hcro_molecule_t& molecule,
  int index,
  hcro_molecule_t& other_molecule,
  int om_index,
  reaction_min_t& result){

  bool ml_min = false;
  time_unit_t fpe, spe, ope, ml_ope;
  float oke, delta1, delta2, delta3, edec;
  hcro_molecule_t first;
  hcro_molecule_t second;

  hcro_algorithm_t* ptr_algorithm  = hcro_molecule_t::get_curr_algorithm();
  assert(ptr_algorithm);

  first.init(false, false);
  second.init(false, false);
  generate_decomp_candidates(molecule, first, second);

  fpe = first.calculate_pe();
  spe = second.calculate_pe();
  ope = molecule.get_pe();
  oke = molecule.get_ke();


  if (ope + oke >= fpe + spe) {
    edec = ope + oke - (fpe + spe);
    delta3 = my_rand_inclusive(0.0f, 1.0f);
    first.set_ke(edec * delta3);
    second.set_ke(edec * (1.0f - delta3));

    molecule.assign_non_ml_fields(first);

    molecule.get_min_local().assign_non_ml_fields(first); //(1)

    if(spe < other_molecule.get_pe()){

      other_molecule.assign_non_ml_fields(second);  
      if(spe < other_molecule.get_min_local().get_pe()){     
        other_molecule.get_min_local().assign_non_ml_fields(second);
      }
    }
    else if(spe >= other_molecule.get_pe()){
      spe = other_molecule.get_pe();
    }

    if(spe > other_molecule.get_min_local().get_pe()){
      spe = other_molecule.get_min_local().get_pe();
      ml_min = true;
    }

    result.ml_min = ml_min;
    if (almost_equal(std::min(fpe, spe), fpe)) {
      result.molecule_index = index;
      result.makespan = fpe;
    } else {
      result.molecule_index = om_index;
      result.makespan = spe;
    }
  }
  else{

    delta1 = my_rand_inclusive(0.0f, 1.0f);
    delta2 = my_rand_inclusive(0.0f, 1.0f);
 
    edec = ope + oke + (delta1 * delta2 * ptr_algorithm->get_buffer_value()) - (fpe + spe);

    if(edec >= 0.0f){

      ptr_algorithm->set_buffer_value(ptr_algorithm->get_buffer_value() * (1.0f - delta1 * delta2));
      delta3 = my_rand_inclusive(0.0f, 1.0f);
      first.set_ke(edec * delta3);
      second.set_ke(edec * (1.0f - delta3));

      molecule.assign_non_ml_fields(first);

      molecule.get_min_local().assign_non_ml_fields(first); //(1)
              
      if(spe < other_molecule.get_pe()){       

        other_molecule.assign_non_ml_fields(second);  
        if(spe < other_molecule.get_min_local().get_pe()){
          other_molecule.get_min_local().assign_non_ml_fields(second);
        }
      }
      else if(spe >= other_molecule.get_pe()){
        spe = other_molecule.get_pe();
      }

      if(spe > other_molecule.get_min_local().get_pe()){
        spe = other_molecule.get_min_local().get_pe();
        ml_min = true;
      }

      result.ml_min = ml_min;
      if (almost_equal(std::min(fpe, spe), fpe)) {
        result.molecule_index = index;
        result.makespan = fpe;
      } else {
        result.molecule_index = om_index;
        result.makespan = spe;
      }
    }
    else{
      molecule.set_num_hit(molecule.get_num_hit() + 1);
      result.molecule_index = index;
      ml_ope = molecule.get_min_local().get_pe();
      result.makespan = std::min(ope, ml_ope);
      result.ml_min = ml_ope < ope;

    }
  }
}

void do_on_wall_ineffect_collision(
  hcro_molecule_t& molecule,
  int index,
  reaction_min_t& result){

  hcro_molecule_t first;
  hcro_algorithm_t* ptr_algorithm  = hcro_molecule_t::get_curr_algorithm();
  assert(ptr_algorithm);
  time_unit_t fpe, ope, ml_ope, ml_fpe;
  float oke, a;
  short molecule_num_hit;
  first.init(false, false);
  generate_owi_candidate(molecule, first);



  fpe = first.calculate_pe();
  ope = molecule.get_pe();
  oke = molecule.get_ke();
  molecule_num_hit = molecule.get_num_hit() + 1;
  result.molecule_index = index;

  if (ope + oke >= fpe) {
    a = my_rand_inclusive(ptr_algorithm->get_ke_loss_rate(), 1.0f);
    first.set_ke((ope - fpe + oke) * a);

    ptr_algorithm->set_buffer_value(ptr_algorithm->get_buffer_value() + (ope - fpe + oke) * (1.0f - a));

    molecule.assign_non_ml_fields(first); //(1)
    //molecule.set_ke(first.get_ke()) => redundante, (1) ya lo hace
    //molecule.set_pe(first.get_pe()) => redundante, (1) ya lo hace
    molecule.set_num_hit(molecule_num_hit); //xq (1) chanco molecule.num_hit desde first.num_hit
    if (fpe < molecule.get_min_local().get_pe()) {
      molecule.get_min_local().assign_non_ml_fields(molecule); //(2) [duda molecule.min_local debe tener num_hit de molecule original + 1? y ke?]
      //molecule.get_min_local().set_pe(molecule.get_pe()) => redundante, (2) ya lo hace
      //molecule.get_min_local().set_num_hit(molecule.get_num_hit()) => redundante, (2) ya lo hace
      //molecule.get_min_local().ke no parece existir segun pdf 
    }
    ml_fpe = molecule.get_min_local().get_pe();
    result.makespan = std::min(fpe, ml_fpe);
    result.ml_min = ml_fpe < fpe;
    return;
  }
  ml_ope = molecule.get_min_local().get_pe();
  result.makespan = std::min(ope, ml_ope);
  result.ml_min = ml_ope < ope;
  molecule.set_num_hit(molecule_num_hit);
}

void do_inter_molec_ineffect_collision(
  hcro_molecule_t& molecule,
  int index,
  hcro_molecule_t& omolecule,
  int om_index,
  reaction_min_t& result){

  time_unit_t ffpe, sspe, fpe, spe, ml_fpe, ml_spe, minimum;
  float fke, ske, einter, delta4;

  hcro_molecule_t first;
  hcro_molecule_t second;

  first.init(false, false);
  second.init(false, false);
  generate_imi_candidates(molecule, omolecule, first, second);
  ffpe = first.calculate_pe();
  sspe = second.calculate_pe();
  fpe = molecule.get_pe();
  fke = molecule.get_ke();
  spe = omolecule.get_pe();
  ske = omolecule.get_ke();
  molecule.set_num_hit(molecule.get_num_hit() + 1);
  omolecule.set_num_hit(omolecule.get_num_hit() + 1);
  einter = (fpe + spe + fke + ske) - (ffpe + sspe);
  if (einter >= 0.0f){
    delta4 = my_rand_inclusive(0.0f, 1.0f);
    first.set_ke(einter * delta4);
    second.set_ke(einter * (1.0f - delta4));
    
    generate_imi_candidates(first, second, molecule, omolecule); //(10)
    
    //molecule.set_ke(first.get_ke());  //implicito por (10)
    //omolecule.set_ke(second.get_ke());  //implicito por (10)

    ffpe = molecule.calculate_pe();
    sspe = omolecule.calculate_pe();
    
    if (ffpe < molecule.get_min_local().get_pe()) { //(5)
      molecule.get_min_local().assign_non_ml_fields(molecule); //(1)
      //molecule.get_min_local().set_pe(molecule.get_pe()) hecha implicitamente por (1)
      //molecule.get_min_local().set_num_hit(molecule.get_num_hit()) hecha implicitamente por (1)
    }
    if (sspe < omolecule.get_min_local().get_pe()) { //(6)
      omolecule.get_min_local().assign_non_ml_fields(omolecule); //(2)
      //omolecule.get_min_local().set_pe(omolecule.get_pe()) hecha implicitamente por (2)
      //omolecule.get_min_local().set_num_hit(omolecule.get_num_hit()) hecha implicitamente por (2)
    }

    //(5) y (6), aunque el paper lo contradiga como hay cambio estructural, pe cambia

    ml_fpe = molecule.get_min_local().get_pe();
    ml_spe = omolecule.get_min_local().get_pe();
    minimum = std::min({ffpe, ml_fpe, sspe, ml_spe});

    if (almost_equal(minimum, ffpe) || almost_equal(minimum, ml_fpe)) {
      result.molecule_index = index;
      result.makespan = minimum;
      result.ml_min = ml_fpe < ffpe;
    } else {
      result.molecule_index = om_index;
      result.makespan = minimum;
      result.ml_min = ml_spe < sspe;
    }
    return;
  }
  ml_fpe = molecule.get_min_local().get_pe();
  ml_spe = omolecule.get_min_local().get_pe();
  minimum = std::min({fpe, ml_fpe, spe, ml_spe});

  if (almost_equal(minimum, fpe) || almost_equal(minimum, ml_fpe)) {
    result.molecule_index = index;
    result.makespan = minimum;
    result.ml_min = ml_fpe < fpe;
  } else {
    result.molecule_index = om_index;
    result.makespan = minimum;
    result.ml_min = ml_spe < spe;
  }
}

bool do_synthesis(
    hcro_molecule_t& molecule,
    int m_index,
    hcro_molecule_t& omolecule,
    int om_index,
    reaction_min_t& result){

  time_unit_t fpe, wpe, owpe, ml_wpe, ml_owpe, minimum;
  float wke, owke;
  hcro_molecule_t first;
  hcro_algorithm_t* ptr_algorithm  = hcro_molecule_t::get_curr_algorithm();
  assert(ptr_algorithm);

  first.init(false, false);
  if (!generate_synth_candidate(molecule, omolecule, first)){
    return false;
  }
  wke = molecule.get_ke();
  owke = omolecule.get_ke();

  //beta en paper:2012___art%3A10.1007%2Fs12293-012-0075-1.pdf pagina 10
  if (wke > ptr_algorithm->get_beta() || owke > ptr_algorithm->get_beta()) {
    return false;
  }
  fpe = first.get_pe(); //pe calculated at generate_synth_candidate
  wpe = molecule.get_pe();
  owpe = omolecule.get_pe();

  if (wpe + owpe + wke + owke >= fpe) {
    first.set_ke((wpe + owpe + wke + owke) - fpe);
    molecule.assign_non_ml_fields(first);
    molecule.get_min_local().assign_non_ml_fields(first); //(1)
    // ofirst.get_min_local().set_pe(ofirst.get_pe()) implicito por (1)
    result.molecule_index = m_index;
    result.makespan = fpe;
    result.ml_min = false;

  }
  else {
    molecule.set_num_hit(molecule.get_num_hit() + 1);
    omolecule.set_num_hit(omolecule.get_num_hit() + 1);

    ml_wpe = molecule.get_min_local().get_pe();
    ml_owpe = omolecule.get_min_local().get_pe();
    minimum = std::min({wpe, ml_wpe, owpe, ml_owpe});
    if (almost_equal(minimum, wpe) || almost_equal(minimum, ml_wpe)) {
      result.molecule_index = m_index;
      result.makespan = minimum;
      result.ml_min = ml_wpe < wpe;
    } else {
      result.molecule_index = om_index;
      result.makespan = minimum;
      result.ml_min = ml_owpe < owpe;
    }
  }

  return true;
}

void do_single_reaction(
  hcro_molecule_t& selected,
  int index,
  hcro_molecule_t& other_selected,
  int other_index,
  reaction_min_t& selected_ms){

  bool synth_ok;
  hcro_algorithm_t* ptr_algorithm  = hcro_molecule_t::get_curr_algorithm();
  assert(ptr_algorithm);
  float b = my_rand_inclusive(0.0f, 1.0f);
  std::cout << "************[begin_do_single_reaction]****************" << std::endl;
  if (b > ptr_algorithm->get_collision_rate()) {
    //TODO: REM
    selected.set_num_hit(300);
    selected.set_ml_num_hit(50);
    //TODO:REM
    if (selected.get_num_hit() - selected.get_min_local().get_num_hit() > ptr_algorithm->get_alpha()) {
      std::cout << "-----[begin_decomposition]--------------" << std::endl;
      do_decomposition(selected, index, other_selected, other_index, selected_ms);  // here
      std::cout << "-----[end_decomposition]--------------" << std::endl;
    } else {
      std::cout << "-----[begin_do_on_wall]--------------" << std::endl;
      do_on_wall_ineffect_collision(selected, index, selected_ms);
      std::cout << "-----[end_do_on_wall]--------------" << std::endl;
    }
  } else {
    std::cout << "-----[begin_do_synth]--------------" << std::endl;
    synth_ok = do_synthesis(selected, index, other_selected, other_index, selected_ms);
    std::cout << "-----[end_do_synth]--------------" << std::endl;
    if (!synth_ok) {
      std::cout << "-----[begin_do_intermolec]--------------" << std::endl;
      do_inter_molec_ineffect_collision(selected, index, other_selected, other_index, selected_ms);
      std::cout << "-----[end_do_intermolec]--------------" << std::endl;
    }
  }
}