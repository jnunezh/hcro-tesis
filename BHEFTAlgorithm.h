/*
 * File:   BHEFTAlgorithm.h
 * Author: Alexander
 *
 * Created on 12 de junio de 2015, 07:20 PM
 */

#ifndef BHEFTALGORITHM_H
#define BHEFTALGORITHM_H
#include <iostream>
#include <queue>
#include "Utils.h"
// struct Comparer{ bool operator()(const std::pair<task_t,Timespan>& a,const
// std::pair<task_t,Timespan>&  b){ return a.second > b.second;}};

class bheft_algorithm_t : public scheduling_algorithm_t {
 public:
  bheft_algorithm_t();
  virtual schedule_t run();
  schedule_t old_run();
  bheft_algorithm_t(const bheft_algorithm_t& orig);
  virtual ~bheft_algorithm_t();

 private:
};

#endif /* BHEFTALGORITHM_H */
