# encoding=utf8
# this script, given a filepath and two molecule indexes(starting from zero), outputs to terminal, their difference
# invoked, for example, like this:
# >python molecs_diff.py -p "/home/alexander/Documents/hcro-tesis/data/test_cr_molecs.txt" -f 1 -s 2

import time
import os
import sys
import getopt
import itertools
from termcolor import colored

def show_comparison(first_seq, sec_seq):
    index = 0;
    diff_count = 0
    for f, s in itertools.izip(first_seq, sec_seq):
        if f != s:
            diff_count += 1
            print colored('%3s:' % str(index), 'red'), colored(' %3s -- %3s' % (f, s), 'green')
        index += 1
    return diff_count


def sequence_from(token, cmp_ml):
    seqs = token.split('##########')
    if cmp_ml:
        seq = seqs[6]
    else:
        seq = seqs[3]
    return [s for s in seq.split(';') if len(s.strip()) > 0]

def main(argv):
    
    filepath = None
    ifirst = None
    isecond = None
    cmp_ml = False

    reload(sys)
    sys.setdefaultencoding('utf8')

    try:
        opts, args = getopt.getopt(argv,"p:f:s:m:",["path=", "first=", "second=", "minlocal="])
    except getopt.GetoptError:
        print '<script>.py -p <filepath> -f <first> -s <second> -m <minlocal>'
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-p", "--path"):
            filepath = arg
        elif opt in ("-f", "--first"):
            ifirst = int(arg)
        elif opt in ("-s", "--second"):
            isecond = int(arg)
        elif opt in ("-m", "--minlocal"):
            cmp_ml = bool(arg)

    if filepath is None or ifirst is None or isecond is None:
        print 'error # <script>.py -p <filepath> -f <first> -s <second> -m <minlocal>'
        sys.exit(2)
    
    start = time.time()

    text = None

    with open(filepath, 'r') as content_file:
        text = content_file.read()

    big_tokens = text.split('========================================')

    first_molec_seq = sequence_from(big_tokens[ifirst], cmp_ml)
    sec_molec_seq = sequence_from(big_tokens[isecond], cmp_ml)
    diff_count = show_comparison(first_molec_seq, sec_molec_seq)    
    end = time.time()
    print('diff_count: %d' % diff_count)
    print('program finished on %f mins' % ((end - start) / 60))

if __name__ == "__main__":
    main(sys.argv[1:])