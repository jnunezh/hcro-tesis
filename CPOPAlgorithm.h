/*
 * File:   CPOPAlgorithm.h
 * Author: Alexander
 *
 * Created on 10 de junio de 2015, 06:44 PM
 */

#ifndef CPOPALGORITHM_H
#define CPOPALGORITHM_H
#include <algorithm>
#include <cassert>
#include <iterator>
#include <limits>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <vector>
#include "Utils.h"

class cpop_algorithm_t : public scheduling_algorithm_t {
 public:
  cpop_algorithm_t();
  virtual schedule_t run();
  schedule_t old_run();
  cpop_algorithm_t(const cpop_algorithm_t& orig);
  virtual ~cpop_algorithm_t();

 private:
};

#endif /* CPOPALGORITHM_H */
