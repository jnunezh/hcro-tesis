class hcro_algorithm_t

	def assign(self, idest, isrc):
		pass

	def set_population_and_indexes(self, ini_pop_sz, max_pop_sz):
		pass

	def pop_first_succesor_index_of(self, task_index, population_index):
		pass

	


	def set_init_population(self):
		NON_EXISTENT = -1
		
		ini_pop_sz = self.num_groups * self.section_amount * self.num_threads
		max_pop_sz = self.max_num_groups * self.section_amount * self.num_threads

		#self.population = [self.get_empty_molecule()] * self.param.max_pop_size
		#self.indexes = [NON_EXISTENT] * self.param.max_pop_size
		
		# crear pool de poblacion inicial e indexes iniciales y setearlos como validos(existent) 
		self.set_population_and_indexes(ini_pop_sz, max_pop_sz)

		model_molecules_indexes = self.get_model_molecules()

		accum_pop_index = 0
		done = False
		## self.param.ini_pop_size xq luego crece el array hacia self.param.max_pop_size

		for index_model_molecule in model_molecules_indexes:
			if done:
				break
			for i in xrange(0, self.param.num_tasks):
				self.assign(accum_pop_index, index_model_molecule)
				j = self.pop_first_succesor_index_of(i, index_model_molecule)
				self.pop_left_rotate_by_one(accum_pop_index, i, j - 1)
				self.pop_calculate_pe(accum_pop_index)
				self.pop_set_inner_min_default(accum_pop_index)
				accum_pop_index += 1
				if accum_pop_index == ini_pop_sz:
					done = True

		assert(num_tasks >= 2)
		e = num_tasks - 2

		while accum_pop_index < ini_pop_sz:
		    u = rand_inclusive(0, accum_pop_index - 1)
		    i = rand_inclusive(0, e)
		    self.assign(accum_pop_index, u)
		    j = self.pop_first_succesor_index_of(i, accum_pop_index);

		    self.pop_left_rotate_by_one(accum_pop_index, i, j - 1)

		    self.pop_calculate_pe(accum_pop_index)
		    self.pop_set_inner_min_default(accum_pop_index)
		    accum_pop_index += 1


	def get_model_molecules(self):
		model_molecules = []

		for cmp in self.ordering_criteria:
			v = self.pop_init(False)
			cmp.generate(self.param, self.param.num_tasks)
			custom_sort(v, cmp)
			model_molecules.add(v)
		return model_molecules

	def do_decomposition(self, molecule_index, thread_idx):
		best_pe = (-1, 0.0)

		ifirst = self.pop_init(False, False)
		isecond = self.pop_init(False, False)
		self.pop_generate_decomp_candidates(molecule_index, ifirst, isecond)
		fpe = self.pop_calculate_pe(ifirst)
		spe = self.pop_calculate_pe(isecond)
		ope = self.pop_get_pe(molecule_index)
		oke = self.pop_get_ke(molecule_index)
		_exec = True
		if ope + oke >= fpe + spe:
			edec = ope + oke - (fpe + spe)
			_exec = False
		if _exec:
			delta1 = rand_inclusive(0.0, 1.0)
			delta2 = rand_inclusive(0.0, 1.0)
		if edec >= 0.0 or !_exec:
			if (_exec):
				#lock
				self.buffer = self.buffer * (1.0 - delta1 * delta2)
				#elock

			delta3 = rand_inclusive(0.0, 1.0)
		    self.pop_set_ke(ifirst, edec * delta3)
		    self.pop_set_ke(isecond, edec * (1.0 - delta3))

		    fi = self.create_individual(thread_idx)
		    si = self.create_individual(thread_idx)
		    self.assign(fi, ifirst)
		    self.assign(si, isecond)
		    self.assign(self.pop_min_local(fi), fi)
		    self.assign(self.pop_min_local(si), si)
		    self.pop_remove_individual(molecule_index, thread_idx)

		    if almost_equal(min(fpe, spe), fpe):
		    	best_pe[0] = fi
		    	best_pe[1] = fpe
		    else:
		    	best_pe[0] = si
		    	best_pe[1] = spe
		else:
			self.pop_set_num_hit(molecule_index, self.pop_get_num_hit(molecule_index) + 1)
			best_pe[0] = index
			best_pe[1] = ope
		return best_pe

	def do_on_wall_ineffect_collision(self, molecule_index, thread_idx):
		best_pe = (-1, 0.0)
		ifirst = self.pop_init(False, False);
		self.pop_generate_owi_candidate(molecule_index, ifirst)
		fpe = self.pop_calculate_pe(ifirst)
		ope = self.pop_get_pe(molecule_index)
		oke = self.pop_get_ke(molecule_index)
		self.pop_set_num_hit(molecule_index, self.pop_get_num_hit(molecule_index) + 1)
		best_pe[0] = molecule_index
		if ope + oke >= fpe:
			a = rand_inclusive(ke_loss_rate, 1.0)
			self.pop_set_ke(ifirst, (ope - fpe + oke) * a)
			#lock
		    self.buffer = self.buffer + (ope - fpe + oke) * (1.0 - a)
		    #elock
		    self.assign(molecule_index, ifirst)
		    if self.pop_get_pe(molecule_index) < self.pop_get_pe(self.pop_min_local(molecule_index)):
		      self.assign(self.pop_min_local(molecule_index), molecule_index)
		    best_pe[1] = fpe
		    return best_pe
		
		best_pe[1] = ope
		return best_pe

	def do_synthesis(self, molecule_index, thread_idx):
		rnd = rand_inclusive(0.0, 1.0)
		om_index = molecule.select_candidate(rnd > 0.5, thread_idx)
		ifirst = self.pop_init(False, False)
		ok_synth = self.pop_generate_synth_candidate(molecule_index, om_index, ifirst, thread_idx)
		if !ok_synth:
			return False
		wke = self.pop_get_ke(molecule_index)
		owke = self.pop_get_ke(om_index)
		if wke > beta or owke > beta:
			return False
		fpe = self.pop_calculate_pe(ifirst)
		wpe = self.pop_get_pe(molecule_index)
		owpe = self.pop_get_pe(om_index)

		if wpe + owpe + wke + owke >= fpe:
			self.pop_set_ke(ifirst, (wpe + owpe + wke + owke) - fpe)
			fi = self.pop_create_individual(thread_idx)
			self.assign(fi, ifirst)
			self.assign(self.pop_min_local(fi), fi)
			self.pop_remove_individual(m_index, thread_idx)
			self.pop_remove_individual(om_index, thread_idx)
			best_pe[0] = fi
			best_pe[1] = fpe
		else:
			self.pop_set_num_hit(molecule_index, self.pop_get_num_hit(molecule_index) + 1)
			self.pop_set_num_hit(om_index, self.pop_get_num_hit(om_index) + 1)

			if almost_equal(min(wpe, owpe), wpe):
				best_pe[0] = m_index
				best_pe[1] = wpe
			else:
				best_pe[0] = om_index
				best_pe[1] = owpe

		return True;

	def do_inter_molec_ineffect_collision(self, molecule_index, thread_idx):
		best_pe = (-1, 0.0)
		om_index = self.pop_select_candidate(molecule_index, False, thread_idx)

		ifirst = self.pop_init(False, False)
		isecond = self.pop_init(False, False)
		self.pop_generate_imi_candidates(molecule_index, om_index, ifirst, isecond)
		
		ffpe = self.pop_calculate_pe(ifirst)
		sspe = self.pop_calculate_pe(isecond)
		fpe = self.pop_get_pe(molecule_index)
		fke = self.pop_get_ke(molecule_index)
		spe = self.pop_get_pe(om_index)
		ske = self.pop_get_ke(om_index)
		self.pop_set_num_hit(molecule_index, self.pop_get_num_hit(molecule_index) + 1)
		self.pop_set_num_hit(om_index, self.pop_get_num_hit(om_index) + 1)
		einter = (fpe + spe + fke + ske) - (ffpe + sspe)
		if einter >= 0.0:
			delta4 = rand_inclusive(0.0, 1.0)
			self.pop_set_ke(ifirst, einter * delta4)
			self.pop_set_ke(isecond, einter * (1.0 - delta4))
			self.pop_generate_imi_candidates(ifirst, isecond, molecule_index, om_index)
			self.pop_set_pe(molecule_index, self.pop_get_pe(ifirst))
			self.pop_set_pe(om_index, self.pop_get_pe(isecond))
			self.pop_set_ke(molecule_index, self.pop_get_ke(ifirst))
			self.pop_set_ke(om_index, self.pop_get_ke(isecond))
			if self.pop_get_pe(molecule_index) < self.pop_get_pe(self.pop_min_local(molecule_index)):
				self.assign(self.pop_min_local(molecule_index), molecule_index)
			if self.pop_get_pe(om_index) < self.pop_get_pe(self.pop_min_local(om_index)):
				self.assign(self.pop_min_local(om_index), om_index)
			if almost_equal(min(ffpe, sspe), ffpe):
				best_pe[0] = index
				best_pe[1] = ffpe
			else:
				best_pe[0] = om_index
				best_pe[1] = sspe
			return best_pe

		if almost_equal(min(fpe, spe), fpe):
			best_pe[0] = index
			best_pe[1] = fpe
		else:
			best_pe[0] = om_index
			best_pe[1] = spe

		return best_pe

	def remove_individual(self, individual_index, thread_idx):
		self.indexes[individual_index] = NON_EXISTENT
		#lock
		self.num_available[thread_idx] += 1
		#elock

	def select_individual(self, except_index, existent, thread_idx):
		assert(population.size() > 0)
		g_marker = group_idx = individual_index = -1;

		#ifdef CPU_MULTITHREADED
		#  mp_mutex.lock();
		#endif

		if num_available[thread_idx] == 0:
			if not existent:
				self.enlarge_population()
				group_idx = g_marker = num_groups - 1

		if g_marker < 0:
			g_marker = rand_inclusive(0, num_groups - 1)
			tmp = g_marker + 1
			group_idx = (tmp < num_groups ? tmp : 0)

		#ifdef CPU_MULTITHREADED
		#  mp_mutex.unlock();
		#endif

		finished = False
		while !finished:
			#ifdef CPU_MULTITHREADED
    		#mp_mutex.lock();
			#endif
			if group_idx >= num_groups:
				group_idx = 0
			#ifdef CPU_MULTITHREADED
			#mp_mutex.unlock();
			#endif

			lb = (group_idx * num_threads * section_amount) + (thread_idx * section_amount)
			ub = lb + section_amount - 1
			marker = rand_inclusive(lb, ub)
			tmp = marker + 1
			index = (tmp > ub ? lb : tmp)

			while !finished:
				if index > ub:
					index = lb
				if index == except_index:
					continue
				if indexes[index] == existent:
					individual_index = index
					finished = True
				if index == marker:
					break
				index += 1

			if group_idx == g_marker:
				break

			group_idx += 1

		assert(individual_index >= 0)

		#ifdef CPU_MULTITHREADED
		#mp_mutex.lock()
		#endif
		ret_i =  individual_index #population[individual_index]
		#ifdef CPU_MULTITHREADED
		#mp_mutex.unlock()
		#endif
		return ret_i



	def do_single_reaction(self, thread_idx):
		b = rand_inclusive(0.0, 1.0)
  		selected_index = self.select_individual(-1, EXISTENT, thread_idx)
  		if b > self.collision_rate:
  			min_local_index = self.pop_min_local(selected_index)
  			if self.pop_num_hit(selected_index) - self.pop_num_hit(min_local_index) > self.alpha:
		    	return self.do_decomposition(selected_index, thread_idx)
		    else:
		    	return self.do_on_wall_ineffect_collision(selected_index, thread_idx)
		else:
			(synth_ok, ret_pair) = self.do_synthesis(selected_index, thread_idx)
			if not synth_ok:
				return self.do_inter_molec_ineffect_collision(selected_index, thread_idx)
			else:
				return ret_pair

	def do_reactions(self):
		min_makespan = infinity
		min_molecule_index = -1
		q = async_queue()
		for i in xrange(0, self.param.num_threads):
			q.add(do_single_reaction)

		for i in xrange(0, self.param.num_threads):
			(molecule_index, makespan) = q.get()
			if makespan < min_makespan:
				min_makespan = makespan
				min_molecule_index = molecule_index
		return (min_makespan, min_molecule_index)


	def run(self):
		self.set_init_population()
		curr_count = 0
		min_makespan = infinity
		molecule_index = -1
		min_molecule_index = -1
		while curr_count <= self.param.num_iterations:
			(makespan, molecule_index) = do_reactions()

		    if makespan < min_makespan:
		      min_makespan = makespan
		      min_molecule_index = molecule_index

		    curr_count += 1
		
		inner_min_index = self.pop_min_local(min_molecule_index)
		min_schedule = self.pop_calculate_pe(inner_min_index, False) #~ inner_min_index.calculate_pe
		return min_schedule
	